# datalib

Bibliothèque de données et datasets libres pour le Master Data Science en Santé.

## Types de datasets

**Datamanagement:**

![](https://gitlab.com/antoinelamer/resources/-/raw/master/icon/raw_dataset.gif) raw dataset

![](https://gitlab.com/antoinelamer/resources/-/raw/master/icon/cleaned_dataset.png) cleaned dataset

![](https://gitlab.com/antoinelamer/resources/-/raw/master/icon/merged_dataset.jpeg) merged dataset

**Statistical unit:**

![](https://gitlab.com/antoinelamer/resources/-/raw/master/icon/individual_dataset.png)  individual : one row per patient

![](https://gitlab.com/antoinelamer/resources/-/raw/master/icon/aggregated_dataset.png)  aggregated dataset