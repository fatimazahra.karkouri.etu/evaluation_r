# Présentation

Ce dataframe contient le French Deprivation Index (Fdep) à l’échelle
communale. Les données date de 2017 et la géographie en vigueur est
celle de 2020.

Le FDEP est un indice composite combinant 4 variables: 
- le revenu fiscal médian par unité de consommation 
- la proportion de chômeurs dans
la population active de 15 à 64 ans 
- la proportion d’ouvriers dans la population active de 15 à 64 ans 
- la proportion de bacheliers dans la population de 15 ans et plus 
non scolarisé

Le FDep est calculé en réalisant une ACP avec ces 4 variables, pondérée
par la population de la commune. Le FDEP correspond à l’opposé du
coefficient de corrélation de la commune considérée avec la 1ère
comosante principale.

Plus le FDep est élevé, plus la commune est socialement défavorisée.

[article de référence] http://bmcpublichealth.biomedcentral.com/articles/10.1186/1471-2458-9-33

# Intérêt

Le FDep peut notamment être utilisé dans les études écologiques.

Exemple d’étude utilisant le FDep: Michel M, Alberti C, Carel J-C,
Chevreul K. Association of Pediatric Inpatient Socioeconomic Status With
Hospital Efficiency and Financial Balance. JAMA Netw Open. 18 oct
2019;2(10):e1913656.

# Données brutes (INSEE)

-   Revenu médian par unité de consommation:
    [source](https://www.insee.fr/fr/statistiques/4291712)
-   Proportion de chômeurs et d’ouvrier dans la population active de 15
    à 64 ans:
    [source](https://www.insee.fr/fr/statistiques/4515500?sommaire=4516095&q=Emploi+-%20population+active)
-   Proportion de bacheliers dans la population de 15 ans et plus non
    scolarisé:
    [source](https://www.insee.fr/fr/statistiques/4516086?sommaire=4516089)
-   Population:
    [source](https://www.insee.fr/fr/statistiques/4515565?sommaire=4516122&q=Évolution+et+structure+de+la+population)

# Calcul des variables nécessaire au calcul du FDep

-   Revenu médian par unité de consommation: Q217
-   Proportion de d’ouvrier dans la population active de 15 à 64 ans:
    C17\_ACT1564\_CS6/C17\_ACT1564
-   Proportion de chômeurs dans la population active de 15 à 64 ans:
    P17\_CHOM1564/ P17\_ACT1564
    P17\_NSCOL15P\_BAC+P17\_NSCOL15P\_SUP2+P17\_NSCOL15P\_SUP34+P17\_NSCOL15P\_SUP5)/P17\_NSCOL15P
-   Population: P17\_POP

A noter, le revenu médian par unité de consommation étant fourni selon
la géographie en vigueur en 2018, il est nécessaire de convertir les
unités spatiale en celle de 2020. En cas de fusion de commune, la
variable est agrégée par la moyenne pondérée par la population.

De plus, en raison du secret statistique cette variable est manquante
pour environ 10% des communes. Une imputation des données manquantes a
été réalisée en utilisant également la moyenne des revenus médians
pondérée par la population des communes adjacentes (cf exemple
d’application du dossier matrice\_adjacence\_commune\_2020).)

# Extrait dataframe final

<table>
<thead>
<tr class="header">
<th style="text-align: left;">CODGEO_2020</th>
<th style="text-align: right;">P17_POP</th>
<th style="text-align: right;">revenu_med</th>
<th style="text-align: right;">prop_bachelier</th>
<th style="text-align: right;">prop_ouvrier</th>
<th style="text-align: right;">prop_chomeur</th>
<th style="text-align: right;">fdep17</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">01001</td>
<td style="text-align: right;">776</td>
<td style="text-align: right;">23760</td>
<td style="text-align: right;">0.4258082</td>
<td style="text-align: right;">0.2905322</td>
<td style="text-align: right;">0.0878871</td>
<td style="text-align: right;">0.1094274</td>
</tr>
<tr class="even">
<td style="text-align: left;">01002</td>
<td style="text-align: right;">248</td>
<td style="text-align: right;">24230</td>
<td style="text-align: right;">0.5657143</td>
<td style="text-align: right;">0.0909091</td>
<td style="text-align: right;">0.0813008</td>
<td style="text-align: right;">-1.6774079</td>
</tr>
<tr class="odd">
<td style="text-align: left;">01004</td>
<td style="text-align: right;">14035</td>
<td style="text-align: right;">19610</td>
<td style="text-align: right;">0.4486848</td>
<td style="text-align: right;">0.2841114</td>
<td style="text-align: right;">0.1485407</td>
<td style="text-align: right;">0.8762977</td>
</tr>
<tr class="even">
<td style="text-align: left;">01005</td>
<td style="text-align: right;">1689</td>
<td style="text-align: right;">23680</td>
<td style="text-align: right;">0.4162378</td>
<td style="text-align: right;">0.2607809</td>
<td style="text-align: right;">0.0784833</td>
<td style="text-align: right;">-0.0752440</td>
</tr>
<tr class="odd">
<td style="text-align: left;">01006</td>
<td style="text-align: right;">111</td>
<td style="text-align: right;">25090</td>
<td style="text-align: right;">0.4526316</td>
<td style="text-align: right;">0.2500000</td>
<td style="text-align: right;">0.1379310</td>
<td style="text-align: right;">-0.0003675</td>
</tr>
<tr class="even">
<td style="text-align: left;">01007</td>
<td style="text-align: right;">2726</td>
<td style="text-align: right;">22520</td>
<td style="text-align: right;">0.5034516</td>
<td style="text-align: right;">0.2421777</td>
<td style="text-align: right;">0.0761554</td>
<td style="text-align: right;">-0.4353620</td>
</tr>
</tbody>
</table>

# Analyse descriptive

### Analyse données manquantes

Dans l’ACP, les communes ayant des données manquantes ne sont pas
utilisées.

**Données utilisées pour l’ACP**

<table>
<colgroup>
<col style="width: 3%" />
<col style="width: 17%" />
<col style="width: 16%" />
<col style="width: 14%" />
<col style="width: 15%" />
<col style="width: 15%" />
<col style="width: 16%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;"></th>
<th style="text-align: left;">CODGEO_2020</th>
<th style="text-align: left;">P17_POP</th>
<th style="text-align: left;">revenu_med</th>
<th style="text-align: left;">prop_bachelier</th>
<th style="text-align: left;">prop_ouvrier</th>
<th style="text-align: left;">prop_chomeur</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;">Length:34745</td>
<td style="text-align: left;">Min. : 4</td>
<td style="text-align: left;">Min. : 7590</td>
<td style="text-align: left;">Min. :0.0678</td>
<td style="text-align: left;">Min. :0.0000</td>
<td style="text-align: left;">Min. :0.00000</td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;">Class :character</td>
<td style="text-align: left;">1st Qu.: 200</td>
<td style="text-align: left;">1st Qu.:18680</td>
<td style="text-align: left;">1st Qu.:0.3497</td>
<td style="text-align: left;">1st Qu.:0.1811</td>
<td style="text-align: left;">1st Qu.:0.07594</td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;">Mode :character</td>
<td style="text-align: left;">Median : 458</td>
<td style="text-align: left;">Median :20359</td>
<td style="text-align: left;">Median :0.4112</td>
<td style="text-align: left;">Median :0.2500</td>
<td style="text-align: left;">Median :0.10096</td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;">NA</td>
<td style="text-align: left;">Mean : 1963</td>
<td style="text-align: left;">Mean :20932</td>
<td style="text-align: left;">Mean :0.4172</td>
<td style="text-align: left;">Mean :0.2586</td>
<td style="text-align: left;">Mean :0.10840</td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;">NA</td>
<td style="text-align: left;">3rd Qu.: 1154</td>
<td style="text-align: left;">3rd Qu.:22500</td>
<td style="text-align: left;">3rd Qu.:0.4777</td>
<td style="text-align: left;">3rd Qu.:0.3333</td>
<td style="text-align: left;">3rd Qu.:0.13304</td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;">NA</td>
<td style="text-align: left;">Max. :2187526</td>
<td style="text-align: left;">Max. :50280</td>
<td style="text-align: left;">Max. :0.8675</td>
<td style="text-align: left;">Max. :1.0000</td>
<td style="text-align: left;">Max. :0.57143</td>
</tr>
</tbody>
</table>

**Données manquantes pour l’ACP**

<table style="width:100%;">
<colgroup>
<col style="width: 3%" />
<col style="width: 17%" />
<col style="width: 15%" />
<col style="width: 14%" />
<col style="width: 15%" />
<col style="width: 15%" />
<col style="width: 16%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;"></th>
<th style="text-align: left;">CODGEO_2020</th>
<th style="text-align: left;">P17_POP</th>
<th style="text-align: left;">revenu_med</th>
<th style="text-align: left;">prop_bachelier</th>
<th style="text-align: left;">prop_ouvrier</th>
<th style="text-align: left;">prop_chomeur</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;">Length:139</td>
<td style="text-align: left;">Min. : 0.00</td>
<td style="text-align: left;">Min. :10140</td>
<td style="text-align: left;">Min. :0.0000</td>
<td style="text-align: left;">Min. :0.0000</td>
<td style="text-align: left;">Min. :0.00000</td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;">Class :character</td>
<td style="text-align: left;">1st Qu.: 16.00</td>
<td style="text-align: left;">1st Qu.:17801</td>
<td style="text-align: left;">1st Qu.:0.3154</td>
<td style="text-align: left;">1st Qu.:0.0000</td>
<td style="text-align: left;">1st Qu.:0.00000</td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;">Mode :character</td>
<td style="text-align: left;">Median : 28.50</td>
<td style="text-align: left;">Median :18902</td>
<td style="text-align: left;">Median :0.4000</td>
<td style="text-align: left;">Median :0.1133</td>
<td style="text-align: left;">Median :0.09677</td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;">NA</td>
<td style="text-align: left;">Mean : 35.74</td>
<td style="text-align: left;">Mean :18725</td>
<td style="text-align: left;">Mean :0.4146</td>
<td style="text-align: left;">Mean :0.1526</td>
<td style="text-align: left;">Mean :0.11588</td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;">NA</td>
<td style="text-align: left;">3rd Qu.: 42.75</td>
<td style="text-align: left;">3rd Qu.:19977</td>
<td style="text-align: left;">3rd Qu.:0.5164</td>
<td style="text-align: left;">3rd Qu.:0.2411</td>
<td style="text-align: left;">3rd Qu.:0.16667</td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;">NA</td>
<td style="text-align: left;">Max. :249.00</td>
<td style="text-align: left;">Max. :22455</td>
<td style="text-align: left;">Max. :0.8889</td>
<td style="text-align: left;">Max. :0.7273</td>
<td style="text-align: left;">Max. :0.60000</td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;">NA</td>
<td style="text-align: left;">NA’s :1</td>
<td style="text-align: left;">NA’s :69</td>
<td style="text-align: left;">NA’s :7</td>
<td style="text-align: left;">NA’s :73</td>
<td style="text-align: left;">NA’s :8</td>
</tr>
</tbody>
</table>

Il y a 139 communes avec des données manquantes et il s’agit de communes
ayant une faible population.

### Statistique descriptive du FDep

    ##    Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
    ## -5.8078 -0.3874  0.4552  0.4275  1.2782  8.4484
	
### Distribution

![](fdep_files/figure-markdown_strict/unnamed-chunk-7-1.png) \`\`\`

# Datamanagement et calcul de la variable
cf dossier script

# Licence des données brutes
Absence de licence noté sur le site

Condition d'utilisation des données de l'INSEE: (https://www.insee.fr/fr/information/2381863)