<table>
<thead>
<tr class="header">
<th style="text-align: left;">nom_long</th>
<th style="text-align: left;">nom_court</th>
<th style="text-align: left;">type</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">Code géographique 2020</td>
<td style="text-align: left;">CODGEO_2020</td>
<td style="text-align: left;">chaîne de caractères</td>
</tr>
<tr class="even">
<td style="text-align: left;">Population 2017</td>
<td style="text-align: left;">P17_POP</td>
<td style="text-align: left;">quantitative</td>
</tr>
<tr class="odd">
<td style="text-align: left;">Revenu médian</td>
<td style="text-align: left;">revenu_median</td>
<td style="text-align: left;">quantitative</td>
</tr>
<tr class="even">
<td style="text-align: left;">Proportion de bacheliers</td>
<td style="text-align: left;">prop_bachelier</td>
<td style="text-align: left;">quantitative</td>
</tr>
<tr class="odd">
<td style="text-align: left;">Proportion d’ouvrier</td>
<td style="text-align: left;">prop_ouvrier</td>
<td style="text-align: left;">quantitative</td>
</tr>
<tr class="even">
<td style="text-align: left;">Proportion de chômeurs</td>
<td style="text-align: left;">prop_chomeur</td>
<td style="text-align: left;">quantitative</td>
</tr>
<tr class="odd">
<td style="text-align: left;">Indice de déprivation 2017</td>
<td style="text-align: left;">fdep17</td>
<td style="text-align: left;">quantitative</td>
</tr>
</tbody>
</table>
