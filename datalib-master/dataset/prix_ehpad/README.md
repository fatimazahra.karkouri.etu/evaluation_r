# Contexte

Afin de renforcer la transparence et l'information sur les prix pratiqués dans les établissements d’hébergement pour personnes âgées dépendantes (EHPAD), la loi d’adaptation de la société au vieillissement oblige,  tous les ans depuis 2016,  les EHPAD à transmettre à la Caisse nationale de solidarité pour l’autonomie (CNSA) leurs prix hébergement et leurs tarifs dépendance associées à un liste de prestations minimales que ces établissements doivent fournir. 

**Quelques définitions :**
- **ASH** : Aide Sociale à l'Hébergement.
- **ESMS** : Etablissements et Services Médicaux - Sociaux.
- **GIR** : Groupe Iso-Ressources, correspond au niveau de perte d'autonomie d'une personne âgée. Il existe 6 valeurs de GIR (1 étant la plus forte et 6 la plus faible).

# Présentation du jeu de données 

Les données mises à disposition ici décrivent les prix d'hébergement et les tarifs dépendances des EHPAD françaises pour l'année 2019. 

| annee | finesset | finessej  | categorie | statut\_jur | ash | nb\_capins\_HP | nb\_capins\_HT | prixHebPermCs | prixHebPermCd | prixHebPermCsa | prixHebPermCda | prixHebTempCs | prixHebTempCd | prixHebTempCsa | prixHebTempCda | tarifGir12 | tarifGir34 | tarifGir56 | prixHebPermCsCNSA | prixHebPermCdCNSA | prixHebPermCsaCNSA | prixHebPermCdaCNSA | prixHebTempCsCNSA | prixHebTempCdCNSA | prixHebTempCsaCNSA | prixHebTempCdaCNSA | tarifGir12CNSA | tarifGir34CNSA | tarifGir56CNSA | prixMoisHebPermCsGir56 |
| ----- | -------- | --------- | --------- | ----------- | --- | -------------- | -------------- | ------------- | ------------- | -------------- | -------------- | ------------- | ------------- | -------------- | -------------- | ---------- | ---------- | ---------- | ----------------- | ----------------- | ------------------ | ------------------ | ----------------- | ----------------- | ------------------ | ------------------ | -------------- | -------------- | -------------- | ---------------------- |
| 2019  | 10002228 | 750057622 | EHPAD   | 3   | 0 | 60   | 0    | 99.6  | 89.1 | NA    | NA  | 109.5 | 98  | NA | NA   | 20.65 | 13.12  | 5.56 | 99.6   | 89.1    | NA   | NA  | NA   | NA | NA  | NA   | 20.65  | 13.12 | 5.56 | 3154.8  |
| 2019  | 10004059 | 10783009  | EHPAD | 2    | 1 | 66   | 4  | 68.13 | NA  | 68.13  | NA| 80   | NA  | 80  | NA   | 19.89 | 12.62 | 5.35  | 68.13 | NA  | 68.13  | NA   | 80  | NA   | NA  | NA  | 19.89 | 12.62 | 5.35 | 2204.4 |
| 2019  | 10006799 | 690802715 | EHPAD     | 2 | 1 | 80  | 4 | 64.97  | NA     | 64.97   | NA   | 87.09 | NA   | NA   | NA   | 17.71 | 11.25 | 4.77  | 64.97    | NA  | 64.97  | NA   | 87.09   | NA   | NA | NA | 17.71 | 11.25  | 4.77  | 2092.2 |
| 2019  | 10008571 | 10007987  | EHPAD | 1 | 1 | 69  | 0  | 45.78  | 45.78   | 45.78  | 45.78   | 45.78 | 45.78 | 45.78  | 45.78  | 23.29  | 14.78 | 6.27| 45.78   | 45.78 | 45.78 | 45.78   | NA  | NA   | NA  | NA | 23.29 | 14.78   | 6.27  | 1561.5 |


Les données proviennent de la **Caisse Nationale de Solidarité pour l'Autonomie (CNSA)** , elles ont été récupérées sur le site data.gouv : https://www.data.gouv.fr/fr/datasets/prix-hebergement-et-tarifs-dependance-des-ehpad/.

Le jeu de données est formé par un fichier *".csv"* : **cnsa-documentation-donnees-ehpad-retraitee.csv**, qui comporte deux pages : 
- Page 1 : Description des variables 
- Page 2 : Données  (6751 obs x 31 variables) 

***Mises à jour*** : annuelle
**License** :  Open Data

# Présentation des variables 

| Nom Variable   | Libellé variable | Type |
|:--------------:|:----------------:|:---:|
| annee  | Prix saisis pour l’année analysée, c’est-à-dire du 1er janvier au 31 décembre   | character |
| finesset     | Numéro finess juridique de l’établissement    | character |
| finessej   | Numéro finess géographique de l’établissement   | character |
| catégorie   | Catégorie d'ESMS |character |
| statut_jur | Statut juridique agrégé FINESS  : 1 - Public ; 2 - Privé non lucratif ; 3 - Privé commercial |character |
| ash | Habilitation à l’aide sociale (ASH) \- si code mft in (08,09,21,40,41,44,45,48,50,52,56) Alors ASH= 1, sinon ASH=0| character |
| nb\_capins\_HP  | Nombre de capacité installés en HP (Hébergement Permanent)  | numeric |
| nb\_capins\_HT   | Nombre de capacité installés en HT (Hébergement Temporaire) | numeric |
| prixHebPermCs   | Prix de l’hébergement permanent en chambre seule   | numeric |
| prixHebPermCd  | Prix de l’hébergement permanent en chambre double   | numeric |
| prixHebPermCsa    | Prix de l’hébergement permanent en chambre seule pour personne bénéficiaire de l’ASH   | numeric |
| prixHebPermCda   | Prix de l’hébergement permanent en chambre double pour personne bénéficiaire de l’ASH   | numeric |
| prixHebTempCs   | Prix de l’hébergement temporaire en chambre seule     | numeric |
| prixHebTempCd | Prix de l’hébergement temporaire en chambre double   | numeric |
| prixHebTempCsa  | Prix de l’hébergement temporaire en chambre seule pour personne bénéficiaire de l’ASH   | numeric |
| prixHebTempCda | Prix de l’hébergement temporaire en chambre double pour personne bénéficiaire de l’ASH  | numeric |
| tarifGir12     | Tarif dépendance GIR 1-2    | numeric |
| tarifGir34   | Tarif dépendance GIR 3-4  | numeric |
| tarifGir56    | Tarif dépendance GIR 5-6     | numeric |
| prixHebPermCsCNSA  | Prix de l’hébergement permanent en chambre seule corrigé par la CNSA | numeric |
| prixHebPermCdCNSA  | Prix de l’hébergement permanent en chambre double corrigé par la CNSA | numeric |
| prixHebPermCsaCNSA | Prix de l’hébergement permanent en chambre seulepour personne bénéficiaire de l’ASH corrigé par la CNSA | numeric |
| prixHebPermCdaCNSA  | Prix de l’hébergement permanent en chambre double pour personne bénéficiaire de l’ASH corrigé par la CNSA | numeric |
| prixHebTempCsCNSA  | Prix de l’hébergement temporaire en chambre seule corrigé par la CNSA | numeric |
| prixHebTempCdCNSA | Prix de l’hébergement temporaire en chambre double corrigé par la CNSA | numeric |
| prixHebTempCsaCNSA | Prix de l’hébergement temporaire en chambre seule pour personne bénéficiaire de l’ASH corrigé par la CNSA | numeric |
| prixHebTempCdaCNSA | Prix de l’hébergement temporaire en chambre double pour personne bénéficiaire de l’ASH corrigépar la CNSA | numeric |
| tarifGir12CNSA    | Tarif Dépendance GIR 1-2 corrigé par la CNSA| numeric |
| tarifGir34CNSA   | Tarif Dépendance GIR 3-4 corrigé par la CNSA | numeric |
| tarifGir56CNSA  |Tarif Dépendance GIR 3-4 corrigé par la CNSA  | numeric |
| prixMoisHebPermCsGir56 | Prix par mois (en €) en hébergement permanent - chambre seule + tarif dépendance GIR 5-6 = (PRIX\_HEB\_PERM\_CS+TARIF\_GIR\_56)\*30;  | numeric |

# Statistiques descriptives

## Variables quantitatives

![quant](./statistiques_descriptives/quant.PNG)

## Variables qualitatives

![quali](./statistiques_descriptives/quali.PNG)

# Modifications apportées
- Remplacement des cases vides par NA
- Renommage des variables pour clarification 
- Réencodage des variables ash et statut_juridique

**Génération d'un fichier propre nommé "data_ehpad_cleaned.csv".**
