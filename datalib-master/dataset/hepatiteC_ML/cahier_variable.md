<table>
<thead>
<tr class="header">
<th style="text-align: left;">variable</th>
<th style="text-align: left;">type</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">x</td>
<td style="text-align: left;">integer</td>
</tr>
<tr class="even">
<td style="text-align: left;">category</td>
<td style="text-align: left;">character</td>
</tr>
<tr class="odd">
<td style="text-align: left;">age</td>
<td style="text-align: left;">integer</td>
</tr>
<tr class="even">
<td style="text-align: left;">sex</td>
<td style="text-align: left;">character</td>
</tr>
<tr class="odd">
<td style="text-align: left;">alb</td>
<td style="text-align: left;">numeric</td>
</tr>
<tr class="even">
<td style="text-align: left;">alp</td>
<td style="text-align: left;">numeric</td>
</tr>
<tr class="odd">
<td style="text-align: left;">alt</td>
<td style="text-align: left;">numeric</td>
</tr>
<tr class="even">
<td style="text-align: left;">ast</td>
<td style="text-align: left;">numeric</td>
</tr>
<tr class="odd">
<td style="text-align: left;">bil</td>
<td style="text-align: left;">numeric</td>
</tr>
<tr class="even">
<td style="text-align: left;">che</td>
<td style="text-align: left;">numeric</td>
</tr>
<tr class="odd">
<td style="text-align: left;">chol</td>
<td style="text-align: left;">numeric</td>
</tr>
<tr class="even">
<td style="text-align: left;">crea</td>
<td style="text-align: left;">numeric</td>
</tr>
<tr class="odd">
<td style="text-align: left;">ggt</td>
<td style="text-align: left;">numeric</td>
</tr>
<tr class="even">
<td style="text-align: left;">prot</td>
<td style="text-align: left;">numeric</td>
</tr>
</tbody>
</table>
