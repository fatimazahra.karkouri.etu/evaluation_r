### Données de biologie de donneurs de sang et de patients atteints d'hépatite C - visée prédictive.

Description
-----------

Le jeu de données, fourni par le centre pour le Machine Learning de l’UC
Irvine, est composé de données de biologie, l’âge et le sexe de 615
personnes. Il est composé de 14 variables.

Tous les attributs, à l’exception de la catégorie et du sexe, sont
numériques. Les attributs 1 à 4 se rapportent aux données du patient :

1.  X (ID/No. du patient)
2.  Catégorie (diagnostic) (valeurs : 0=Donneur de sang’, ‘0s=Donneur de
    sang suspect’, ‘1=Hépatite’, ‘2=Fibrose’, ‘3=Cirrhose’)
3.  Âge (en années)
4.  Sexe (f,m) Les attributs 5 à 14 se réfèrent aux données de
    laboratoire.

Il a pour objectif la classification donneurs de sang vs porteurs
Hépatite C, et ses évolutions (simple, hépatite C, fibrose, cirrhose).

| Fichier | Description | 
| -------- | -------- | 
| HepatitisCdata.csv | Jeu de données | 
| HepatiteC_data.Rmd |  Markdown pour l'aperçu du dataset + table 1 + .md |

Preview
-------

<table>
<thead>
<tr class="header">
<th style="text-align: right;">x</th>
<th style="text-align: left;">category</th>
<th style="text-align: right;">age</th>
<th style="text-align: left;">sex</th>
<th style="text-align: right;">alb</th>
<th style="text-align: right;">alp</th>
<th style="text-align: right;">alt</th>
<th style="text-align: right;">ast</th>
<th style="text-align: right;">bil</th>
<th style="text-align: right;">che</th>
<th style="text-align: right;">chol</th>
<th style="text-align: right;">crea</th>
<th style="text-align: right;">ggt</th>
<th style="text-align: right;">prot</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: right;">1</td>
<td style="text-align: left;">0=Blood Donor</td>
<td style="text-align: right;">32</td>
<td style="text-align: left;">m</td>
<td style="text-align: right;">38.5</td>
<td style="text-align: right;">52.5</td>
<td style="text-align: right;">7.7</td>
<td style="text-align: right;">22.1</td>
<td style="text-align: right;">7.5</td>
<td style="text-align: right;">6.93</td>
<td style="text-align: right;">3.23</td>
<td style="text-align: right;">106</td>
<td style="text-align: right;">12.1</td>
<td style="text-align: right;">69.0</td>
</tr>
<tr class="even">
<td style="text-align: right;">2</td>
<td style="text-align: left;">0=Blood Donor</td>
<td style="text-align: right;">32</td>
<td style="text-align: left;">m</td>
<td style="text-align: right;">38.5</td>
<td style="text-align: right;">70.3</td>
<td style="text-align: right;">18.0</td>
<td style="text-align: right;">24.7</td>
<td style="text-align: right;">3.9</td>
<td style="text-align: right;">11.17</td>
<td style="text-align: right;">4.80</td>
<td style="text-align: right;">74</td>
<td style="text-align: right;">15.6</td>
<td style="text-align: right;">76.5</td>
</tr>
<tr class="odd">
<td style="text-align: right;">3</td>
<td style="text-align: left;">0=Blood Donor</td>
<td style="text-align: right;">32</td>
<td style="text-align: left;">m</td>
<td style="text-align: right;">46.9</td>
<td style="text-align: right;">74.7</td>
<td style="text-align: right;">36.2</td>
<td style="text-align: right;">52.6</td>
<td style="text-align: right;">6.1</td>
<td style="text-align: right;">8.84</td>
<td style="text-align: right;">5.20</td>
<td style="text-align: right;">86</td>
<td style="text-align: right;">33.2</td>
<td style="text-align: right;">79.3</td>
</tr>
<tr class="even">
<td style="text-align: right;">4</td>
<td style="text-align: left;">0=Blood Donor</td>
<td style="text-align: right;">32</td>
<td style="text-align: left;">m</td>
<td style="text-align: right;">43.2</td>
<td style="text-align: right;">52.0</td>
<td style="text-align: right;">30.6</td>
<td style="text-align: right;">22.6</td>
<td style="text-align: right;">18.9</td>
<td style="text-align: right;">7.33</td>
<td style="text-align: right;">4.74</td>
<td style="text-align: right;">80</td>
<td style="text-align: right;">33.8</td>
<td style="text-align: right;">75.7</td>
</tr>
<tr class="odd">
<td style="text-align: right;">5</td>
<td style="text-align: left;">0=Blood Donor</td>
<td style="text-align: right;">32</td>
<td style="text-align: left;">m</td>
<td style="text-align: right;">39.2</td>
<td style="text-align: right;">74.1</td>
<td style="text-align: right;">32.6</td>
<td style="text-align: right;">24.8</td>
<td style="text-align: right;">9.6</td>
<td style="text-align: right;">9.15</td>
<td style="text-align: right;">4.32</td>
<td style="text-align: right;">76</td>
<td style="text-align: right;">29.9</td>
<td style="text-align: right;">68.7</td>
</tr>
<tr class="even">
<td style="text-align: right;">6</td>
<td style="text-align: left;">0=Blood Donor</td>
<td style="text-align: right;">32</td>
<td style="text-align: left;">m</td>
<td style="text-align: right;">41.6</td>
<td style="text-align: right;">43.3</td>
<td style="text-align: right;">18.5</td>
<td style="text-align: right;">19.7</td>
<td style="text-align: right;">12.3</td>
<td style="text-align: right;">9.92</td>
<td style="text-align: right;">6.05</td>
<td style="text-align: right;">111</td>
<td style="text-align: right;">91.0</td>
<td style="text-align: right;">74.0</td>
</tr>
</tbody>
</table>

Dataset Characteristics
-----------------------

-   Rows : 615
-   Columns : 14
-   Size : 46kb

Variables
---------

    ##  [1] "x"        "category" "age"      "sex"      "alb"      "alp"     
    ##  [7] "alt"      "ast"      "bil"      "che"      "chol"     "crea"    
    ## [13] "ggt"      "prot"

Table One
---------

<table style="width:100%;">
<colgroup>
<col style="width: 19%" />
<col style="width: 11%" />
<col style="width: 19%" />
<col style="width: 12%" />
<col style="width: 11%" />
<col style="width: 13%" />
<col style="width: 5%" />
<col style="width: 4%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;"></th>
<th style="text-align: left;">0=Blood Donor</th>
<th style="text-align: left;">0s=suspect Blood Donor</th>
<th style="text-align: left;">1=Hepatitis</th>
<th style="text-align: left;">2=Fibrosis</th>
<th style="text-align: left;">3=Cirrhosis</th>
<th style="text-align: left;">p</th>
<th style="text-align: left;">test</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">n</td>
<td style="text-align: left;">533</td>
<td style="text-align: left;">7</td>
<td style="text-align: left;">24</td>
<td style="text-align: left;">21</td>
<td style="text-align: left;">30</td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;">category (%)</td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;">&lt;0.001</td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;">0=Blood Donor</td>
<td style="text-align: left;">533 (100.0)</td>
<td style="text-align: left;">0 ( 0.0)</td>
<td style="text-align: left;">0 ( 0.0)</td>
<td style="text-align: left;">0 ( 0.0)</td>
<td style="text-align: left;">0 ( 0.0)</td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;">0s=suspect Blood Donor</td>
<td style="text-align: left;">0 ( 0.0)</td>
<td style="text-align: left;">7 (100.0)</td>
<td style="text-align: left;">0 ( 0.0)</td>
<td style="text-align: left;">0 ( 0.0)</td>
<td style="text-align: left;">0 ( 0.0)</td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;">1=Hepatitis</td>
<td style="text-align: left;">0 ( 0.0)</td>
<td style="text-align: left;">0 ( 0.0)</td>
<td style="text-align: left;">24 (100.0)</td>
<td style="text-align: left;">0 ( 0.0)</td>
<td style="text-align: left;">0 ( 0.0)</td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;">2=Fibrosis</td>
<td style="text-align: left;">0 ( 0.0)</td>
<td style="text-align: left;">0 ( 0.0)</td>
<td style="text-align: left;">0 ( 0.0)</td>
<td style="text-align: left;">21 (100.0)</td>
<td style="text-align: left;">0 ( 0.0)</td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;">3=Cirrhosis</td>
<td style="text-align: left;">0 ( 0.0)</td>
<td style="text-align: left;">0 ( 0.0)</td>
<td style="text-align: left;">0 ( 0.0)</td>
<td style="text-align: left;">0 ( 0.0)</td>
<td style="text-align: left;">30 (100.0)</td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;">age (mean (SD))</td>
<td style="text-align: left;">47.13 (9.62)</td>
<td style="text-align: left;">57.57 (11.07)</td>
<td style="text-align: left;">38.71 (11.35)</td>
<td style="text-align: left;">52.33 (11.44)</td>
<td style="text-align: left;">53.47 (8.91)</td>
<td style="text-align: left;">&lt;0.001</td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;">sex = m (%)</td>
<td style="text-align: left;">318 ( 59.7)</td>
<td style="text-align: left;">6 ( 85.7)</td>
<td style="text-align: left;">20 ( 83.3)</td>
<td style="text-align: left;">13 ( 61.9)</td>
<td style="text-align: left;">20 ( 66.7)</td>
<td style="text-align: left;">0.106</td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;">alb (mean (SD))</td>
<td style="text-align: left;">42.24 (5.03)</td>
<td style="text-align: left;">24.40 (10.56)</td>
<td style="text-align: left;">43.83 (3.51)</td>
<td style="text-align: left;">41.76 (3.74)</td>
<td style="text-align: left;">32.48 (5.82)</td>
<td style="text-align: left;">&lt;0.001</td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;">alp (mean (SD))</td>
<td style="text-align: left;">68.37 (18.23)</td>
<td style="text-align: left;">107.30 (52.82)</td>
<td style="text-align: left;">42.11 (23.52)</td>
<td style="text-align: left;">37.84 (8.31)</td>
<td style="text-align: left;">93.22 (80.05)</td>
<td style="text-align: left;">&lt;0.001</td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;">alt (mean (SD))</td>
<td style="text-align: left;">26.63 (14.50)</td>
<td style="text-align: left;">102.11 (119.84)</td>
<td style="text-align: left;">26.90 (22.59)</td>
<td style="text-align: left;">59.60 (66.70)</td>
<td style="text-align: left;">22.97 (36.36)</td>
<td style="text-align: left;">&lt;0.001</td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;">ast (mean (SD))</td>
<td style="text-align: left;">26.55 (10.62)</td>
<td style="text-align: left;">71.00 (53.32)</td>
<td style="text-align: left;">75.73 (68.78)</td>
<td style="text-align: left;">81.17 (42.26)</td>
<td style="text-align: left;">107.46 (74.64)</td>
<td style="text-align: left;">&lt;0.001</td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;">bil (mean (SD))</td>
<td style="text-align: left;">8.53 (6.09)</td>
<td style="text-align: left;">4.69 (3.11)</td>
<td style="text-align: left;">15.62 (13.47)</td>
<td style="text-align: left;">13.43 (5.33)</td>
<td style="text-align: left;">59.13 (69.37)</td>
<td style="text-align: left;">&lt;0.001</td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;">che (mean (SD))</td>
<td style="text-align: left;">8.40 (1.88)</td>
<td style="text-align: left;">7.48 (4.64)</td>
<td style="text-align: left;">9.28 (2.51)</td>
<td style="text-align: left;">8.33 (1.67)</td>
<td style="text-align: left;">3.82 (2.24)</td>
<td style="text-align: left;">&lt;0.001</td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;">chol (mean (SD))</td>
<td style="text-align: left;">5.49 (1.06)</td>
<td style="text-align: left;">4.45 (1.87)</td>
<td style="text-align: left;">5.10 (1.45)</td>
<td style="text-align: left;">4.60 (0.73)</td>
<td style="text-align: left;">4.01 (0.99)</td>
<td style="text-align: left;">&lt;0.001</td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;">crea (mean (SD))</td>
<td style="text-align: left;">78.98 (14.51)</td>
<td style="text-align: left;">61.71 (52.85)</td>
<td style="text-align: left;">73.96 (19.77)</td>
<td style="text-align: left;">73.49 (12.92)</td>
<td style="text-align: left;">138.22 (209.56)</td>
<td style="text-align: left;">&lt;0.001</td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;">ggt (mean (SD))</td>
<td style="text-align: left;">29.04 (24.74)</td>
<td style="text-align: left;">151.51 (133.77)</td>
<td style="text-align: left;">92.58 (116.67)</td>
<td style="text-align: left;">79.55 (47.76)</td>
<td style="text-align: left;">129.44 (138.04)</td>
<td style="text-align: left;">&lt;0.001</td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;">prot (mean (SD))</td>
<td style="text-align: left;">72.11 (4.55)</td>
<td style="text-align: left;">53.91 (11.71)</td>
<td style="text-align: left;">74.70 (6.08)</td>
<td style="text-align: left;">76.10 (5.05)</td>
<td style="text-align: left;">70.05 (7.93)</td>
<td style="text-align: left;">&lt;0.001</td>
<td style="text-align: left;"></td>
</tr>
</tbody>
</table>

Licence
-------

**Open Data Commons Open Database License (ODbL) v1.0**

Source
------

Source :
<a href="https://archive.ics.uci.edu/ml/datasets/HCV+data" class="uri">https://archive.ics.uci.edu/ml/datasets/HCV+data</a>
