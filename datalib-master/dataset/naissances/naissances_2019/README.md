### Naissances 2019

| Fichier | Description | 
| -------- | -------- | 
| FD_NAIS_2019.csv | Jeu de données   | 
| Contenu_etatcivil2019_nais2019.pdf | Documentation des variables     | 
| varmod_NAIS_2019.csv |  Description des valeurs des variables qualitatives |