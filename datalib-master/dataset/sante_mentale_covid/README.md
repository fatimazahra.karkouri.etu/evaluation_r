# Contexte
La crise sanitaire liée à l'épidémie du COVID-19 a entraîné une augmentation de l'anxiété et de la dépression chez de nombreuses personnes. Depuis le 23 mars 2020, Santé publique France a lancé l'enquête **CoviPrev** avec la société d'étude et de conseil BVA. Cette étude a plusieurs objectifs : 
- suivre l'adoption des mesures de protection et de la santé de la population pendant les périodes de confinement et déconfinement. 
- Recueillir les informations nécessaires à l'orientation et l'ajustement des mesures de prévention.
- Surveiller les inégalités de santé.
- Capitaliser des connaissances en prévision de futures pandémies.

# Présentation du jeu de données

Les données mises à disposition ici décrivent pour l’échelle régionale et nationale:

-   Les prévalences de l'anxiété dans le contexte de l’épidémie de Covid-19 ;
-   Les prévalences des problèmes de sommeil dans le contexte de l’épidémie de Covid-19 ;
-   Les prévalences de la dépression dans le contexte de l’épidémie de Covid-19 ;
-   Les prévalences de l'adoption systématique des 3m de distanciation physique ;
-   La prévalence de l'adoption systématique des 4 mesures d’hygiène : *Se laver régulièrement les mains ; Saluer sans serrer la main et arrêter les embrassades ; Tousser dans son coude ; Utiliser un mouchoir à usage unique.*
-   La prévalences de l'adoption systématique du port du masque en public pendant l’épidémie de Covid-19 ;
-   La prévalence de l'adoption systématique de la distance d’1 mètre
-   Le nombre moyen de mesures de prévention adoptées systématiquement (sur 7 mesures) : *Se laver régulièrement les mains ; Saluer sans serrer la main ; Tousser dans son coude ; Utiliser un mouchoir à usage unique ; Rester confiné à la maison ; Limiter toutes formes d'interactions ; Garder une distance d'au moins un mètre.*
-   Le nombre moyen de mesures d’hygiène adoptées systématiquement (sur 4 mesures).

L'anxiété et la dépression ont été mesurées avec l'échelle **HAD** (Hospitality Anxiety and Depression scale ; score > 10). 

Au total, le jeu de données est composé de **6 fichiers CSV**  comportant portant sur l'anxiété, la dépression, les problèmes de sommeil relativement aux mesures d'hygiènes, de distanciation sociales et de port du masque, pour l'ensemble des vagues pour des populations différentes : 
| Nom du fichier | Description | Taille |
|:--------------:|:------------:|:-----:|
| coviprev-france.csv | Données pour l'ensemble de la France (jusqu'à vague semaine 16)|16 observations x 29 variables |
| coviprev-age.csv | Données par tranche d'âge (jusqu'à vague semaine 16) | 88 observations x 29 variables |
|coviprev-region.csv | Données par régions (jusqu'à vague semaine 16) | 192 observations X 29 variables |
|coviprev-sexe.csv| Données par sexe (jusqu'à vague semaine 16) | 33 observations x 29 variables | 
|metadata-coviprev-libelle-region.csv | Fichier de correspondance, indiquant le code attribué par région |19 observations x 2 variables |
|metadata-coviprev.csv | Fichier d'explication des variables |24 observations x 6 variables |

**Aperçu des données**
1. ***coviprev-france.csv***

| fra | semaine| anxiete | anxiete_inf | anxiete_sup | depression | depression_inf | depression_sup | pbsommeil | pbsommeil_inf | pbsommeil_sup | hyg4mes | hyg4mes_inf | hyg4mes_sup | dist3mes | dist3mes_inf | dist3mes_sup | nbmoy7mes | nbmoy7mes_inf | nbmoy7mes_sup | nbmoy4mesHyg |nbmoy4mesHyg_inf | nbmoy4mesHyg_sup | dist1m | dist1m_inf | dist1m_sup | portmasque | portmasque_inf | portmasque_sup |
|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|--|---|---|---|---|---|---|---|---|---|---|
| fr | Vague1:23-25mars | 26.7 | 24.8 | 28.7 | NA | NA | NA | NA | NA | NA | NA | NA | NA | NA | NA | NA | NA | NA | NA | NA | NA | NA | NA | NA | NA | NA | NA | NA | 
| fr | Vague2:30mars-1avr | 21.5 | 19.8 | 23.4 | 19.9 | 18.2 | 21.7 | 61.3 | 59.1 | 63.4 | 49.9 | 47.7 | 52.2 | 70.3 | 68.3 | 72.3 | 5.66 | 5.58 | 5.73 | 3.1 | 3.05 | 3.15 | NA | NA | NA | 15.1 | 13.6 | 16.8 | 
| fr | Vague3:14-16avr | 18.1 | 16.5 | 19.9 | 17.6 | 16 | 19.4 | 63.7 | 61.5 | 65.7 | 46.8 | 44.6 | 49 | 65.2 | 63.1 | 67.3 | 5.48 | 5.4 | 5.55 | 2.99 | 2.94 | 3.04 | NA | NA | NA | 24 | 22.1 | 25.9 | 
fr | Vague4:20-22avr | 18.9 | 17.2 | 20.7 | 20.4 | 18.7 | 22.3 | 66.8 | 64.7 | 68.9 | 45.9 | 43.7 | 48.1 | 63.1 | 60.9 | 65.2 | 5.4 | 5.32 | 5.49 | 2.98 | 2.93 | 3.03 | NA | NA | NA | 27.6 | 25.7 | 29.6 | 


2.  ***coviprev-age.csv***

| age | semaine| anxiete | anxiete_inf | anxiete_sup | depression | depression_inf | depression_sup | pbsommeil | pbsommeil_inf | pbsommeil_sup | hyg4mes | hyg4mes_inf | hyg4mes_sup | dist3mes | dist3mes_inf | dist3mes_sup | nbmoy7mes | nbmoy7mes_inf | nbmoy7mes_sup | nbmoy4mesHyg |nbmoy4mesHyg_inf | nbmoy4mesHyg_sup | dist1m | dist1m_inf | dist1m_sup | portmasque | portmasque_inf | portmasque_sup |
|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|--|---|---|---|---|---|---|---|---|---|---|
| 18-24 | Vague2:30mars-1avr | 22.5 | 16.8 | 29.5 | 16.5 | 11.7 | 22.9 | 68.5 | 61.1 | 75 | 38 | 31 | 45.6 | 55.9 | 48.4 | 63.2 | 5.13 | 4.84 | 5.42 | 2.8 | 2.62 | 2.99 | NA | NA | NA | 13.7 | 9.3 | 19.9 |
| 25-34 | Vague2:30mars-1avr | 25.9 | 21.2 | 31.3 | 23.5 | 19 | 28.8 | 64.6 | 58.9 | 69.9 | 48.5 | 42.8 | 54.2 | 66.6 | 61 | 71.8 | 5.53 | 5.33 | 5.74 | 3.06 | 2.93 | 3.2 | NA | NA | NA | 13.6 | 10.1 | 18.1 | 
| 35-49 | Vague2:30mars-1avr | 25 | 21.5 | 28.9 | 23.7 | 20.2 | 27.5 | 68.4 | 54.3 | 72.2 | 49.9 | 45.6 | 54.2 | 68.9 | 64.9 | 72.7 | 5.6 | 5.46 | 5.75 | 3.08 | 2.98 | 3.18 | NA | NA | NA | 18.2 | 15.1 | 21.8 |
| 50-64 | Vague2:30mars-1avr | 18.4 | 15.3 | 21.9 | 19.5 | 16.3 | 23.1 | 56.5 | 52.2 | 60.7 | 54.4 | 50.1 | 58.6 | 70.9 | 66.8 | 74.7 | 5.77 | 5.63 | 5.9 | 3.19 | 3.1 | 3.28 | NA | NA | NA | 15.2 | 12.4 | 18.6 |


3. ***coviprev-region.csv***

| reg | semaine| anxiete | anxiete_inf | anxiete_sup | depression | depression_inf | depression_sup | pbsommeil | pbsommeil_inf | pbsommeil_sup | hyg4mes | hyg4mes_inf | hyg4mes_sup | dist3mes | dist3mes_inf | dist3mes_sup | nbmoy7mes | nbmoy7mes_inf | nbmoy7mes_sup | nbmoy4mesHyg |nbmoy4mesHyg_inf | nbmoy4mesHyg_sup | dist1m | dist1m_inf | dist1m_sup | portmasque | portmasque_inf | portmasque_sup |
|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|--|---|---|---|---|---|---|---|---|---|---|
| 11 | Vague2:30mars-1avr | 19.4 | 15.6 | 23.9 | 20.6 | 16.7 | 25.1 | 63.5 | 58.4 | 68.4 | 44.9 | 39.7 | 50.1 | 67 | 61.9 | 71.7 | 5.61 | 5.45 | 5.77 | 3.06 | 2.94 | 3.17 | NA | NA | NA | 16.2 | 12.7 | 20.5 | 
| 24 | Vague2:30mars-1avr | 22.4 | 14.4 | 33.2 | 27.7 | 18.8 | 38.8 | 61.7 | 50.4 | 71.9 | 52.3 | 41.1 | 63.3 | 69.5 | 58.2 | 78.9 | 5.59 | 5. | 18 | 6 | 3.12 | 2.86 | 3.38 | NA | NA | NA | 11.9 | 6.3 | 21.5 | 
| 27 | Vague2:30mars-1avr | 17.6 | 10.9 | 27.3 | 17.8 | 11.1 | 27.2 | 55 | 44.6 | 65.1 | 55.8 | 45.3 | 65.8 | 65.2 | 54.7 | 74.4 | 5.66 | 5.32 | 6.01 | 3.18 | 2.95 | 3.41 | NA | NA | NA | 21.1 | 13.9 | 30.9 | 
| 28 | Vague2:30mars-1avr | 22.8 | 15.7 | 31.8 | 19 | 12.6 | 27.7 | 60.9 | 51.2 | 69.8 | 44.6 | 35.4 | 54.2 | 68.8 | 59.4 | 77 | 5.55 | 5.23 | 5.86 | 2.98 | 2.76 | 3.2 | NA | NA | NA | 9.3 | 5 | 16.6 | 

4. ***coviprev-sexe.csv***

| sexe | semaine| anxiete | anxiete_inf | anxiete_sup | depression | depression_inf | depression_sup | pbsommeil | pbsommeil_inf | pbsommeil_sup | hyg4mes | hyg4mes_inf | hyg4mes_sup | dist3mes | dist3mes_inf | dist3mes_sup | nbmoy7mes | nbmoy7mes_inf | nbmoy7mes_sup | nbmoy4mesHyg |nbmoy4mesHyg_inf | nbmoy4mesHyg_sup | dist1m | dist1m_inf | dist1m_sup | portmasque | portmasque_inf | portmasque_sup |
|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|--|---|---|---|---|---|---|---|---|---|---|
| 0 | Vague2:30mars-1avr | 16.6 | 14.4 | 19.1 | 18.5 | 16.2 | 21.1 | 54.2 | 51 | 57.4 | 41.5 | 38.4 | 44.7 | 65.7 | 62.6 | 68.7 | 5.35 | 5.24 | 5.47 | 2.88	 | 2.8 | 2.95 | NA | NA | NA | 14.2 | 12.1 | 16.6 | 
| 1 | Vague2:30mars-1avr | 26 | 23.4 | 28.7 | 21.1 | 18.7 | 23.7 | 67.8 | 64.8 | 70.6 | 57.6 | 54.6 | 60.6 | 74.5 | 71.8 | 77.1 | 5.94 | 5.85 | 6.03 | 3.3 | 3.24 | 3.36 | NA | NA | NA | 16 | 13.9 | 18.3 | 
|0 | Vague3:14-16avr  | 14.4 | 12.3 | 16.8 | 15.3 | 13.1 | 17.7 | 57.2 | 54 | 60.3 | 38 | 34.9 | 41.1 | 59.2 | 56 | 62.3 | 5.12 | 5.01 | 5.24 | 2.75 | 2.67 | 2.83 | NA | NA | NA | 20 | 17.6 | 22.7  | 
| 1 | Vague3:14-16avr | 21.5 | 19.1 | 24.1 | 19.7 | 17.4 | 22.3 | 69.5 | 66.7 | 72.3 | 54.8 | 51.7 | 57.8 | 70.7 | 67.8 | 73.4 | 5.79 | 5.7 | 5.89 | 3.21 | 3.15 | 3.28 | NA | NA | NA | 27.5 | 24.9 | 30.3  | 



Les données proviennent de Santé publique France et ont été récupérées sur le site **data.gouv** : https://www.data.gouv.fr/fr/datasets/donnees-denquete-relatives-a-levolution-des-comportements-et-de-la-sante-mentale-pendant-lepidemie-de-covid-19-coviprev/

**License** : Licence Ouverte / Open Licence

# Présentation des variables
| Nom de la variable | Description | Type |
|:------------------:|:-----------:|:----:|
| semaine |Semaines correspondant aux différentes vagues | character|
| anxiete |Anxiété mesurée par l'échelle HAD|numeric |
| anxiete_inf |Valeur inférieure (IC à 95%) de l'anxiété |numeric|
| anxiete_sup |Valeur supérieure (IC à 95%) de l'anxiété |numeric|
| depression |Dépression mesurée par l'échelle HAD|numeric|
| depression_inf |Valeur inférieure (IC à 95%) de la dépression |numeric|
| depression_sup |Valeur supérieure (IC à 95%) de la dépression |numeric|
| pbsommeil |Prévalence des personnes ayant des problèmes de sommeil |numeric|
| pbsommeil_inf |Valeur inférieure (IC à 95%) des problèmes de sommeil |numeric|
| pbsommeil_sup |Valeur supérieure (IC à 95%) des problèmes de sommeil |numeric|
| hyg4mes | Prévalence de l'adoption systématique des 4 mesures d’hygiène (lavage régulier des mains, limiter les contacts physique, tousser dans son coude, utiliser un mouchoir à usage unique | numeric |
| hyg4mes_inf |Valeur inférieure (IC à 95%) de l'adoption des mesures d'hygiène|numeric|
| hyg4mes_sup | Valeur inférieure (IC à 95%) de l’adoption des mesures d’hygiènes |numeric|
|dist3mes |Prévalence de l'adoption systématique de la distance de trois mètres |numeric |
|dist3mes_inf |Valeur inférieure (IC à 95%) de l'adoption systématique de la distance d'3m |numeric |
|dist3mes_sup |Valeur supérieure (IC à 95%) de l'adoption systématique de la distance d'3m |numeric |
|nbmoy7mes|Nombre moyen de mesures adoptées systématiquement (sur 7 mesures) | numeric |
|nbmoy7mes_inf | Valeur inférieure (IC à 95%) de l'adoption systématique des 7 mesures |numeric |
|nbmoy7mes_sup |  Valeur supérieure (IC à 95%) de l'adoption systématique des 7 mesures | numeric |
|nbmoy4mesHyg | Nombre moyen de mesures adoptées systématiquement (sur 4 mesures) |numeric|
|nbmoy4mesHyg_inf | Valeur inférieure (IC à 95%) de l'adoption systématique des 4 mesures | numeric |
|nbmoy4mesHyg_sup |Valeur inférieure (IC à 95%) de l'adoption systématique des 4 mesures | numeric |
|dist1m| Prévalence de l'adoption systématique de la distance d’un mètre |numeric |
|dist1m_inf |Valeur inférieure (IC à 95%) de l'adoption systématique de la distance d'1m |numeric |
|dist1m_sup |Valeur supérieure (IC à 95%) de l'adoption systématique de la distance d'1m |numeric |
|portmasque |Prévalence de l'adoption systématique du masque en public | numeric
|portmasque_inf | Valeur inférieure (IC à 95%) du port du masque en public | numeric
|portmasque_sup |Valeur supérieure (IC à 95%) de l'adoption systématique du port du masque en public | numeric | 
|age |intervalle des âges des individus |character|
|reg | Région | character |
|sexe | Sexe de l'individu (0 - Homme ; 1 - Femme) |character |


# Statistiques descriptives

## Visualisation des variables liées à la santé mentale pour la table région
![statistique_sante_mentale](./statistiques_descriptives/statistique_sante_mentale.PNG)

## Visualisation des variables liées aux mesures sanitaires gouvernementales pour la table région 
![statistique_mesuresGouv_reg](./statistiques_descriptives/statistique_mesuresGouv_reg.PNG)


# Modifications apportées 
- Élimination de colonnes vides
- Remplacement des cases vides par "NA"
- Élimination du terme "âge" dans les intervalles de variables *age*
- Réencodage des variables descrivant le sexe
- Redéfinition du type de certaines variables (région : character )
