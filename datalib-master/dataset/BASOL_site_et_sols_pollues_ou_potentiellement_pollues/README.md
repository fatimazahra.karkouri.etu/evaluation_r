# **Sites et sols pollués (ou potentiellement pollués) - Echelle Nationale**

## Description du fichier

<table>
<colgroup>
<col style="width: 4%" />
<col style="width: 82%" />
<col style="width: 12%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">Description</th>
<th style="text-align: center;">Ce jeu de données correspond aux sites et sols pollués, ou potentiellement pollués (BASOL)</th>
<th><a href="https://www.georisques.gouv.fr/articles-risques/basol" class="uri">https://www.georisques.gouv.fr/articles-risques/basol</a></th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">Source / Téléchargement</td>
<td style="text-align: center;">BASOL (disponible à l’échelle nationale, régionale, départementale)</td>
<td><a href="https://www.georisques.gouv.fr/donnees/bases-de-donnees/sites-et-sols-pollues-ou-potentiellement-pollues" class="uri">https://www.georisques.gouv.fr/donnees/bases-de-donnees/sites-et-sols-pollues-ou-potentiellement-pollues</a></td>
</tr>
<tr class="even">
<td style="text-align: left;">Producteur</td>
<td style="text-align: center;">Mise à disposition par le ministère chargé de l’environnement, disponible sur Géorisques</td>
<td><a href="https://www.georisques.gouv.fr/" class="uri">https://www.georisques.gouv.fr/</a></td>
</tr>
<tr class="odd">
<td style="text-align: left;">Licence</td>
<td style="text-align: center;">Open data</td>
<td></td>
</tr>
<tr class="even">
<td style="text-align: left;">Format</td>
<td style="text-align: center;">Fichier CSV (csv2)</td>
<td></td>
</tr>
<tr class="odd">
<td style="text-align: left;">Encodage</td>
<td style="text-align: center;">UTF-8</td>
<td></td>
</tr>
<tr class="even">
<td style="text-align: left;">Date de Création</td>
<td style="text-align: center;">Inventaire BASOL depuis 1994</td>
<td></td>
</tr>
<tr class="odd">
<td style="text-align: left;">Date de téléchargement du fichier</td>
<td style="text-align: center;">06/10/2021</td>
<td></td>
</tr>
<tr class="even">
<td style="text-align: left;">Fréquence de mise à jour</td>
<td style="text-align: center;">Quotidiennement</td>
<td><a href="https://www.georisques.gouv.fr/donnees/bases-de-donnees/sites-et-sols-pollues-ou-potentiellement-pollues" class="uri">https://www.georisques.gouv.fr/donnees/bases-de-donnees/sites-et-sols-pollues-ou-potentiellement-pollues</a></td>
</tr>
<tr class="odd">
<td style="text-align: left;">Couverture territoriale</td>
<td style="text-align: center;">FRANCE, fichier à l’échelle Nationale (disponible également à l’échelle Régionale et Départementale)</td>
<td></td>
</tr>
<tr class="even">
<td style="text-align: left;">Granularité</td>
<td style="text-align: center;">Point d’intérêt</td>
<td></td>
</tr>
<tr class="odd">
<td style="text-align: left;">Raison / Méthode de création</td>
<td style="text-align: center;">Comme la plupart des pays industrialisés, la France a hérité d’un long passé industriel durant lequel les préoccupations et les contraintes environnementales n’étaient pas celles d’aujourd’hui. Les conséquences du déversement des produits et des pollutions dans l’eau, dans l’air et/ou dans les sols n’étaient alors pas ou peu connues […]. La nécessité de connaître les sites pollués (ou potentiellement pollués), de les traiter le cas échéant, en lien notamment avec l’usage prévu, d’informer le public et les acteurs locaux, d’assurer la traçabilité des pollutions et des risques y compris après traitement a conduit le ministère chargé de l’environnement à créer la base de données BASOL.</td>
<td><a href="https://www.georisques.gouv.fr/articles-risques/basol#pourquoi-basol--" class="uri">https://www.georisques.gouv.fr/articles-risques/basol#pourquoi-basol--</a></td>
</tr>
</tbody>
</table>

## Intérêt et possibilités

-   Cartographies et statistiques par communes / départements / régions

-   Calcul de densité de population vivant à proximité de zone polluée
    (en croisant d’autres jeux de données)

-   Prévalence et localisation de certaines maladie par rapport à
    position de ces sites

-   Etudes épidémiologiques

-   …

## Description des données

### Variables

<table>
<colgroup>
<col style="width: 9%" />
<col style="width: 34%" />
<col style="width: 8%" />
<col style="width: 46%" />
</colgroup>
<thead>
<tr class="header">
<th>Champ</th>
<th>Libellé</th>
<th>Type</th>
<th>Exemple</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>code_metier</td>
<td>Non renseigné</td>
<td>character</td>
<td>“SSP001106401” “SSP001254301” “SSP000761401” “SSP000997101” …</td>
</tr>
<tr class="even">
<td>nom_usuel</td>
<td>Non renseigné</td>
<td></td>
<td>NA NA NA NA NA NA …</td>
</tr>
<tr class="odd">
<td>adresse</td>
<td>Adresse du site concerné</td>
<td>character</td>
<td>“” “RUE ANDRÉ BELLEMARE” “” “USINE” …</td>
</tr>
<tr class="even">
<td>code_postal</td>
<td>Code postal du site concerné</td>
<td>integer</td>
<td>67810 61190 57260 40410 33430 8500 73600 70100 88370 33123 …</td>
</tr>
<tr class="odd">
<td>code_insee</td>
<td>Code insee du site concerné</td>
<td>character</td>
<td>“67212” “61491” “57177” “40200” …</td>
</tr>
<tr class="even">
<td>code_dpt</td>
<td>Code département du site concerné</td>
<td>character</td>
<td>“67” “61” “57” “40” …</td>
</tr>
<tr class="odd">
<td>code_reg</td>
<td>Code région du site concerné</td>
<td>integer</td>
<td>44 28 44 75 75 44 84 27 44 75 …</td>
</tr>
<tr class="even">
<td>nom_commune</td>
<td>Nom de la commune intégrant le site concerné</td>
<td>character</td>
<td>“HOLTZHEIM” “TOUROUVRE” “DIEUZE” “MOUSTEY” …</td>
</tr>
<tr class="odd">
<td>x_wgs84</td>
<td>Coordonnées x avec le système géodésique WGS84</td>
<td>character</td>
<td>“7.6636333” “0.67494625” “6.71518” “-0.7514216” …</td>
</tr>
<tr class="even">
<td>y_wgs84</td>
<td>Coordonnées y avec le système géodésique WGS84</td>
<td>character</td>
<td>“48.55083” “48.64802” “48.817417” “44.383003” …</td>
</tr>
</tbody>
</table>

### Dimensions / analyses descriptives (au 06/10/2021)

-   Nombre de sites concernés au niveau national : 4082

-   Nombre de départements concernés : 99

-   Nombre de régions concernées : 17

![Nombre de sites et sols pollués par code
région](https://gitlab.univ-lille.fr/master_dss/datalib/-/blob/master/dataset/BASOL_site_et_sols_pollues_ou_potentiellement_pollues/Rplot.png "Nombre de sites et sols pollués par code région")

## Extrait de la base de données

<table>
<colgroup>
<col style="width: 10%" />
<col style="width: 8%" />
<col style="width: 16%" />
<col style="width: 10%" />
<col style="width: 9%" />
<col style="width: 7%" />
<col style="width: 7%" />
<col style="width: 10%" />
<col style="width: 10%" />
<col style="width: 8%" />
</colgroup>
<thead>
<tr class="header">
<th>code_metier</th>
<th>nom_usuel</th>
<th>adresse</th>
<th>code_postal</th>
<th>code_insee</th>
<th>code_dpt</th>
<th>code_reg</th>
<th>nom_commune</th>
<th>x_wgs84</th>
<th>y_wgs84</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>SSP001106401</td>
<td>NA</td>
<td></td>
<td>67810</td>
<td>67212</td>
<td>67</td>
<td>44</td>
<td>HOLTZHEIM</td>
<td>7.6636333</td>
<td>48.55083</td>
</tr>
<tr class="even">
<td>SSP001254301</td>
<td>NA</td>
<td>RUE ANDRÉ BELLEMARE</td>
<td>61190</td>
<td>61491</td>
<td>61</td>
<td>28</td>
<td>TOUROUVRE</td>
<td>0.67494625</td>
<td>48.64802</td>
</tr>
<tr class="odd">
<td>SSP000761401</td>
<td>NA</td>
<td></td>
<td>57260</td>
<td>57177</td>
<td>57</td>
<td>44</td>
<td>DIEUZE</td>
<td>6.71518</td>
<td>48.817417</td>
</tr>
<tr class="even">
<td>SSP000997101</td>
<td>NA</td>
<td>USINE</td>
<td>40410</td>
<td>40200</td>
<td>40</td>
<td>75</td>
<td>MOUSTEY</td>
<td>-0.7514216</td>
<td>44.383003</td>
</tr>
<tr class="odd">
<td>SSP001037301</td>
<td>NA</td>
<td></td>
<td>33430</td>
<td>33036</td>
<td>33</td>
<td>75</td>
<td>BAZAS</td>
<td>-0.21031913</td>
<td>44.443077</td>
</tr>
</tbody>
</table>

### Améliorations

Les données brutes présentant certains défauts, un scrip R permettant
d’améliorer le jeu de donnée amélioré est proposé ci-dessous :

-   Typage de certaines variables auparavant incorrecte

-   Suppression de valeurs inutiles

<!-- -->

    library(tidyverse)
    df <- read.csv2("Sites et sols pollués (ou potentiellement pollués) - Echelle Nationale.csv", encoding = "UTF-8") %>%
      select(-nom_usuel) %>% #variable vide
      mutate(code_postal = as.integer(code_postal)) %>%
      mutate(code_insee = as.integer(code_insee)) %>%
      mutate(code_dpt = as.integer(code_dpt)) %>%
      mutate(code_reg = as.integer(code_reg)) %>%
      mutate(x_wgs84 = as.numeric(x_wgs84)) %>%
      mutate(y_wgs84 = as.numeric(y_wgs84)) 
