# **Géod’AIR - Qualité de l’air au quotidien**

## Description du fichier

<table>
<colgroup>
<col style="width: 6%" />
<col style="width: 84%" />
<col style="width: 8%" />
</colgroup>
<tbody>
<tr class="odd">
<td style="text-align: left;">Description</td>
<td style="text-align: center;"><p>Geod’air (GEstion des données d’Observation de la qualité de l’AIR)</p>
<p>Base de données nationale de référence de la qualité de l’air. Sur ce Git, le fichier concerne les moyenne de concentration d’Ozone (O3) pour la journée du 07/10/2021 mais il est possible de télécharger les concentrations d’autres polluants pour d’autres jours dans la base (avec la possibilité d’avoir les concentrations moyenne horaire, journalières etc…) Polluants disponibles : benzène, monoxyde de carbone, dioxyde d’azote, oxydes d’azote, ozone, dioxyde de soufre, PM10, PM2.5</p></td>
<td><a href="https://www.geodair.fr/" class="uri">https://www.geodair.fr/</a></td>
</tr>
<tr class="even">
<td style="text-align: left;">Source / Téléchargement</td>
<td style="text-align: center;">Disponible en export csv sur Geodair pour différents polluants et différentes journées</td>
<td><a href="https://www.geodair.fr/donnees/consultation" class="uri">https://www.geodair.fr/donnees/consultation</a></td>
</tr>
<tr class="odd">
<td style="text-align: left;">Producteur</td>
<td style="text-align: center;">Gérée et mise en œuvre par l’Ineris au titre du Laboratoire central de surveillance de la qualité de l’air (LCSQA)</td>
<td><a href="https://www.geodair.fr/a-propos" class="uri">https://www.geodair.fr/a-propos</a></td>
</tr>
<tr class="even">
<td style="text-align: left;">Licence</td>
<td style="text-align: center;">Open data</td>
<td></td>
</tr>
<tr class="odd">
<td style="text-align: left;">Format</td>
<td style="text-align: center;">Fichier CSV (csv2)</td>
<td></td>
</tr>
<tr class="even">
<td style="text-align: left;">Encodage</td>
<td style="text-align: center;">UTF-8</td>
<td></td>
</tr>
<tr class="odd">
<td style="text-align: left;">Date de Création</td>
<td style="text-align: center;">2013</td>
<td></td>
</tr>
<tr class="even">
<td style="text-align: left;">Date de téléchargement du fichier</td>
<td style="text-align: center;">07/10/2021</td>
<td></td>
</tr>
<tr class="odd">
<td style="text-align: left;">Fréquence de mise à jour</td>
<td style="text-align: center;">Quotidienne. /!\ d’après le site “les données d’une année peuvent être considérées comme stables en juin de l’année suivante”</td>
<td></td>
</tr>
<tr class="even">
<td style="text-align: left;">Couverture territoriale</td>
<td style="text-align: center;">Territoire National</td>
<td></td>
</tr>
<tr class="odd">
<td style="text-align: left;">Granularité</td>
<td style="text-align: center;">Point d’Intérêt (station de mesure)</td>
<td></td>
</tr>
<tr class="even">
<td style="text-align: left;">Raison / Méthode de création</td>
<td style="text-align: center;">Les informations sur le dispositif de surveillance ainsi que les statistiques de concentration et indicateurs sur la qualité de l’air  contenus dans Geod’air constituent les données de référence pour élaborer le Bilan de la qualité de l’air extérieur en France, qui est établi chaque année par le ministère chargé de l’environnement. Elles sont également transmises par le LCSQA à la Commission européenne, conformément aux obligations de déclaration des Etats membres.</td>
<td><a href="https://www.geodair.fr/a-propos" class="uri">https://www.geodair.fr/a-propos</a></td>
</tr>
</tbody>
</table>

Schéma du fonctionnement de GEODAIR pour la création et l’utilisation
des données :

![](Geodair/schema_fonct_geodair.png)

Capture d’écran de l’outil en ligne permettant d’exporter les données

![](Geodair/outil_export_data.png)

## Intérêt et possibilités

-   Cartographies et statistiques des concentrations de polluants
    atmosphériques

-   Indicence des maladies en fonction de la concentration annuelle des
    polluants dans chaque région / commune

-   Evolution du nombre de séjours aux urgences suite à un pic de
    pollution

-   Etude épidémiologique santé-environnement

-   …

## Description des données

### Variables du fichier téléchargé

<table>
<colgroup>
<col style="width: 7%" />
<col style="width: 51%" />
<col style="width: 3%" />
<col style="width: 36%" />
</colgroup>
<thead>
<tr class="header">
<th>Champ</th>
<th>Libellé</th>
<th>Type</th>
<th>Exemple</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>X.U.FEFF.Date.de.début</td>
<td>Date de début de la mesure</td>
<td>character</td>
<td>“2021/10/07 00:00:00” “2021/10/07 00:00:00” “2021/10/07 00:00:00” “2021/10/07 00:00:00”</td>
</tr>
<tr class="even">
<td>Date.de.fin</td>
<td>Date de fin de la mesure</td>
<td>character</td>
<td>“2021/10/07 12:00:00” “2021/10/07 12:00:00” “2021/10/07 12:00:00” “2021/10/07 12:00:00” …</td>
</tr>
<tr class="odd">
<td>Organisme</td>
<td>Nom de l’Association agréée de surveillance de la qualité de l’air (AASQA) responsable du site de mesure et de la réalisation des mesure</td>
<td>character</td>
<td>“ATMO GRAND EST” “ATMO GRAND EST” “ATMO GRAND EST” “ATMO GRAND EST” …</td>
</tr>
<tr class="even">
<td>code.zas</td>
<td>code de la zone administrative de surveillance dans laquelle se trouve le point de prélèvement</td>
<td>character</td>
<td>“FR44ZAG02” “FR44ZAG02” “FR44ZAG02” “FR44ZAG02” …</td>
</tr>
<tr class="odd">
<td>Zas</td>
<td>nom de la zone administrative de surveillance dans laquelle se trouve le point de prélèvement</td>
<td>character</td>
<td>“ZAG METZ” “ZAG METZ” “ZAG METZ” “ZAG METZ” …</td>
</tr>
<tr class="even">
<td>code.site</td>
<td>Code du site de mesure</td>
<td>character</td>
<td>“FR01011” “FR01018” “FR01020” “FR01021” …</td>
</tr>
<tr class="odd">
<td>nom.site</td>
<td>Nom du site de mesure</td>
<td>character</td>
<td>“Metz-Centre” “Scy-Chazelles” “Thionville-Centre” “Thionville-Garche” …</td>
</tr>
<tr class="even">
<td>type.d.implantation</td>
<td>Type d’implantation du site de mesure - station rurale nationale, rurale régionale, rurale près des villes, périurbaine, urbaine</td>
<td>character</td>
<td>“Urbaine” “Périurbaine” “Urbaine” “Périurbaine” …</td>
</tr>
<tr class="odd">
<td>Polluant</td>
<td>Polluant mesuré</td>
<td>character</td>
<td>“O3” “O3” “O3” “O3” …</td>
</tr>
<tr class="even">
<td>type.d.influence</td>
<td>Type d’influence pour le polluant considéré - influence industrielle, influence du trafic, fond (la concentration résulte de la contribution de différentes sources)</td>
<td>character</td>
<td>“Fond” “Fond” “Fond” “Fond” …</td>
</tr>
<tr class="odd">
<td>discriminant</td>
<td>code discriminant de la configuration -variable utilisée dans la base de données, non utile à l’exploitation du fichier d’export</td>
<td>characet</td>
<td>“A” "" “A” “A” …</td>
</tr>
<tr class="even">
<td>Réglementaire</td>
<td>caractère réglementaire de la donnée (oui/non)</td>
<td>character</td>
<td>“Oui” “Oui” “Oui” “Oui” …</td>
</tr>
<tr class="odd">
<td>type.d.évaluation</td>
<td>type d’évaluation correspondant à la mesure fixe - indicative ou estimation objective</td>
<td>character</td>
<td>“mesures fixes” “mesures fixes” “mesures fixes” “mesures fixes” …</td>
</tr>
<tr class="even">
<td>procédure.de.mesure</td>
<td>procédure de mesure variable utilisée dans la base de données - non utile à l’exploitation du fichier d’export</td>
<td>logical</td>
<td>NA NA NA NA NA NA …</td>
</tr>
<tr class="odd">
<td>type.de.valeur</td>
<td>type de la donnée ou statistique</td>
<td>character</td>
<td>“Max. journalier moy. hor.” “Max. journalier moy. hor.” “Max. journalier moy. hor.” “Max. journalier moy. hor.” …</td>
</tr>
<tr class="even">
<td>valeur</td>
<td>Valeur arrondie</td>
<td>character</td>
<td>“46.1” “91.3” “60.4” “77.2” …</td>
</tr>
<tr class="odd">
<td>valeur.brute</td>
<td>Valeur Brute</td>
<td>character</td>
<td>“46.1” “91.3” “60.4” “77.2” …</td>
</tr>
<tr class="even">
<td>unité.de.mesure</td>
<td>unité de mesure de la donnée unité dans laquelle les données de mesure sont exprimées</td>
<td>character</td>
<td>“µg-m3” “µg-m3” “µg-m3” “µg-m3” …</td>
</tr>
<tr class="odd">
<td>taux.de.saisie</td>
<td>pourcentage de données valides sur la période de mesure disponible uniquement pour certaines statistiques</td>
<td>character</td>
<td>“92.0” “100.0” “92.0” “100.0” …</td>
</tr>
<tr class="even">
<td>couverture.temporelle</td>
<td>pourcentage de la période couverte par des mesures disponible uniquement pour certaines statistiques</td>
<td>integer</td>
<td>100 100 100 100 100 100 100 100 100 100 …</td>
</tr>
<tr class="odd">
<td>couverture.de.données</td>
<td>pourcentage de données valides sur la période (produit du taux de saisie et de la couverture temporelle) -disponible uniquement pour certaines statistiques</td>
<td>character</td>
<td>“92.0” “100.0” “92.0” “100.0” …</td>
</tr>
<tr class="even">
<td>code.qualité</td>
<td>code qualité de la mesure (A,R,I, ..) variable utilisée pour la gestion des données en base - non utile à l’exploitation du fichier d’export</td>
<td>character</td>
<td>“R” “A” “A” “A” …</td>
</tr>
<tr class="odd">
<td>validité</td>
<td>validité de la mesure &gt;=1 si valide, autres valeurs si invalide</td>
<td>integer</td>
<td>1 1 1 1 1 1 1 1 1 1 …</td>
</tr>
<tr class="even">
<td>Latitude</td>
<td>Latitude du point de mesure</td>
<td>character</td>
<td>“49.119442” “49.107502” “49.358337” “49.394444” …</td>
</tr>
<tr class="odd">
<td>Longitude</td>
<td>Longitude du point de mesure</td>
<td>character</td>
<td>“6.180833” “6.122775” “6.156942” “6.201392” …</td>
</tr>
</tbody>
</table>

Pourplus d’informations sur les variables :
<https://www.geodair.fr/themes/custom/geodair_gp_theme/images/Geodair_Description_exports.pdf>

## Améliorations

Les données brutes ouvertes en csv2 présentent des défauts :

-   Mauvais typage des variables essentielles

-   Nom des colonnes améliorables

-   Colonnes inutiles (variable utilisée dans le processus de création
    de la base de données, non utile à l’exploitation du fichier
    d’export - d’après geodair)

Le script R suivant propose une amélioration du jeu de données initial,
il est normalement applicable quelque soit l’export sur le site GEODAIR
(c’est-à-dire quel que soit le type de polluant concerné et quelque soit
la période concernée).

    library(dplyr)
    library(janitor) #permet d'améliorer les noms de colonnes
    library(lubridate) #permet de gérer les dates/times

    df <- read.csv2("data.csv", encoding = "UTF-8") %>%
      clean_names() %>%
      select(-code_qualite, -procedure_de_mesure, -discriminant)
    df$latitude <- as.numeric(df$latitude)
    df$longitude <- as.numeric(df$longitude)
    df$valeur <- as.numeric(df$valeur)
    df$valeur_brute <- as.numeric(df$valeur_brute)
    df$validite <- as.numeric(df$validite)
    df$couverture_de_donnees <- as.numeric(df$couverture_de_donnees)
    df$taux_de_saisie <- as.numeric(df$taux_de_saisie)
    df$type_de_valeur <- as.factor(df$type_de_valeur)
    df$type_d_influence <- as.factor(df$type_d_influence)
    df$type_d_evaluation <- as.factor(df$type_d_evaluation)
    df$reglementaire <- as.factor(df$reglementaire)
    df$type_d_implantation <- as.factor(df$type_d_implantation)

Voici le nouveau typage des variable suite aux modifications :

<table>
<colgroup>
<col style="width: 11%" />
<col style="width: 5%" />
<col style="width: 82%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">variable</th>
<th style="text-align: left;">classe</th>
<th style="text-align: left;">first_values</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">x_u_feff_date_de_debut</td>
<td style="text-align: left;">character</td>
<td style="text-align: left;">2021/10/07 00:00:00, 2021/10/07 00:00:00, 2021/10/07 00:00:00, 2021/10/07 00:00:00, 2021/10/07 00:00:00, 2021/10/07 00:00:00</td>
</tr>
<tr class="even">
<td style="text-align: left;">date_de_fin</td>
<td style="text-align: left;">character</td>
<td style="text-align: left;">2021/10/07 12:00:00, 2021/10/07 12:00:00, 2021/10/07 12:00:00, 2021/10/07 12:00:00, 2021/10/07 12:00:00, 2021/10/07 12:00:00</td>
</tr>
<tr class="odd">
<td style="text-align: left;">organisme</td>
<td style="text-align: left;">character</td>
<td style="text-align: left;">ATMO GRAND EST, ATMO GRAND EST, ATMO GRAND EST, ATMO GRAND EST, ATMO SUD, ATMO SUD</td>
</tr>
<tr class="even">
<td style="text-align: left;">code_zas</td>
<td style="text-align: left;">character</td>
<td style="text-align: left;">FR44ZAG02, FR44ZAG02, FR44ZAG02, FR44ZAG02, FR93ZAG01, FR93ZAG01</td>
</tr>
<tr class="odd">
<td style="text-align: left;">zas</td>
<td style="text-align: left;">character</td>
<td style="text-align: left;">ZAG METZ, ZAG METZ, ZAG METZ, ZAG METZ, ZAG MARSEILLE-AIX, ZAG MARSEILLE-AIX</td>
</tr>
<tr class="even">
<td style="text-align: left;">code_site</td>
<td style="text-align: left;">character</td>
<td style="text-align: left;">FR01011, FR01018, FR01020, FR01021, FR02001, FR02004</td>
</tr>
<tr class="odd">
<td style="text-align: left;">nom_site</td>
<td style="text-align: left;">character</td>
<td style="text-align: left;">Metz-Centre, Scy-Chazelles, Thionville-Centre, Thionville-Garche, Berre l’Etang, Martigues P. Central</td>
</tr>
<tr class="even">
<td style="text-align: left;">type_d_implantation</td>
<td style="text-align: left;">integer</td>
<td style="text-align: left;">Urbaine, Périurbaine, Urbaine, Périurbaine, Périurbaine, Urbaine</td>
</tr>
<tr class="odd">
<td style="text-align: left;">polluant</td>
<td style="text-align: left;">character</td>
<td style="text-align: left;">O3, O3, O3, O3, O3, O3</td>
</tr>
<tr class="even">
<td style="text-align: left;">type_d_influence</td>
<td style="text-align: left;">integer</td>
<td style="text-align: left;">Fond, Fond, Fond, Fond, Fond, Fond</td>
</tr>
<tr class="odd">
<td style="text-align: left;">reglementaire</td>
<td style="text-align: left;">integer</td>
<td style="text-align: left;">Oui, Oui, Oui, Oui, Oui, Oui</td>
</tr>
<tr class="even">
<td style="text-align: left;">type_d_evaluation</td>
<td style="text-align: left;">integer</td>
<td style="text-align: left;">mesures fixes, mesures fixes, mesures fixes, mesures fixes, mesures fixes, mesures fixes</td>
</tr>
<tr class="odd">
<td style="text-align: left;">type_de_valeur</td>
<td style="text-align: left;">integer</td>
<td style="text-align: left;">Max. journalier moy. hor., Max. journalier moy. hor., Max. journalier moy. hor., Max. journalier moy. hor., Max. journalier moy. hor., Max. journalier moy. hor.</td>
</tr>
<tr class="even">
<td style="text-align: left;">valeur</td>
<td style="text-align: left;">double</td>
<td style="text-align: left;">46.1, 91.3, 60.4, 77.2, 69.4, 72.4</td>
</tr>
<tr class="odd">
<td style="text-align: left;">valeur_brute</td>
<td style="text-align: left;">double</td>
<td style="text-align: left;">46.1, 91.3, 60.4, 77.2, 69.375, 72.4</td>
</tr>
<tr class="even">
<td style="text-align: left;">unite_de_mesure</td>
<td style="text-align: left;">character</td>
<td style="text-align: left;">µg-m3, µg-m3, µg-m3, µg-m3, µg-m3, µg-m3</td>
</tr>
<tr class="odd">
<td style="text-align: left;">taux_de_saisie</td>
<td style="text-align: left;">double</td>
<td style="text-align: left;">92, 100, 92, 100, 100, 100</td>
</tr>
<tr class="even">
<td style="text-align: left;">couverture_temporelle</td>
<td style="text-align: left;">integer</td>
<td style="text-align: left;">100, 100, 100, 100, 100, 100</td>
</tr>
<tr class="odd">
<td style="text-align: left;">couverture_de_donnees</td>
<td style="text-align: left;">double</td>
<td style="text-align: left;">92, 100, 92, 100, 100, 100</td>
</tr>
<tr class="even">
<td style="text-align: left;">validite</td>
<td style="text-align: left;">double</td>
<td style="text-align: left;">1, 1, 1, 1, 1, 1</td>
</tr>
<tr class="odd">
<td style="text-align: left;">latitude</td>
<td style="text-align: left;">double</td>
<td style="text-align: left;">49.119442, 49.107502, 49.358337, 49.394444, 43.486234, 43.416661</td>
</tr>
<tr class="even">
<td style="text-align: left;">longitude</td>
<td style="text-align: left;">double</td>
<td style="text-align: left;">6.180833, 6.122775, 6.156942, 6.201392, 5.171939, 5.042731</td>
</tr>
</tbody>
</table>

### Dimensions / analyses descriptives

Ces informations concernent la base de de données ayant subi un
nettoyage à l’aide du script disponible dans la partie “Améliorations”.

La base de données comporte 301 lignes et 22 colonnes.

    ##  x_u_feff_date_de_debut date_de_fin         organisme        
    ##  Length:301             Length:301         Length:301        
    ##  Class :character       Class :character   Class :character  
    ##  Mode  :character       Mode  :character   Mode  :character  
    ##                                                              
    ##                                                              
    ##                                                              
    ##    code_zas             zas             code_site           nom_site        
    ##  Length:301         Length:301         Length:301         Length:301        
    ##  Class :character   Class :character   Class :character   Class :character  
    ##  Mode  :character   Mode  :character   Mode  :character   Mode  :character  
    ##                                                                             
    ##                                                                             
    ##                                                                             
    ##              type_d_implantation   polluant             type_d_influence
    ##  Périurbaine           : 95      Length:301         Fond        :299    
    ##  Rurale nationale      : 12      Class :character   Industrielle:  2    
    ##  Rurale près des villes: 11      Mode  :character                       
    ##  Rurale régionale      : 30                                             
    ##  Urbaine               :153                                             
    ##                                                                         
    ##  reglementaire            type_d_evaluation                   type_de_valeur
    ##  Oui:301       estimation objective:  4     Max. journalier moy. hor.:301   
    ##                mesures fixes       :264                                     
    ##                mesures indicatives : 33                                     
    ##                                                                             
    ##                                                                             
    ##                                                                             
    ##      valeur        valeur_brute     unite_de_mesure    taux_de_saisie  
    ##  Min.   :  5.20   Min.   :  5.175   Length:301         Min.   : 33.00  
    ##  1st Qu.: 52.30   1st Qu.: 52.325   Class :character   1st Qu.:100.00  
    ##  Median : 64.10   Median : 64.075   Mode  :character   Median :100.00  
    ##  Mean   : 61.39   Mean   : 61.371                      Mean   : 97.47  
    ##  3rd Qu.: 71.60   3rd Qu.: 71.575                      3rd Qu.:100.00  
    ##  Max.   :111.50   Max.   :111.500                      Max.   :100.00  
    ##  couverture_temporelle couverture_de_donnees    validite          latitude     
    ##  Min.   :100           Min.   : 33.00        Min.   :-1.0000   Min.   :-21.33  
    ##  1st Qu.:100           1st Qu.:100.00        1st Qu.: 1.0000   1st Qu.: 44.80  
    ##  Median :100           Median :100.00        Median : 1.0000   Median : 46.80  
    ##  Mean   :100           Mean   : 97.47        Mean   : 0.9668   Mean   : 44.52  
    ##  3rd Qu.:100           3rd Qu.:100.00        3rd Qu.: 1.0000   3rd Qu.: 48.78  
    ##  Max.   :100           Max.   :100.00        Max.   : 1.0000   Max.   : 51.03  
    ##    longitude      
    ##  Min.   :-61.727  
    ##  1st Qu.:  1.148  
    ##  Median :  3.066  
    ##  Mean   :  2.557  
    ##  3rd Qu.:  5.395  
    ##  Max.   : 55.583

## Extrait de la base de données brute

<table>
<colgroup>
<col style="width: 6%" />
<col style="width: 5%" />
<col style="width: 4%" />
<col style="width: 2%" />
<col style="width: 5%" />
<col style="width: 2%" />
<col style="width: 6%" />
<col style="width: 5%" />
<col style="width: 2%" />
<col style="width: 4%" />
<col style="width: 4%" />
<col style="width: 5%" />
<col style="width: 7%" />
<col style="width: 2%" />
<col style="width: 3%" />
<col style="width: 4%" />
<col style="width: 4%" />
<col style="width: 6%" />
<col style="width: 6%" />
<col style="width: 2%" />
<col style="width: 2%" />
<col style="width: 2%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">x_u_feff_date_de_debut</th>
<th style="text-align: left;">date_de_fin</th>
<th style="text-align: left;">organisme</th>
<th style="text-align: left;">code_zas</th>
<th style="text-align: left;">zas</th>
<th style="text-align: left;">code_site</th>
<th style="text-align: left;">nom_site</th>
<th style="text-align: left;">type_d_implantation</th>
<th style="text-align: left;">polluant</th>
<th style="text-align: left;">type_d_influence</th>
<th style="text-align: left;">reglementaire</th>
<th style="text-align: left;">type_d_evaluation</th>
<th style="text-align: left;">type_de_valeur</th>
<th style="text-align: right;">valeur</th>
<th style="text-align: right;">valeur_brute</th>
<th style="text-align: left;">unite_de_mesure</th>
<th style="text-align: right;">taux_de_saisie</th>
<th style="text-align: right;">couverture_temporelle</th>
<th style="text-align: right;">couverture_de_donnees</th>
<th style="text-align: right;">validite</th>
<th style="text-align: right;">latitude</th>
<th style="text-align: right;">longitude</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">2021/10/07 00:00:00</td>
<td style="text-align: left;">2021/10/07 12:00:00</td>
<td style="text-align: left;">ATMO GRAND EST</td>
<td style="text-align: left;">FR44ZAG02</td>
<td style="text-align: left;">ZAG METZ</td>
<td style="text-align: left;">FR01011</td>
<td style="text-align: left;">Metz-Centre</td>
<td style="text-align: left;">Urbaine</td>
<td style="text-align: left;">O3</td>
<td style="text-align: left;">Fond</td>
<td style="text-align: left;">Oui</td>
<td style="text-align: left;">mesures fixes</td>
<td style="text-align: left;">Max. journalier moy. hor.</td>
<td style="text-align: right;">46.1</td>
<td style="text-align: right;">46.100</td>
<td style="text-align: left;">µg-m3</td>
<td style="text-align: right;">92</td>
<td style="text-align: right;">100</td>
<td style="text-align: right;">92</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">49.11944</td>
<td style="text-align: right;">6.180833</td>
</tr>
<tr class="even">
<td style="text-align: left;">2021/10/07 00:00:00</td>
<td style="text-align: left;">2021/10/07 12:00:00</td>
<td style="text-align: left;">ATMO GRAND EST</td>
<td style="text-align: left;">FR44ZAG02</td>
<td style="text-align: left;">ZAG METZ</td>
<td style="text-align: left;">FR01018</td>
<td style="text-align: left;">Scy-Chazelles</td>
<td style="text-align: left;">Périurbaine</td>
<td style="text-align: left;">O3</td>
<td style="text-align: left;">Fond</td>
<td style="text-align: left;">Oui</td>
<td style="text-align: left;">mesures fixes</td>
<td style="text-align: left;">Max. journalier moy. hor.</td>
<td style="text-align: right;">91.3</td>
<td style="text-align: right;">91.300</td>
<td style="text-align: left;">µg-m3</td>
<td style="text-align: right;">100</td>
<td style="text-align: right;">100</td>
<td style="text-align: right;">100</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">49.10750</td>
<td style="text-align: right;">6.122775</td>
</tr>
<tr class="odd">
<td style="text-align: left;">2021/10/07 00:00:00</td>
<td style="text-align: left;">2021/10/07 12:00:00</td>
<td style="text-align: left;">ATMO GRAND EST</td>
<td style="text-align: left;">FR44ZAG02</td>
<td style="text-align: left;">ZAG METZ</td>
<td style="text-align: left;">FR01020</td>
<td style="text-align: left;">Thionville-Centre</td>
<td style="text-align: left;">Urbaine</td>
<td style="text-align: left;">O3</td>
<td style="text-align: left;">Fond</td>
<td style="text-align: left;">Oui</td>
<td style="text-align: left;">mesures fixes</td>
<td style="text-align: left;">Max. journalier moy. hor.</td>
<td style="text-align: right;">60.4</td>
<td style="text-align: right;">60.400</td>
<td style="text-align: left;">µg-m3</td>
<td style="text-align: right;">92</td>
<td style="text-align: right;">100</td>
<td style="text-align: right;">92</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">49.35834</td>
<td style="text-align: right;">6.156942</td>
</tr>
<tr class="even">
<td style="text-align: left;">2021/10/07 00:00:00</td>
<td style="text-align: left;">2021/10/07 12:00:00</td>
<td style="text-align: left;">ATMO GRAND EST</td>
<td style="text-align: left;">FR44ZAG02</td>
<td style="text-align: left;">ZAG METZ</td>
<td style="text-align: left;">FR01021</td>
<td style="text-align: left;">Thionville-Garche</td>
<td style="text-align: left;">Périurbaine</td>
<td style="text-align: left;">O3</td>
<td style="text-align: left;">Fond</td>
<td style="text-align: left;">Oui</td>
<td style="text-align: left;">mesures fixes</td>
<td style="text-align: left;">Max. journalier moy. hor.</td>
<td style="text-align: right;">77.2</td>
<td style="text-align: right;">77.200</td>
<td style="text-align: left;">µg-m3</td>
<td style="text-align: right;">100</td>
<td style="text-align: right;">100</td>
<td style="text-align: right;">100</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">49.39444</td>
<td style="text-align: right;">6.201392</td>
</tr>
<tr class="odd">
<td style="text-align: left;">2021/10/07 00:00:00</td>
<td style="text-align: left;">2021/10/07 12:00:00</td>
<td style="text-align: left;">ATMO SUD</td>
<td style="text-align: left;">FR93ZAG01</td>
<td style="text-align: left;">ZAG MARSEILLE-AIX</td>
<td style="text-align: left;">FR02001</td>
<td style="text-align: left;">Berre l’Etang</td>
<td style="text-align: left;">Périurbaine</td>
<td style="text-align: left;">O3</td>
<td style="text-align: left;">Fond</td>
<td style="text-align: left;">Oui</td>
<td style="text-align: left;">mesures fixes</td>
<td style="text-align: left;">Max. journalier moy. hor.</td>
<td style="text-align: right;">69.4</td>
<td style="text-align: right;">69.375</td>
<td style="text-align: left;">µg-m3</td>
<td style="text-align: right;">100</td>
<td style="text-align: right;">100</td>
<td style="text-align: right;">100</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">43.48623</td>
<td style="text-align: right;">5.171939</td>
</tr>
<tr class="even">
<td style="text-align: left;">2021/10/07 00:00:00</td>
<td style="text-align: left;">2021/10/07 12:00:00</td>
<td style="text-align: left;">ATMO SUD</td>
<td style="text-align: left;">FR93ZAG01</td>
<td style="text-align: left;">ZAG MARSEILLE-AIX</td>
<td style="text-align: left;">FR02004</td>
<td style="text-align: left;">Martigues P. Central</td>
<td style="text-align: left;">Urbaine</td>
<td style="text-align: left;">O3</td>
<td style="text-align: left;">Fond</td>
<td style="text-align: left;">Oui</td>
<td style="text-align: left;">mesures fixes</td>
<td style="text-align: left;">Max. journalier moy. hor.</td>
<td style="text-align: right;">72.4</td>
<td style="text-align: right;">72.400</td>
<td style="text-align: left;">µg-m3</td>
<td style="text-align: right;">100</td>
<td style="text-align: right;">100</td>
<td style="text-align: right;">100</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">43.41666</td>
<td style="text-align: right;">5.042731</td>
</tr>
</tbody>
</table>
