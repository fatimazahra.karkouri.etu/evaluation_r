<table>
<thead>
<tr class="header">
<th style="text-align: left;">nom_long</th>
<th style="text-align: left;">nom_court</th>
<th style="text-align: left;">type</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">Services d’urgences</td>
<td style="text-align: left;">SU</td>
<td style="text-align: left;">qualitative</td>
</tr>
<tr class="even">
<td style="text-align: left;">Services mobiles d’urgences et de réanimation</td>
<td style="text-align: left;">SMUR</td>
<td style="text-align: left;">qualitative</td>
</tr>
<tr class="odd">
<td style="text-align: left;">Médecins correspondant du SAMU</td>
<td style="text-align: left;">MCS</td>
<td style="text-align: left;">qualitative</td>
</tr>
<tr class="even">
<td style="text-align: left;">Hélicoptères du SMUR</td>
<td style="text-align: left;">HéliSMUR</td>
<td style="text-align: left;">qualitative</td>
</tr>
<tr class="odd">
<td style="text-align: left;">Hélicoptères d’État</td>
<td style="text-align: left;">HéliETAT</td>
<td style="text-align: left;">qualitative</td>
</tr>
<tr class="even">
<td style="text-align: left;">Temps d’accès à un SU ou un SMUR</td>
<td style="text-align: left;">tps_SU_SMUR</td>
<td style="text-align: left;">quantitative</td>
</tr>
<tr class="odd">
<td style="text-align: left;">Temps d’accès à un SU, un SMUR ou un MCS</td>
<td style="text-align: left;">tps_SU_SMUR_MCS</td>
<td style="text-align: left;">quantitative</td>
</tr>
<tr class="even">
<td style="text-align: left;">Temps d’accès élargi aux HéliSMUR</td>
<td style="text-align: left;">tps_ychelismur</td>
<td style="text-align: left;">quantitative</td>
</tr>
<tr class="odd">
<td style="text-align: left;">Temps d’accès élargi aux HéliSMUR et HéliETAT</td>
<td style="text-align: left;">tps_ycheli</td>
<td style="text-align: left;">quantitative</td>
</tr>
<tr class="even">
<td style="text-align: left;">Code la commune équipée la plus proche</td>
<td style="text-align: left;">com_equipee</td>
<td style="text-align: left;">qualitative</td>
</tr>
<tr class="odd">
<td style="text-align: left;">Type d’équipement le plus proche</td>
<td style="text-align: left;">typ_equip</td>
<td style="text-align: left;">qualitative</td>
</tr>
<tr class="even">
<td style="text-align: left;">Densité de population de la commune</td>
<td style="text-align: left;">densite</td>
<td style="text-align: left;">quantitative</td>
</tr>
</tbody>
</table>
