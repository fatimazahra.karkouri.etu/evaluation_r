# Présentation

La diagnostic d’accès aux soins urgents a été réalisé conjointement par
la DREES et la DGOS pour décrire, commune par commune, l’équipement en
services de soins urgents au 31 décembre d’une année donnée (service
d’urgences, SMUR ou antenne de SMUR, médecin correspondant du Samu,
HéliSMUR et hélicoptères d’Etat) et les distances d’accès des habitants
de la commune à ces équipements. Ce dataframe est fourni par la DRESS.
Les temps d’accès sont calculés avec le distancier METRIC.

[source](https://data.drees.solidarites-sante.gouv.fr/explore/dataset/2943_diagnostic-d-acces-aux-soins-urgents/information/):
<https://data.drees.solidarites-sante.gouv.fr/explore/dataset/2943_diagnostic-d-acces-aux-soins-urgents/information/>

# Intérêt

Ces données peuvent être utilisées dans des études écologiques prenant
en compte le délai entre une commune et l’accès à des soins d’urgence.


# Variables

<table>
<colgroup>
<col style="width: 8%" />
<col style="width: 19%" />
<col style="width: 71%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">Code de la variable</th>
<th style="text-align: left;">Libellé détaillé</th>
<th style="text-align: left;">Commentaires</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">SU</td>
<td style="text-align: left;">Services d’urgences</td>
<td style="text-align: left;">Indicatrices 0/1 de la présence d’un SU dans chaque commune</td>
</tr>
<tr class="even">
<td style="text-align: left;">SMUR</td>
<td style="text-align: left;">Services mobiles d’urgences et de réanimation</td>
<td style="text-align: left;">Indicatrices 0/1 de la présence d’un SMUR dans chaque commune</td>
</tr>
<tr class="odd">
<td style="text-align: left;">MCS</td>
<td style="text-align: left;">Médecins correspondant du SAMU</td>
<td style="text-align: left;">Indicatrices 0/1 de la présence d’un MCS dans chaque commune</td>
</tr>
<tr class="even">
<td style="text-align: left;">HéliSMUR</td>
<td style="text-align: left;">Hélicoptères du SMUR</td>
<td style="text-align: left;">Indicatrices 0/1 de la présence d’un HéliSMUR dans chaque commune</td>
</tr>
<tr class="odd">
<td style="text-align: left;">HéliETAT</td>
<td style="text-align: left;">Hélicoptères d’État</td>
<td style="text-align: left;">Indicatrices 0/1 de la présence d’un Hélico d’État dans chaque commune</td>
</tr>
<tr class="even">
<td style="text-align: left;">tps_SU_SMUR</td>
<td style="text-align: left;">Temps d’accès à un SU ou un SMUR</td>
<td style="text-align: left;">min (tps d’accès moyen à SU, tps d’accès HC à SMUR)</td>
</tr>
<tr class="odd">
<td style="text-align: left;">tps_SU_SMUR_MCS</td>
<td style="text-align: left;">Temps d’accès à un SU, un SMUR ou un MCS</td>
<td style="text-align: left;">min (tps d’accès moyen à SU, tps d’accès HC à SMUR, tps d’accès moyen à MCS)</td>
</tr>
<tr class="even">
<td style="text-align: left;">tps_ychelismur</td>
<td style="text-align: left;">Temps d’accès élargi aux HéliSMUR</td>
<td style="text-align: left;">si tps_SU_SMUR_MCS &gt;30min, min(tps_SU_SMUR_MCS, tps Hélismur) - sinon, tps_SU_SMUR_MCS</td>
</tr>
<tr class="odd">
<td style="text-align: left;">tps_ycheli</td>
<td style="text-align: left;">Temps d’accès élargi aux HéliSMUR et HéliETAT</td>
<td style="text-align: left;">si tps_SU_SMUR_MCS &gt;30min, min(tps Hélismur, tps HéliETAT ) - sinon, tps_SU_SMUR_MCS</td>
</tr>
<tr class="even">
<td style="text-align: left;">com_equipee</td>
<td style="text-align: left;">Code la commune équipée la plus proche</td>
<td style="text-align: left;">Pour chaque commune de résidence, code de la commune équipée qui minimise le temps d’accès à un soin de médecine d’urgence</td>
</tr>
<tr class="odd">
<td style="text-align: left;">typ_equip</td>
<td style="text-align: left;">Type d’équipement le plus proche</td>
<td style="text-align: left;">Pour chaque commune de résidence, type d’équipement permettant de minimiser le temps d’accès à un soin de médecine d’urgence (SU_SMUR, MCS, HéliSMUR ou Hélico d’Etat)</td>
</tr>
<tr class="even">
<td style="text-align: left;">densite</td>
<td style="text-align: left;">Densité de population de la commune</td>
<td style="text-align: left;">Densité de population municipale 2017 (en habitants par km2)</td>
</tr>
</tbody>
</table>

# Extrait des données

<table style="width:100%;">
<colgroup>
<col style="width: 9%" />
<col style="width: 12%" />
<col style="width: 6%" />
<col style="width: 3%" />
<col style="width: 1%" />
<col style="width: 2%" />
<col style="width: 2%" />
<col style="width: 4%" />
<col style="width: 4%" />
<col style="width: 8%" />
<col style="width: 5%" />
<col style="width: 6%" />
<col style="width: 8%" />
<col style="width: 7%" />
<col style="width: 5%" />
<col style="width: 6%" />
<col style="width: 5%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">Code commune Insee</th>
<th style="text-align: left;">Libellé commune</th>
<th style="text-align: left;">Département</th>
<th style="text-align: left;">Région</th>
<th style="text-align: right;">SU</th>
<th style="text-align: right;">SMUR</th>
<th style="text-align: right;">MCS</th>
<th style="text-align: right;">HéliSMUR</th>
<th style="text-align: right;">HéliETAT</th>
<th style="text-align: right;">Population 2017</th>
<th style="text-align: left;">typ_equip</th>
<th style="text-align: right;">tps_SU_SMUR</th>
<th style="text-align: right;">tps_SU_SMUR_MCS</th>
<th style="text-align: right;">tps_ychelismur</th>
<th style="text-align: right;">tps_ycheli</th>
<th style="text-align: left;">com_equipee</th>
<th style="text-align: right;">densite</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">01001</td>
<td style="text-align: left;">L’Abergement-Clémenciat</td>
<td style="text-align: left;">01</td>
<td style="text-align: left;">84</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">776</td>
<td style="text-align: left;">HeliSMUR</td>
<td style="text-align: right;">31.0</td>
<td style="text-align: right;">31.0</td>
<td style="text-align: right;">30.18518</td>
<td style="text-align: right;">30.18518</td>
<td style="text-align: left;">69029</td>
<td style="text-align: right;">48.65204</td>
</tr>
<tr class="even">
<td style="text-align: left;">01002</td>
<td style="text-align: left;">L’Abergement-de-Varey</td>
<td style="text-align: left;">01</td>
<td style="text-align: left;">84</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">248</td>
<td style="text-align: left;">SU_SMUR</td>
<td style="text-align: right;">18.0</td>
<td style="text-align: right;">18.0</td>
<td style="text-align: right;">18.00000</td>
<td style="text-align: right;">18.00000</td>
<td style="text-align: left;">01004</td>
<td style="text-align: right;">27.10383</td>
</tr>
<tr class="odd">
<td style="text-align: left;">01004</td>
<td style="text-align: left;">Ambérieu-en-Bugey</td>
<td style="text-align: left;">01</td>
<td style="text-align: left;">84</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">14035</td>
<td style="text-align: left;">SU_SMUR</td>
<td style="text-align: right;">0.0</td>
<td style="text-align: right;">0.0</td>
<td style="text-align: right;">0.00000</td>
<td style="text-align: right;">0.00000</td>
<td style="text-align: left;">01004</td>
<td style="text-align: right;">570.52846</td>
</tr>
<tr class="even">
<td style="text-align: left;">01005</td>
<td style="text-align: left;">Ambérieux-en-Dombes</td>
<td style="text-align: left;">01</td>
<td style="text-align: left;">84</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">1689</td>
<td style="text-align: left;">SU_SMUR</td>
<td style="text-align: right;">23.5</td>
<td style="text-align: right;">23.5</td>
<td style="text-align: right;">23.50000</td>
<td style="text-align: right;">23.50000</td>
<td style="text-align: left;">69013</td>
<td style="text-align: right;">106.09296</td>
</tr>
<tr class="odd">
<td style="text-align: left;">01006</td>
<td style="text-align: left;">Ambléon</td>
<td style="text-align: left;">01</td>
<td style="text-align: left;">84</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">111</td>
<td style="text-align: left;">SU_SMUR</td>
<td style="text-align: right;">15.0</td>
<td style="text-align: right;">15.0</td>
<td style="text-align: right;">15.00000</td>
<td style="text-align: right;">15.00000</td>
<td style="text-align: left;">01034</td>
<td style="text-align: right;">18.87755</td>
</tr>
<tr class="even">
<td style="text-align: left;">01007</td>
<td style="text-align: left;">Ambronay</td>
<td style="text-align: left;">01</td>
<td style="text-align: left;">84</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">2726</td>
<td style="text-align: left;">SU_SMUR</td>
<td style="text-align: right;">9.0</td>
<td style="text-align: right;">9.0</td>
<td style="text-align: right;">9.00000</td>
<td style="text-align: right;">9.00000</td>
<td style="text-align: left;">01004</td>
<td style="text-align: right;">81.25186</td>
</tr>
</tbody>
</table>

# Analyses descriptives

**Répartition des variables catégorielles**

![](acces_soins_urgents_files/figure-markdown_strict/unnamed-chunk-4-1.png)


**Distribution des variables quantitatives**
![](acces_soins_urgents_files/figure-markdown_strict/unnamed-chunk-6-1.png)

<table>
<colgroup>
<col style="width: 3%" />
<col style="width: 16%" />
<col style="width: 14%" />
<col style="width: 16%" />
<col style="width: 15%" />
<col style="width: 15%" />
<col style="width: 17%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;"></th>
<th style="text-align: left;">Population 2017</th>
<th style="text-align: left;">tps_SU_SMUR</th>
<th style="text-align: left;">tps_SU_SMUR_MCS</th>
<th style="text-align: left;">tps_ychelismur</th>
<th style="text-align: left;">tps_ycheli</th>
<th style="text-align: left;">densite</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;">Min. : 0</td>
<td style="text-align: left;">Min. : 0.0</td>
<td style="text-align: left;">Min. : 0.00</td>
<td style="text-align: left;">Min. : 0.00</td>
<td style="text-align: left;">Min. : 0.00</td>
<td style="text-align: left;">Min. : 0.00</td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;">1st Qu.: 199</td>
<td style="text-align: left;">1st Qu.: 15.0</td>
<td style="text-align: left;">1st Qu.: 14.00</td>
<td style="text-align: left;">1st Qu.: 14.00</td>
<td style="text-align: left;">1st Qu.: 14.00</td>
<td style="text-align: left;">1st Qu.: 22.51</td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;">Median : 457</td>
<td style="text-align: left;">Median : 22.0</td>
<td style="text-align: left;">Median : 20.00</td>
<td style="text-align: left;">Median : 19.30</td>
<td style="text-align: left;">Median : 19.00</td>
<td style="text-align: left;">Median : 66.79</td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;">Mean : 1908</td>
<td style="text-align: left;">Mean : 23.3</td>
<td style="text-align: left;">Mean : 20.33</td>
<td style="text-align: left;">Mean : 19.32</td>
<td style="text-align: left;">Mean : 19.06</td>
<td style="text-align: left;">Mean : 129.88</td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;">3rd Qu.: 1161</td>
<td style="text-align: left;">3rd Qu.: 29.5</td>
<td style="text-align: left;">3rd Qu.: 26.00</td>
<td style="text-align: left;">3rd Qu.: 25.00</td>
<td style="text-align: left;">3rd Qu.: 25.00</td>
<td style="text-align: left;">3rd Qu.: 162.46</td>
</tr>
<tr class="even">
<td style="text-align: left;"></td>
<td style="text-align: left;">Max. :479553</td>
<td style="text-align: left;">Max. :163.0</td>
<td style="text-align: left;">Max. :104.00</td>
<td style="text-align: left;">Max. :100.25</td>
<td style="text-align: left;">Max. :100.25</td>
<td style="text-align: left;">Max. :22103.33</td>
</tr>
<tr class="odd">
<td style="text-align: left;"></td>
<td style="text-align: left;">NA</td>
<td style="text-align: left;">NA’s :23</td>
<td style="text-align: left;">NA’s :11</td>
<td style="text-align: left;">NA’s :3</td>
<td style="text-align: left;">NA’s :2</td>
<td style="text-align: left;">NA’s :17</td>
</tr>
</tbody>
</table>

# Datamanagement

    acces_su_19$SU <- as.factor(acces_su_19$SU)
    acces_su_19$SMUR <- as.factor(acces_su_19$SMUR)
    acces_su_19$MCS <- as.factor(acces_su_19$MCS)
    acces_su_19$HéliSMUR <- as.factor(acces_su_19$HéliSMUR)
    acces_su_19$HéliETAT <- as.factor(acces_su_19$HéliETAT)
    acces_su_19$typ_equip <- as.factor(acces_su_19$typ_equip)

# Licence

Licence Etalab 2.0
