<table>
<thead>
<tr class="header">
<th style="text-align: left;">variable</th>
<th style="text-align: left;">type</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">id</td>
<td style="text-align: left;">integer</td>
</tr>
<tr class="even">
<td style="text-align: left;">gender</td>
<td style="text-align: left;">character</td>
</tr>
<tr class="odd">
<td style="text-align: left;">age</td>
<td style="text-align: left;">numeric</td>
</tr>
<tr class="even">
<td style="text-align: left;">hypertension</td>
<td style="text-align: left;">integer</td>
</tr>
<tr class="odd">
<td style="text-align: left;">heart_disease</td>
<td style="text-align: left;">integer</td>
</tr>
<tr class="even">
<td style="text-align: left;">ever_married</td>
<td style="text-align: left;">character</td>
</tr>
<tr class="odd">
<td style="text-align: left;">work_type</td>
<td style="text-align: left;">character</td>
</tr>
<tr class="even">
<td style="text-align: left;">residence_type</td>
<td style="text-align: left;">character</td>
</tr>
<tr class="odd">
<td style="text-align: left;">avg_glucose_level</td>
<td style="text-align: left;">numeric</td>
</tr>
<tr class="even">
<td style="text-align: left;">bmi</td>
<td style="text-align: left;">character</td>
</tr>
<tr class="odd">
<td style="text-align: left;">smoking_status</td>
<td style="text-align: left;">character</td>
</tr>
<tr class="even">
<td style="text-align: left;">stroke</td>
<td style="text-align: left;">integer</td>
</tr>
</tbody>
</table>
