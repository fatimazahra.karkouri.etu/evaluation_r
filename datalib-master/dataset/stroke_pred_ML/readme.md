### Données pour la prédiction d'Accident Vasculaire Cérébral.

Description
-----------

Ce jeu de données a pour objectif de prédire si un patient est
susceptible de subir un accident vasculaire cérébral (AVC) en fonction
de paramètres d’entrée divers (voir section variables). Des données pour
5110 personnes sont disponibles.

| Fichier | Description | 
| -------- | -------- | 
| stroke_data.csv | Jeu de données | 
| stroke_data.csv.Rmd |  Markdown pour l'aperçu du dataset + .md |

Preview
-------

<table style="width:100%;">
<colgroup>
<col style="width: 4%" />
<col style="width: 5%" />
<col style="width: 3%" />
<col style="width: 9%" />
<col style="width: 10%" />
<col style="width: 9%" />
<col style="width: 10%" />
<col style="width: 11%" />
<col style="width: 13%" />
<col style="width: 3%" />
<col style="width: 12%" />
<col style="width: 5%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: right;">id</th>
<th style="text-align: left;">gender</th>
<th style="text-align: right;">age</th>
<th style="text-align: right;">hypertension</th>
<th style="text-align: right;">heart_disease</th>
<th style="text-align: left;">ever_married</th>
<th style="text-align: left;">work_type</th>
<th style="text-align: left;">residence_type</th>
<th style="text-align: right;">avg_glucose_level</th>
<th style="text-align: left;">bmi</th>
<th style="text-align: left;">smoking_status</th>
<th style="text-align: right;">stroke</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: right;">9046</td>
<td style="text-align: left;">Male</td>
<td style="text-align: right;">67</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">1</td>
<td style="text-align: left;">Yes</td>
<td style="text-align: left;">Private</td>
<td style="text-align: left;">Urban</td>
<td style="text-align: right;">228.69</td>
<td style="text-align: left;">36.6</td>
<td style="text-align: left;">formerly smoked</td>
<td style="text-align: right;">1</td>
</tr>
<tr class="even">
<td style="text-align: right;">51676</td>
<td style="text-align: left;">Female</td>
<td style="text-align: right;">61</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: left;">Yes</td>
<td style="text-align: left;">Self-employed</td>
<td style="text-align: left;">Rural</td>
<td style="text-align: right;">202.21</td>
<td style="text-align: left;">N/A</td>
<td style="text-align: left;">never smoked</td>
<td style="text-align: right;">1</td>
</tr>
<tr class="odd">
<td style="text-align: right;">31112</td>
<td style="text-align: left;">Male</td>
<td style="text-align: right;">80</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">1</td>
<td style="text-align: left;">Yes</td>
<td style="text-align: left;">Private</td>
<td style="text-align: left;">Rural</td>
<td style="text-align: right;">105.92</td>
<td style="text-align: left;">32.5</td>
<td style="text-align: left;">never smoked</td>
<td style="text-align: right;">1</td>
</tr>
<tr class="even">
<td style="text-align: right;">60182</td>
<td style="text-align: left;">Female</td>
<td style="text-align: right;">49</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: left;">Yes</td>
<td style="text-align: left;">Private</td>
<td style="text-align: left;">Urban</td>
<td style="text-align: right;">171.23</td>
<td style="text-align: left;">34.4</td>
<td style="text-align: left;">smokes</td>
<td style="text-align: right;">1</td>
</tr>
<tr class="odd">
<td style="text-align: right;">1665</td>
<td style="text-align: left;">Female</td>
<td style="text-align: right;">79</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0</td>
<td style="text-align: left;">Yes</td>
<td style="text-align: left;">Self-employed</td>
<td style="text-align: left;">Rural</td>
<td style="text-align: right;">174.12</td>
<td style="text-align: left;">24</td>
<td style="text-align: left;">never smoked</td>
<td style="text-align: right;">1</td>
</tr>
<tr class="even">
<td style="text-align: right;">56669</td>
<td style="text-align: left;">Male</td>
<td style="text-align: right;">81</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: left;">Yes</td>
<td style="text-align: left;">Private</td>
<td style="text-align: left;">Urban</td>
<td style="text-align: right;">186.21</td>
<td style="text-align: left;">29</td>
<td style="text-align: left;">formerly smoked</td>
<td style="text-align: right;">1</td>
</tr>
</tbody>
</table>

Dataset Characteristics
-----------------------

-   Rows : 5 110
-   Columns : 12
-   Size : 310kB

Variables
---------

    ##  [1] "id"                "gender"            "age"              
    ##  [4] "hypertension"      "heart_disease"     "ever_married"     
    ##  [7] "work_type"         "residence_type"    "avg_glucose_level"
    ## [10] "bmi"               "smoking_status"    "stroke"


| Variable | Description | 
| -------- | -------- | 
| id | Unique Identifier | 
| gender |  Gender : "Male", "Female" or "Other" |
| age |  Age |
| hypertension |  0 : the patient doesn't have hypertension ; 1 : the patient has hypertension |
| heart_disease |  0 : the patient doesn't have any heart diseases ;  1 : the patient has a heart disease |
| ever_married |  If the patient has ever been married : "No" or "Yes" |
| work_type |  Type of work :  "children", "Govt_jov", "Never_worked", "Private" or "Self-employed" |
| residence_type | Type of residency : "Rural" or "Urban"|
| avg_glucose_level | Average blood glucose level |
| bmi |  Body Mass Index |
| smoking_status | Smoking status : "formerly smoked", "never smoked", "smokes" or "Unknown" |
| stroke | 0 : the patient never had a stroke 1 : the patient had a stroke |


Licence
-------

**Only for educational purpose**

Source
------

Source :
<a href="https://www.kaggle.com/fedesoriano/stroke-prediction-dataset" class="uri">https://www.kaggle.com/fedesoriano/stroke-prediction-dataset</a>
