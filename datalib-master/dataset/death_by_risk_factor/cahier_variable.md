<table>
<thead>
<tr class="header">
<th style="text-align: left;">variable</th>
<th style="text-align: left;">type</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">entity</td>
<td style="text-align: left;">character</td>
</tr>
<tr class="even">
<td style="text-align: left;">code</td>
<td style="text-align: left;">character</td>
</tr>
<tr class="odd">
<td style="text-align: left;">year</td>
<td style="text-align: left;">integer</td>
</tr>
<tr class="even">
<td style="text-align: left;">unsafe_water_source</td>
<td style="text-align: left;">numeric</td>
</tr>
<tr class="odd">
<td style="text-align: left;">unsafe_sanitation</td>
<td style="text-align: left;">numeric</td>
</tr>
<tr class="even">
<td style="text-align: left;">no_access_to_handwashing_facility</td>
<td style="text-align: left;">numeric</td>
</tr>
<tr class="odd">
<td style="text-align: left;">household_air_pollution_from_solid_fuels</td>
<td style="text-align: left;">numeric</td>
</tr>
<tr class="even">
<td style="text-align: left;">non_exclusive_breastfeeding</td>
<td style="text-align: left;">numeric</td>
</tr>
<tr class="odd">
<td style="text-align: left;">discontinued_breastfeeding</td>
<td style="text-align: left;">numeric</td>
</tr>
<tr class="even">
<td style="text-align: left;">child_wasting</td>
<td style="text-align: left;">numeric</td>
</tr>
<tr class="odd">
<td style="text-align: left;">child_stuning</td>
<td style="text-align: left;">numeric</td>
</tr>
<tr class="even">
<td style="text-align: left;">low_birth_weight_for_gestation</td>
<td style="text-align: left;">numeric</td>
</tr>
<tr class="odd">
<td style="text-align: left;">secondhand_smoke</td>
<td style="text-align: left;">numeric</td>
</tr>
<tr class="even">
<td style="text-align: left;">alcohol_use</td>
<td style="text-align: left;">numeric</td>
</tr>
<tr class="odd">
<td style="text-align: left;">drug_use</td>
<td style="text-align: left;">numeric</td>
</tr>
<tr class="even">
<td style="text-align: left;">diet_low_in_fruits</td>
<td style="text-align: left;">numeric</td>
</tr>
<tr class="odd">
<td style="text-align: left;">diet_low_in_vegetables</td>
<td style="text-align: left;">numeric</td>
</tr>
<tr class="even">
<td style="text-align: left;">unsafe_sex</td>
<td style="text-align: left;">numeric</td>
</tr>
<tr class="odd">
<td style="text-align: left;">low_physical_activity</td>
<td style="text-align: left;">numeric</td>
</tr>
<tr class="even">
<td style="text-align: left;">high_fasting_plasma_glucose</td>
<td style="text-align: left;">numeric</td>
</tr>
<tr class="odd">
<td style="text-align: left;">high_total_cholesterol</td>
<td style="text-align: left;">numeric</td>
</tr>
<tr class="even">
<td style="text-align: left;">high_body_mass_index</td>
<td style="text-align: left;">numeric</td>
</tr>
<tr class="odd">
<td style="text-align: left;">high_systolic_blood_pressure</td>
<td style="text-align: left;">numeric</td>
</tr>
<tr class="even">
<td style="text-align: left;">smoking</td>
<td style="text-align: left;">numeric</td>
</tr>
<tr class="odd">
<td style="text-align: left;">iro._deficiency</td>
<td style="text-align: left;">numeric</td>
</tr>
<tr class="even">
<td style="text-align: left;">vitamin_a_deficiency</td>
<td style="text-align: left;">numeric</td>
</tr>
<tr class="odd">
<td style="text-align: left;">low_bone_mineral_density</td>
<td style="text-align: left;">numeric</td>
</tr>
<tr class="even">
<td style="text-align: left;">air_pollution</td>
<td style="text-align: left;">numeric</td>
</tr>
<tr class="odd">
<td style="text-align: left;">outdoor_air_pollution</td>
<td style="text-align: left;">numeric</td>
</tr>
<tr class="even">
<td style="text-align: left;">diet_high_in_sodium</td>
<td style="text-align: left;">numeric</td>
</tr>
<tr class="odd">
<td style="text-align: left;">diet_low_in_whole_grains</td>
<td style="text-align: left;">numeric</td>
</tr>
<tr class="even">
<td style="text-align: left;">diet_low_in_nuts.and_seeds</td>
<td style="text-align: left;">numeric</td>
</tr>
</tbody>
</table>
