# **Géo’DAE - Base Nationale des Défibrillateurs**

## Description du fichier

<table>
<colgroup>
<col style="width: 5%" />
<col style="width: 79%" />
<col style="width: 14%" />
</colgroup>
<tbody>
<tr class="odd">
<td style="text-align: left;">Description</td>
<td style="text-align: center;">Géo’DAE est la base de données nationale des défibrillateurs automatisés externes (DAE), recensés en France.</td>
<td><a href="https://solidarites-sante.gouv.fr/IMG/pdf/geodae_plaquette_de_communication_2021.pdf" class="uri">https://solidarites-sante.gouv.fr/IMG/pdf/geodae_plaquette_de_communication_2021.pdf</a></td>
</tr>
<tr class="even">
<td style="text-align: left;">Téléchargement</td>
<td style="text-align: center;">Disponible sur le site data.gouv.fr</td>
<td><a href="https://www.data.gouv.fr/fr/datasets/geodae-base-nationale-des-defibrillateurs/" class="uri">https://www.data.gouv.fr/fr/datasets/geodae-base-nationale-des-defibrillateurs/</a></td>
</tr>
<tr class="odd">
<td style="text-align: left;">Producteur</td>
<td style="text-align: center;"><strong><em>AtlaSanté</em></strong>, portail de l’information géographique des ARS et du Ministère de la Santé et des Affaires Sociales</td>
<td><p><a href="https://www.atlasante.fr/accueil" class="uri">https://www.atlasante.fr/accueil</a></p>
<p><a href="https://geodae.atlasante.fr/apropos" class="uri">https://geodae.atlasante.fr/apropos</a></p></td>
</tr>
<tr class="even">
<td style="text-align: left;">Licence</td>
<td style="text-align: center;">Licence Ouverte / Open Licence</td>
<td><a href="https://www.etalab.gouv.fr/licence-ouverte-open-licence" class="uri">https://www.etalab.gouv.fr/licence-ouverte-open-licence</a></td>
</tr>
<tr class="odd">
<td style="text-align: left;">Format</td>
<td style="text-align: center;">Fichier CSV</td>
<td></td>
</tr>
<tr class="even">
<td style="text-align: left;">Encodage</td>
<td style="text-align: center;">UTF-8</td>
<td></td>
</tr>
<tr class="odd">
<td style="text-align: left;">Date de Création</td>
<td style="text-align: center;">30/09/2021</td>
<td></td>
</tr>
<tr class="even">
<td style="text-align: left;">Dernière MàJ</td>
<td style="text-align: center;">30/09/2021</td>
<td></td>
</tr>
<tr class="odd">
<td style="text-align: left;">Date de téléchargement du fichier</td>
<td style="text-align: center;">05/10/2021</td>
<td></td>
</tr>
<tr class="even">
<td style="text-align: left;">Couverture territoriale</td>
<td style="text-align: center;">FRANCE</td>
<td></td>
</tr>
<tr class="odd">
<td style="text-align: left;">Granularité</td>
<td style="text-align: center;">Point d’intérêt</td>
<td></td>
</tr>
<tr class="even">
<td style="text-align: left;">Raison / Méthode de création</td>
<td style="text-align: center;">Le plan national de santé publique prévoit […] d’améliorer l’accès aux défibrillateurs automatisés externes sur le territoire national, en favorisant leur géolocalisation et leur maintenance. La création d’une base de données nationale, disposition de la loi du 28 juin 2018 relative au défibrillateur cardiaque, s’inscrit dans cette ambition. Tous les exploitants de DAE doivent désormais déclarer les données de leurs défibrillateurs et leurs caractéristiques dans la base nationale.</td>
<td></td>
</tr>
</tbody>
</table>

## Intérêt et possibilités

-   Cartographies et statistiques en croisant avec d’autres bases de
    données et à l’aide des coordonnées géographiques

-   Exercice de datamanagement

-   Indicence des décès par arrêts cardiaque en fonction de la
    localisation

-   Densite de population à X Km d’un défibrillateur

-   Défibrillateur le plus proche par rapport à une commune

-   …

## Description des données

### Variables

<table>
<colgroup>
<col style="width: 9%" />
<col style="width: 38%" />
<col style="width: 10%" />
<col style="width: 41%" />
</colgroup>
<thead>
<tr class="header">
<th>Champ</th>
<th>Libellé</th>
<th>Type</th>
<th>Exemple</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>gml_id</td>
<td>Non renseigné dans la documentation</td>
<td>character</td>
<td>“geodae_publique.1” “geodae_publique.2” “geodae_publique.3”</td>
</tr>
<tr class="even">
<td>gid</td>
<td>Identifiant interne du DAE dans la base de données</td>
<td>Integer</td>
<td>1 2 3 4 5 6 7 8 9 10 …</td>
</tr>
<tr class="odd">
<td>c_gid</td>
<td>Non renseigné dans la documentation</td>
<td>character</td>
<td>1895 1896 1897 1898 1899 1900 1902 1903 1904 1905 …</td>
</tr>
<tr class="even">
<td>c_etat_valid</td>
<td>Validation par l’exploitant des données</td>
<td>character (qualitative discrète)</td>
<td>“validées” “en attente de validation” “validées” “en attente de validation” …</td>
</tr>
<tr class="odd">
<td>c_nom</td>
<td>Nom donné au DAE</td>
<td>character</td>
<td>“DAE G5 CARDIAC SCIENCE” “DAE-SSR PRIMEROSE” “DAE NANOOMTECH”…</td>
</tr>
<tr class="even">
<td>c_lat_coor1</td>
<td>Latitude</td>
<td>numeric</td>
<td>47.8 43.7 46.8 45.8 45.6 …</td>
</tr>
<tr class="odd">
<td>c_long_coor1</td>
<td>Longitude</td>
<td>numeric</td>
<td>-3.36 -1.44 4.86 4.83 4.05 …</td>
</tr>
<tr class="even">
<td>c_xy_precis</td>
<td>Précision des coordonnées X et Y</td>
<td>numeric</td>
<td>1 1 1 1 1 …</td>
</tr>
<tr class="odd">
<td>c_id_adr</td>
<td>Clé d’interopérabilité</td>
<td>character</td>
<td>“56121_0990_00009” “40304_0312_00187” “71076_7017_00009”</td>
</tr>
<tr class="even">
<td>c_adr_num</td>
<td>Numéro de la voie et, le cas échéant, sufixe, d’implantation du DAE</td>
<td>character</td>
<td>“9” “200” “9” “6” …</td>
</tr>
<tr class="odd">
<td>c_adr_voie</td>
<td>Type et nom de la voie ou lieu-dit d’implantation du DAE</td>
<td>character</td>
<td>“Cours Louis de Chazelles” “Avenue de Gaujacq” …</td>
</tr>
<tr class="even">
<td>c_com_cp</td>
<td>Code postal de la commune d’implantation du DAE</td>
<td>integer</td>
<td>56100 40150 71100 69009 42600 42600 42110 38200 38200 38200 …</td>
</tr>
<tr class="odd">
<td>c_com_insee</td>
<td>Code Insee de la commune d’implantation du DAE</td>
<td>integer</td>
<td>56121 40304 71076 69389 42147 42147 42094 38544 38544 38544 …</td>
</tr>
<tr class="even">
<td>c_com_nom</td>
<td>Nom de la commune d’implantation du DAE</td>
<td>character</td>
<td>“Lorient” “Soorts-Hossegor” “Chalon-sur-Saône” “Lyon” …</td>
</tr>
<tr class="odd">
<td>c_acc</td>
<td>Environnement d’accès du DAE</td>
<td>character</td>
<td>“Intérieur” “Intérieur” “Intérieur” “Intérieur” …</td>
</tr>
<tr class="even">
<td>c_acc_lib</td>
<td>Accès libre du DAE</td>
<td>character (boolean)</td>
<td>“f” “f” “f” “t” …</td>
</tr>
<tr class="odd">
<td>c_acc_pcsec</td>
<td>Poste de sécurité</td>
<td>character (boolean)</td>
<td>“f” “f” “f” “f” …</td>
</tr>
<tr class="even">
<td>c_acc_acc</td>
<td>Présence d’un accueil public</td>
<td>character (boolean)</td>
<td>“t” “t” “t” “f” …</td>
</tr>
<tr class="odd">
<td>c_acc_etg</td>
<td>Etage d’accessibilité du DAE</td>
<td>character</td>
<td>“0” “1” “RDC” “4” …</td>
</tr>
<tr class="even">
<td>c_acc_complt</td>
<td>Complément d’information sur l’accès au DAE</td>
<td>character</td>
<td>“Boutique Transports &amp; Déplacements CTRL” “sur le chariot d’urgence” “A l’infirmerie de l’EHPAD” "" …</td>
</tr>
<tr class="odd">
<td>c_disp_j</td>
<td>Jours d’accessibilité de l’appareil</td>
<td>character</td>
<td>“{lundi,mardi,mercredi,jeudi,vendredi,samedi}” “{7j/7}” “{7j/7}” …</td>
</tr>
<tr class="even">
<td>c_disp_h</td>
<td>Heures d’accessibilité de l’appareil</td>
<td>character</td>
<td>“{\”heures ouvrables\“}” “{\”heures ouvrables\“}” “{\”heures ouvrables\“}” “{24h/24}” …</td>
</tr>
<tr class="odd">
<td>c_etat_fonct</td>
<td>Etat de fonctionnement du DAE</td>
<td>character</td>
<td>“En fonctionnement” “En fonctionnement” “En fonctionnement” “En fonctionnement” …</td>
</tr>
<tr class="even">
<td>c_lc_ped</td>
<td>Présence d’électrodes pédiatriques</td>
<td>character (boolean)</td>
<td>f" “f” “f” "" …</td>
</tr>
<tr class="odd">
<td>c_dispsurv</td>
<td>Dispositif de surveillance à distance du DAE</td>
<td>character (boolean)</td>
<td>f" “f” “f” "" …</td>
</tr>
<tr class="even">
<td>timeposition</td>
<td>Non renseigné dans la documentation</td>
<td>character</td>
<td>“2019-10-09” “2019-06-28” “2019-12-01” “2020-01-01” …</td>
</tr>
<tr class="odd">
<td>c_maj_don.timePosition</td>
<td>Date de la dernière mise à jour des données</td>
<td>character</td>
<td>“2021-05-06” “2020-02-04” “2021-05-06” "" …</td>
</tr>
<tr class="even">
<td>c_expt_siren</td>
<td>Numéro SIREN de l’exploitant</td>
<td>integer</td>
<td>817710650 986520070 352242564 702042755 200034932 200034932 200034932 263800328 263800328 263800328 …</td>
</tr>
<tr class="odd">
<td>c_expt_rais</td>
<td>Raison sociale de l’exploitant, personne morale</td>
<td>character</td>
<td>“RD LORIENT AGGLOMERATION” “MAISON DE REPOS PRIMEROSE” “KORIAN LA VILLA PAPYRI” “CGI FRANCE” …</td>
</tr>
<tr class="even">
<td>c_etat</td>
<td>Non renseigné dans la documentation</td>
<td>character</td>
<td>“Actif” “Actif” “Actif” “Actif” …</td>
</tr>
<tr class="odd">
<td>c_doublon</td>
<td>Non renseigné dans la documentation</td>
<td>character</td>
<td>“f” “f” “f” “f” …</td>
</tr>
<tr class="even">
<td>c__edit_datemaj.timePosition</td>
<td>Non renseigné dans la documentation</td>
<td>character</td>
<td>“2020-02-04T09:47:09Z” “2021-08-17T10:06:52Z” “2020-02-05T15:21:28Z” “2020-02-05T16:44:56Z” …</td>
</tr>
<tr class="odd">
<td>c_srid_origin</td>
<td>Information non renseigné dans la documentation. Semble définir le système de coordonnées géographiques de référence EPSG:4326</td>
<td>character</td>
<td>‘EPSG:4326’::character varying" “‘EPSG:4326’::character varying” “‘EPSG:4326’::character varying” “‘EPSG:4326’::character varying” …</td>
</tr>
<tr class="even">
<td>c_dtpr_lcad.timePosition</td>
<td>Date de péremption des électrodes adultes</td>
<td>character</td>
<td>"" “2020-07-31” “2020-11-30” "" …</td>
</tr>
<tr class="odd">
<td>c_dtpr_bat.timePosition</td>
<td>Date de péremption de la batterie</td>
<td>character</td>
<td>"" “2021-04-30” “2020-11-30” "" …</td>
</tr>
<tr class="even">
<td>c_freq_mnt</td>
<td>Fréquence de la maintenance</td>
<td>character</td>
<td>"" “vérification quotidienne et 1 maintenance annuelle” “Annuelle” "" …</td>
</tr>
<tr class="odd">
<td>c_photo1</td>
<td>Photo 1 du DAE dans son environnement</td>
<td>character</td>
<td>"" "" “5e3acf6805a50.jpeg” "" …</td>
</tr>
<tr class="even">
<td>c_photo2</td>
<td>Photo 12du DAE dans son environnement</td>
<td>character</td>
<td>"" "" “5e3acf68067ef.jpeg” "" …</td>
</tr>
<tr class="odd">
<td>c_date_instal.timePosition</td>
<td>Non renseigné dans la documentation</td>
<td>character</td>
<td>"" "" "" "" …</td>
</tr>
</tbody>
</table>

Pourplus d’informations sur les variables :
<https://carto.atlasante.fr/IHM/cartes/infofactures/geodae/dgs-dae_standard_de_donnees_vf.pdf>

### Dimensions / analyses descriptives (au 05/10/2021)

La base de données comporte 35 867 lignes et 39 colonnes.

## Extrait de la base de données

<table style="width:100%;">
<colgroup>
<col style="width: 2%" />
<col style="width: 0%" />
<col style="width: 0%" />
<col style="width: 3%" />
<col style="width: 3%" />
<col style="width: 1%" />
<col style="width: 1%" />
<col style="width: 1%" />
<col style="width: 2%" />
<col style="width: 1%" />
<col style="width: 4%" />
<col style="width: 1%" />
<col style="width: 1%" />
<col style="width: 2%" />
<col style="width: 1%" />
<col style="width: 1%" />
<col style="width: 1%" />
<col style="width: 1%" />
<col style="width: 1%" />
<col style="width: 7%" />
<col style="width: 6%" />
<col style="width: 2%" />
<col style="width: 2%" />
<col style="width: 1%" />
<col style="width: 1%" />
<col style="width: 1%" />
<col style="width: 3%" />
<col style="width: 1%" />
<col style="width: 3%" />
<col style="width: 0%" />
<col style="width: 1%" />
<col style="width: 3%" />
<col style="width: 4%" />
<col style="width: 3%" />
<col style="width: 3%" />
<col style="width: 6%" />
<col style="width: 2%" />
<col style="width: 2%" />
<col style="width: 3%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">gml_id</th>
<th style="text-align: right;">gid</th>
<th style="text-align: right;">c_gid</th>
<th style="text-align: left;">c_etat_valid</th>
<th style="text-align: left;">c_nom</th>
<th style="text-align: right;">c_lat_coor1</th>
<th style="text-align: right;">c_long_coor1</th>
<th style="text-align: right;">c_xy_precis</th>
<th style="text-align: left;">c_id_adr</th>
<th style="text-align: left;">c_adr_num</th>
<th style="text-align: left;">c_adr_voie</th>
<th style="text-align: right;">c_com_cp</th>
<th style="text-align: right;">c_com_insee</th>
<th style="text-align: left;">c_com_nom</th>
<th style="text-align: left;">c_acc</th>
<th style="text-align: left;">c_acc_lib</th>
<th style="text-align: left;">c_acc_pcsec</th>
<th style="text-align: left;">c_acc_acc</th>
<th style="text-align: left;">c_acc_etg</th>
<th style="text-align: left;">c_acc_complt</th>
<th style="text-align: left;">c_disp_j</th>
<th style="text-align: left;">c_disp_h</th>
<th style="text-align: left;">c_etat_fonct</th>
<th style="text-align: left;">c_lc_ped</th>
<th style="text-align: left;">c_dispsurv</th>
<th style="text-align: left;">timePosition</th>
<th style="text-align: left;">c_maj_don.timePosition</th>
<th style="text-align: right;">c_expt_siren</th>
<th style="text-align: left;">c_expt_rais</th>
<th style="text-align: left;">c_etat</th>
<th style="text-align: left;">c_doublon</th>
<th style="text-align: left;">c__edit_datemaj.timePosition</th>
<th style="text-align: left;">c_srid_origin</th>
<th style="text-align: left;">c_dtpr_lcad.timePosition</th>
<th style="text-align: left;">c_dtpr_bat.timePosition</th>
<th style="text-align: left;">c_freq_mnt</th>
<th style="text-align: left;">c_photo1</th>
<th style="text-align: left;">c_photo2</th>
<th style="text-align: left;">c_date_instal.timePosition</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">geodae_publique.1</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">1895</td>
<td style="text-align: left;">validées</td>
<td style="text-align: left;">DAE G5 CARDIAC SCIENCE</td>
<td style="text-align: right;">47.7552</td>
<td style="text-align: right;">-3.36392</td>
<td style="text-align: right;">1.000000</td>
<td style="text-align: left;">56121_0990_00009</td>
<td style="text-align: left;">9</td>
<td style="text-align: left;">Cours Louis de Chazelles</td>
<td style="text-align: right;">56100</td>
<td style="text-align: right;">56121</td>
<td style="text-align: left;">Lorient</td>
<td style="text-align: left;">Intérieur</td>
<td style="text-align: left;">f</td>
<td style="text-align: left;">f</td>
<td style="text-align: left;">t</td>
<td style="text-align: left;">0</td>
<td style="text-align: left;">Boutique Transports &amp; Déplacements CTRL</td>
<td style="text-align: left;">{lundi,mardi,mercredi,jeudi,vendredi,samedi}</td>
<td style="text-align: left;">{“heures ouvrables”}</td>
<td style="text-align: left;">En fonctionnement</td>
<td style="text-align: left;">f</td>
<td style="text-align: left;">f</td>
<td style="text-align: left;">2019-10-09</td>
<td style="text-align: left;">2021-05-06</td>
<td style="text-align: right;">817710650</td>
<td style="text-align: left;">RD LORIENT AGGLOMERATION</td>
<td style="text-align: left;">Actif</td>
<td style="text-align: left;">f</td>
<td style="text-align: left;">2020-02-04T09:47:09Z</td>
<td style="text-align: left;">‘EPSG:4326’::character varying</td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;">geodae_publique.2</td>
<td style="text-align: right;">2</td>
<td style="text-align: right;">1896</td>
<td style="text-align: left;">en attente de validation</td>
<td style="text-align: left;">DAE-SSR PRIMEROSE</td>
<td style="text-align: right;">43.6586</td>
<td style="text-align: right;">-1.43754</td>
<td style="text-align: right;">1.000000</td>
<td style="text-align: left;">40304_0312_00187</td>
<td style="text-align: left;">200</td>
<td style="text-align: left;">Avenue de Gaujacq</td>
<td style="text-align: right;">40150</td>
<td style="text-align: right;">40304</td>
<td style="text-align: left;">Soorts-Hossegor</td>
<td style="text-align: left;">Intérieur</td>
<td style="text-align: left;">f</td>
<td style="text-align: left;">f</td>
<td style="text-align: left;">t</td>
<td style="text-align: left;">1</td>
<td style="text-align: left;">sur le chariot d’urgence</td>
<td style="text-align: left;">{7j/7}</td>
<td style="text-align: left;">{“heures ouvrables”}</td>
<td style="text-align: left;">En fonctionnement</td>
<td style="text-align: left;">f</td>
<td style="text-align: left;">f</td>
<td style="text-align: left;">2019-06-28</td>
<td style="text-align: left;">2020-02-04</td>
<td style="text-align: right;">986520070</td>
<td style="text-align: left;">MAISON DE REPOS PRIMEROSE</td>
<td style="text-align: left;">Actif</td>
<td style="text-align: left;">f</td>
<td style="text-align: left;">2021-08-17T10:06:52Z</td>
<td style="text-align: left;">‘EPSG:4326’::character varying</td>
<td style="text-align: left;">2020-07-31</td>
<td style="text-align: left;">2021-04-30</td>
<td style="text-align: left;">vérification quotidienne et 1 maintenance annuelle</td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;">geodae_publique.3</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">1897</td>
<td style="text-align: left;">validées</td>
<td style="text-align: left;">DAE NANOOMTECH</td>
<td style="text-align: right;">46.7966</td>
<td style="text-align: right;">4.85640</td>
<td style="text-align: right;">1.000000</td>
<td style="text-align: left;">71076_7017_00009</td>
<td style="text-align: left;">9</td>
<td style="text-align: left;">Allee de Saint Jean des Vignes</td>
<td style="text-align: right;">71100</td>
<td style="text-align: right;">71076</td>
<td style="text-align: left;">Chalon-sur-Saône</td>
<td style="text-align: left;">Intérieur</td>
<td style="text-align: left;">f</td>
<td style="text-align: left;">f</td>
<td style="text-align: left;">t</td>
<td style="text-align: left;">RDC</td>
<td style="text-align: left;">A l’infirmerie de l’EHPAD</td>
<td style="text-align: left;">{7j/7}</td>
<td style="text-align: left;">{“heures ouvrables”}</td>
<td style="text-align: left;">En fonctionnement</td>
<td style="text-align: left;">f</td>
<td style="text-align: left;">f</td>
<td style="text-align: left;">2019-12-01</td>
<td style="text-align: left;">2021-05-06</td>
<td style="text-align: right;">352242564</td>
<td style="text-align: left;">KORIAN LA VILLA PAPYRI</td>
<td style="text-align: left;">Actif</td>
<td style="text-align: left;">f</td>
<td style="text-align: left;">2020-02-05T15:21:28Z</td>
<td style="text-align: left;">‘EPSG:4326’::character varying</td>
<td style="text-align: left;">2020-11-30</td>
<td style="text-align: left;">2020-11-30</td>
<td style="text-align: left;">Annuelle</td>
<td style="text-align: left;">5e3acf6805a50.jpeg</td>
<td style="text-align: left;">5e3acf68067ef.jpeg</td>
<td style="text-align: left;"></td>
</tr>
<tr class="even">
<td style="text-align: left;">geodae_publique.4</td>
<td style="text-align: right;">4</td>
<td style="text-align: right;">1898</td>
<td style="text-align: left;">en attente de validation</td>
<td style="text-align: left;">Test 9 05022020</td>
<td style="text-align: right;">45.7965</td>
<td style="text-align: right;">4.82597</td>
<td style="text-align: right;">1.000000</td>
<td style="text-align: left;">69389_1866_00006</td>
<td style="text-align: left;">6</td>
<td style="text-align: left;">Rue Communieu</td>
<td style="text-align: right;">69009</td>
<td style="text-align: right;">69389</td>
<td style="text-align: left;">Lyon</td>
<td style="text-align: left;">Intérieur</td>
<td style="text-align: left;">t</td>
<td style="text-align: left;">f</td>
<td style="text-align: left;">f</td>
<td style="text-align: left;">4</td>
<td style="text-align: left;"></td>
<td style="text-align: left;">{7j/7}</td>
<td style="text-align: left;">{24h/24}</td>
<td style="text-align: left;">En fonctionnement</td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;">2020-01-01</td>
<td style="text-align: left;"></td>
<td style="text-align: right;">702042755</td>
<td style="text-align: left;">CGI FRANCE</td>
<td style="text-align: left;">Actif</td>
<td style="text-align: left;">f</td>
<td style="text-align: left;">2020-02-05T16:44:56Z</td>
<td style="text-align: left;">‘EPSG:4326’::character varying</td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
</tr>
<tr class="odd">
<td style="text-align: left;">geodae_publique.5</td>
<td style="text-align: right;">5</td>
<td style="text-align: right;">1899</td>
<td style="text-align: left;">validées</td>
<td style="text-align: left;">DAE Hôpital Montbrison</td>
<td style="text-align: right;">45.6127</td>
<td style="text-align: right;">4.04854</td>
<td style="text-align: right;">1.000000</td>
<td style="text-align: left;">42147_0699_00010</td>
<td style="text-align: left;">10</td>
<td style="text-align: left;">Avenue des Monts du Soir</td>
<td style="text-align: right;">42600</td>
<td style="text-align: right;">42147</td>
<td style="text-align: left;">Montbrison</td>
<td style="text-align: left;">Extérieur</td>
<td style="text-align: left;">t</td>
<td style="text-align: left;">t</td>
<td style="text-align: left;">t</td>
<td style="text-align: left;">0</td>
<td style="text-align: left;">Entrée principale du bâtiment, au niveau du dépose minute</td>
<td style="text-align: left;">{7j/7}</td>
<td style="text-align: left;">{24h/24}</td>
<td style="text-align: left;">En fonctionnement</td>
<td style="text-align: left;">f</td>
<td style="text-align: left;">f</td>
<td style="text-align: left;">2019-11-18</td>
<td style="text-align: left;">2021-05-06</td>
<td style="text-align: right;">200034932</td>
<td style="text-align: left;">CENTRE HOSPITALIER DU FOREZ</td>
<td style="text-align: left;">Actif</td>
<td style="text-align: left;">f</td>
<td style="text-align: left;">2020-02-07T10:18:00Z</td>
<td style="text-align: left;">‘EPSG:4326’::character varying</td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;">1 fois par an</td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;">2019-11-18</td>
</tr>
<tr class="even">
<td style="text-align: left;">geodae_publique.6</td>
<td style="text-align: right;">6</td>
<td style="text-align: right;">1900</td>
<td style="text-align: left;">validées</td>
<td style="text-align: left;">DAE EHPAD Hall - Montbrison</td>
<td style="text-align: right;">45.6098</td>
<td style="text-align: right;">4.06093</td>
<td style="text-align: right;">0.999999</td>
<td style="text-align: left;">42147_0400_00022</td>
<td style="text-align: left;">22</td>
<td style="text-align: left;">Rue du Faubourg de la Croix</td>
<td style="text-align: right;">42600</td>
<td style="text-align: right;">42147</td>
<td style="text-align: left;">Montbrison</td>
<td style="text-align: left;">Intérieur</td>
<td style="text-align: left;">t</td>
<td style="text-align: left;">f</td>
<td style="text-align: left;">t</td>
<td style="text-align: left;">0</td>
<td style="text-align: left;">Hall d’accueil du bâtiment</td>
<td style="text-align: left;">{7j/7}</td>
<td style="text-align: left;">{“heures ouvrables”}</td>
<td style="text-align: left;">En fonctionnement</td>
<td style="text-align: left;">f</td>
<td style="text-align: left;">f</td>
<td style="text-align: left;">2020-02-06</td>
<td style="text-align: left;">2021-05-06</td>
<td style="text-align: right;">200034932</td>
<td style="text-align: left;">CENTRE HOSPITALIER DU FOREZ</td>
<td style="text-align: left;">Actif</td>
<td style="text-align: left;">f</td>
<td style="text-align: left;">2020-02-07T11:13:24Z</td>
<td style="text-align: left;">‘EPSG:4326’::character varying</td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;">1 fois par an</td>
<td style="text-align: left;"></td>
<td style="text-align: left;"></td>
<td style="text-align: left;">2014-06-27</td>
</tr>
</tbody>
</table>

## Améliorations

Les données brutes ouvertes en csv présentent des défauts :

-   Mauvais typage des variables essentielles

-   Colonnes potentiellement inutiles

Le script R suivant propose une amélioration du jeu de données initial :

    library(dplyr)
    df <- read.csv("geodae-publique-2-.csv", encoding = "UTF-8") %>%
      select(-c_photo1, -c_photo2, -c_date_instal.timePosition)

    df$c_acc_lib <- as.factor(df$c_acc_lib)
    df$c_acc_pcsec <- as.factor(df$c_acc_pcsec)
    df$c_acc_acc <- as.factor(df$c_acc_acc)
    df$c_acc <- as.factor(df$c_acc)
    df$timePosition <- as.Date(df$timePosition)
    df$c_maj_don.timePosition <- as.Date(df$c_maj_don.timePosition)
    # le typage des variables dépend des besoins
