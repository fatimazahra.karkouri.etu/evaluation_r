<table>
<colgroup>
<col style="width: 62%" />
<col style="width: 22%" />
<col style="width: 15%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">nom_long</th>
<th style="text-align: left;">nom_court</th>
<th style="text-align: left;">type</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">Finess juridique</td>
<td style="text-align: left;">finess</td>
<td style="text-align: left;">chaîne de caractères</td>
</tr>
<tr class="even">
<td style="text-align: left;">Finess juridique - raison sociale</td>
<td style="text-align: left;">rs_finess</td>
<td style="text-align: left;">chaîne de caractères</td>
</tr>
<tr class="odd">
<td style="text-align: left;">Finess géographique</td>
<td style="text-align: left;">finess_geo</td>
<td style="text-align: left;">chaîne de caractères</td>
</tr>
<tr class="even">
<td style="text-align: left;">Finess géographique - raison sociale</td>
<td style="text-align: left;">rs_finess_geo</td>
<td style="text-align: left;">chaîne de caractères</td>
</tr>
<tr class="odd">
<td style="text-align: left;">Région</td>
<td style="text-align: left;">region</td>
<td style="text-align: left;">qualitative</td>
</tr>
<tr class="even">
<td style="text-align: left;">Participation obligatoire ou facultative</td>
<td style="text-align: left;">participation</td>
<td style="text-align: left;">qualitative</td>
</tr>
<tr class="odd">
<td style="text-align: left;">Dépot d’au moins une adresse mail valide</td>
<td style="text-align: left;">Depot</td>
<td style="text-align: left;">qualitative</td>
</tr>
<tr class="even">
<td style="text-align: left;">Nombre de réponses - Satisfaction globale des patients</td>
<td style="text-align: left;">nb_rep_score_all_rea_ajust</td>
<td style="text-align: left;">quantitative</td>
</tr>
<tr class="odd">
<td style="text-align: left;">Note ajustée de satisfaction globale des patients</td>
<td style="text-align: left;">score_all_rea_ajust</td>
<td style="text-align: left;">quantitative</td>
</tr>
<tr class="even">
<td style="text-align: left;">Classement de l’hôpital</td>
<td style="text-align: left;">classement</td>
<td style="text-align: left;">qualitative</td>
</tr>
<tr class="odd">
<td style="text-align: left;">Evolution du classement entre 2019 et 2020</td>
<td style="text-align: left;">evolution</td>
<td style="text-align: left;">qualitative</td>
</tr>
<tr class="even">
<td style="text-align: left;">Nombre de réponses - Satisfaction de l’accueil</td>
<td style="text-align: left;">nb_rep_score_accueil_rea_ajust</td>
<td style="text-align: left;">quantitative</td>
</tr>
<tr class="odd">
<td style="text-align: left;">Note ajustée de satisfaction de l’accueil</td>
<td style="text-align: left;">score_accueil_rea_ajust</td>
<td style="text-align: left;">quantitative</td>
</tr>
<tr class="even">
<td style="text-align: left;">Nombre de réponses - Satisfaction de la prise en charge infirmiers/aides-soignants</td>
<td style="text-align: left;">nb_rep_score_PECinf_rea_ajust</td>
<td style="text-align: left;">quantitative</td>
</tr>
<tr class="odd">
<td style="text-align: left;">Note ajustée de satisfaction de la prise en charge infirmiers/aides-soignants</td>
<td style="text-align: left;">score_PECinf_rea_ajust</td>
<td style="text-align: left;">quantitative</td>
</tr>
<tr class="even">
<td style="text-align: left;">Nombre de réponses - Satisfaction de la prise en charge par les médecins/chirurgiens</td>
<td style="text-align: left;">nb_rep_score_PECmed_rea_ajust</td>
<td style="text-align: left;">quantitative</td>
</tr>
<tr class="odd">
<td style="text-align: left;">Note ajustée de satisfaction de la prise en charge par les médecins/chirurgiens</td>
<td style="text-align: left;">score_PECmed_rea_ajust</td>
<td style="text-align: left;">quantitative</td>
</tr>
<tr class="even">
<td style="text-align: left;">Nombre de réponses - Satisfaction de la chambre</td>
<td style="text-align: left;">nb_rep_score_chambre_rea_ajust</td>
<td style="text-align: left;">quantitative</td>
</tr>
<tr class="odd">
<td style="text-align: left;">Note ajustée de satisfaction de la chambre</td>
<td style="text-align: left;">score_chambre_rea_ajust</td>
<td style="text-align: left;">quantitative</td>
</tr>
<tr class="even">
<td style="text-align: left;">Nombre de réponses - Satisfaction des repas</td>
<td style="text-align: left;">nb_rep_score_repas_rea_ajust</td>
<td style="text-align: left;">quantitative</td>
</tr>
<tr class="odd">
<td style="text-align: left;">Note ajustée de satisfaction des repas</td>
<td style="text-align: left;">score_repas_rea_ajust</td>
<td style="text-align: left;">quantitative</td>
</tr>
<tr class="even">
<td style="text-align: left;">Nombre de réponses - Satisfaction de la sortie</td>
<td style="text-align: left;">nb_rep_score_sortie_rea_ajust</td>
<td style="text-align: left;">quantitative</td>
</tr>
<tr class="odd">
<td style="text-align: left;">Note ajustée de satisfaction de la sortie</td>
<td style="text-align: left;">score_sortie_rea_ajust</td>
<td style="text-align: left;">quantitative</td>
</tr>
<tr class="even">
<td style="text-align: left;">Pourcentage de patients recommandant certainement l’établissement</td>
<td style="text-align: left;">taux_reco_brut</td>
<td style="text-align: left;">quantitative</td>
</tr>
<tr class="odd">
<td style="text-align: left;">Nombre de patients recommandant certainement l’établissement</td>
<td style="text-align: left;">nb_reco_brut</td>
<td style="text-align: left;">quantitative</td>
</tr>
</tbody>
</table>
