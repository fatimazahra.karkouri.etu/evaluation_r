# Présentation

Ce dataframe contient les résultats nationaux des indicateurs de qualité
et sécurité des soins mesurés en 2020 concernant la satisfaction et
expérience des patients hospitalisé pendant plus de 48h.

source:<https://www.data.gouv.fr/fr/datasets/Indicateurs_de-qualite-et-de-securite-des-soins-recueil-2020/>

source:<https://www.has-sante.fr/jcms/c_970821/fr/resultats-des-Indicateurs_et-impact-des-dispositifs-d-amelioration-de-la-qualite-iqss>

# Intérêt

Outre la comparaison entre les différents hopitaux, ce dataframe
pourrait être utiliser pour évaluer le lien entre ces scores de
satisfaction et les performances d’un hôpital. Par exemple, pour une
pathologie ou prise en charge chirurgicale donnée, nous pourrions
rechercher si il existe un lien entre les scores de satisfaction et le
taux de réadmission dans les hôpitaux.

# Variables

<table>
<colgroup>
<col style="width: 17%" />
<col style="width: 82%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">NAME</th>
<th style="text-align: left;">LABEL</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">Depot</td>
<td style="text-align: left;">Dépot d’au moins une adresse mail valide</td>
</tr>
<tr class="even">
<td style="text-align: left;">classement</td>
<td style="text-align: left;">Classe A : &gt;= à 77.3, Classe B : &gt;= à 74 et &lt; à 77.3,Classe C : &gt;= à 70.7 et &lt; à 74,Classe D : &lt; à 70.7, DI : moins de 30 réponses exploitables</td>
</tr>
<tr class="odd">
<td style="text-align: left;">evolution</td>
<td style="text-align: left;">Changement de classe entre 2019 et 2020</td>
</tr>
<tr class="even">
<td style="text-align: left;">finess</td>
<td style="text-align: left;">finess juridique</td>
</tr>
<tr class="odd">
<td style="text-align: left;">finess_geo</td>
<td style="text-align: left;">finess géographique</td>
</tr>
<tr class="even">
<td style="text-align: left;">nb_reco_brut</td>
<td style="text-align: left;">Nombre de patients recommandant certainement l’établissement</td>
</tr>
<tr class="odd">
<td style="text-align: left;">nb_rep_score_PECinf_rea_ajust</td>
<td style="text-align: left;">N réponses - Satisfaction de la prise en charge infirmiers/aides-soignants</td>
</tr>
<tr class="even">
<td style="text-align: left;">nb_rep_score_PECmed_rea_ajust</td>
<td style="text-align: left;">N réponses - Satisfaction de la prise en charge par les médecins/chirurgiens</td>
</tr>
<tr class="odd">
<td style="text-align: left;">nb_rep_score_accueil_rea_ajust</td>
<td style="text-align: left;">N réponses - Satisfaction de l’accueil</td>
</tr>
<tr class="even">
<td style="text-align: left;">nb_rep_score_all_rea_ajust</td>
<td style="text-align: left;">N réponses - Satisfaction globale des patients</td>
</tr>
<tr class="odd">
<td style="text-align: left;">nb_rep_score_chambre_rea_ajust</td>
<td style="text-align: left;">N réponses - Satisfaction de la chambre</td>
</tr>
<tr class="even">
<td style="text-align: left;">nb_rep_score_repas_rea_ajust</td>
<td style="text-align: left;">N réponses - Satisfaction des repas</td>
</tr>
<tr class="odd">
<td style="text-align: left;">nb_rep_score_sortie_rea_ajust</td>
<td style="text-align: left;">N réponses - Satisfaction de la sortie</td>
</tr>
<tr class="even">
<td style="text-align: left;">participation</td>
<td style="text-align: left;">Participation obligatoire ou facultative</td>
</tr>
<tr class="odd">
<td style="text-align: left;">region</td>
<td style="text-align: left;">Région</td>
</tr>
<tr class="even">
<td style="text-align: left;">rs_finess</td>
<td style="text-align: left;">finess juridique - raison sociale</td>
</tr>
<tr class="odd">
<td style="text-align: left;">rs_finess_geo</td>
<td style="text-align: left;">finess géographique - raison sociale</td>
</tr>
<tr class="even">
<td style="text-align: left;">score_PECinf_rea_ajust</td>
<td style="text-align: left;">Note ajustée de satisfaction de la prise en charge infirmiers/aides-soignants</td>
</tr>
<tr class="odd">
<td style="text-align: left;">score_PECmed_rea_ajust</td>
<td style="text-align: left;">Note ajustée de satisfaction de la prise en charge par les médecins/chirurgiens</td>
</tr>
<tr class="even">
<td style="text-align: left;">score_accueil_rea_ajust</td>
<td style="text-align: left;">Note ajustée de satisfaction de l’accueil</td>
</tr>
<tr class="odd">
<td style="text-align: left;">score_all_rea_ajust</td>
<td style="text-align: left;">Note ajustée de satisfaction globale des patients</td>
</tr>
<tr class="even">
<td style="text-align: left;">score_chambre_rea_ajust</td>
<td style="text-align: left;">Note ajustée de satisfaction de la chambre</td>
</tr>
<tr class="odd">
<td style="text-align: left;">score_repas_rea_ajust</td>
<td style="text-align: left;">Note ajustée de satisfaction des repas</td>
</tr>
<tr class="even">
<td style="text-align: left;">score_sortie_rea_ajust</td>
<td style="text-align: left;">Note ajustée de satisfaction de la sortie</td>
</tr>
<tr class="odd">
<td style="text-align: left;">taux_reco_brut</td>
<td style="text-align: left;">% de patients recommandant certainement l’établissement</td>
</tr>
</tbody>
</table>

# Extrait du dataframe

<table>
<colgroup>
<col style="width: 1%" />
<col style="width: 5%" />
<col style="width: 2%" />
<col style="width: 5%" />
<col style="width: 3%" />
<col style="width: 2%" />
<col style="width: 1%" />
<col style="width: 4%" />
<col style="width: 3%" />
<col style="width: 2%" />
<col style="width: 1%" />
<col style="width: 5%" />
<col style="width: 4%" />
<col style="width: 5%" />
<col style="width: 4%" />
<col style="width: 5%" />
<col style="width: 4%" />
<col style="width: 5%" />
<col style="width: 4%" />
<col style="width: 5%" />
<col style="width: 4%" />
<col style="width: 5%" />
<col style="width: 4%" />
<col style="width: 2%" />
<col style="width: 2%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;">finess</th>
<th style="text-align: left;">rs_finess</th>
<th style="text-align: left;">finess_geo</th>
<th style="text-align: left;">rs_finess_geo</th>
<th style="text-align: left;">region</th>
<th style="text-align: left;">participation</th>
<th style="text-align: left;">Depot</th>
<th style="text-align: left;">nb_rep_score_all_rea_ajust</th>
<th style="text-align: right;">score_all_rea_ajust</th>
<th style="text-align: left;">classement</th>
<th style="text-align: left;">evolution</th>
<th style="text-align: right;">nb_rep_score_accueil_rea_ajust</th>
<th style="text-align: left;">score_accueil_rea_ajust</th>
<th style="text-align: right;">nb_rep_score_PECinf_rea_ajust</th>
<th style="text-align: left;">score_PECinf_rea_ajust</th>
<th style="text-align: right;">nb_rep_score_PECmed_rea_ajust</th>
<th style="text-align: left;">score_PECmed_rea_ajust</th>
<th style="text-align: right;">nb_rep_score_chambre_rea_ajust</th>
<th style="text-align: left;">score_chambre_rea_ajust</th>
<th style="text-align: right;">nb_rep_score_repas_rea_ajust</th>
<th style="text-align: left;">score_repas_rea_ajust</th>
<th style="text-align: right;">nb_rep_score_sortie_rea_ajust</th>
<th style="text-align: left;">score_sortie_rea_ajust</th>
<th style="text-align: right;">taux_reco_brut</th>
<th style="text-align: right;">nb_reco_brut</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">010780054</td>
<td style="text-align: left;">CH DE BOURG EN BRESSE FLEYRIAT</td>
<td style="text-align: left;">010000024</td>
<td style="text-align: left;">CH DE FLEYRIAT</td>
<td style="text-align: left;">Auvergne-Rhône-Alpes</td>
<td style="text-align: left;">1- Obligatoire</td>
<td style="text-align: left;">1- Oui</td>
<td style="text-align: left;">572</td>
<td style="text-align: right;">73.94</td>
<td style="text-align: left;">C</td>
<td style="text-align: left;">NA</td>
<td style="text-align: right;">572</td>
<td style="text-align: left;">71.69</td>
<td style="text-align: right;">572</td>
<td style="text-align: left;">81.98</td>
<td style="text-align: right;">572</td>
<td style="text-align: left;">80.89</td>
<td style="text-align: right;">572</td>
<td style="text-align: left;">73.6</td>
<td style="text-align: right;">572</td>
<td style="text-align: left;">60.63</td>
<td style="text-align: right;">572</td>
<td style="text-align: left;">64.99</td>
<td style="text-align: right;">55.5</td>
<td style="text-align: right;">568</td>
</tr>
<tr class="even">
<td style="text-align: left;">010780062</td>
<td style="text-align: left;">CH DOCTEUR RECAMIER</td>
<td style="text-align: left;">010000032</td>
<td style="text-align: left;">CH HAUT BUGEY</td>
<td style="text-align: left;">Auvergne-Rhône-Alpes</td>
<td style="text-align: left;">1- Obligatoire</td>
<td style="text-align: left;">1- Oui</td>
<td style="text-align: left;">313</td>
<td style="text-align: right;">70.93</td>
<td style="text-align: left;">C</td>
<td style="text-align: left;">NA</td>
<td style="text-align: right;">313</td>
<td style="text-align: left;">69.57</td>
<td style="text-align: right;">313</td>
<td style="text-align: left;">80.57</td>
<td style="text-align: right;">313</td>
<td style="text-align: left;">77.39</td>
<td style="text-align: right;">313</td>
<td style="text-align: left;">67.39</td>
<td style="text-align: right;">313</td>
<td style="text-align: left;">61.11</td>
<td style="text-align: right;">313</td>
<td style="text-align: left;">60.84</td>
<td style="text-align: right;">47.6</td>
<td style="text-align: right;">313</td>
</tr>
<tr class="odd">
<td style="text-align: left;">010780096</td>
<td style="text-align: left;">CH DE MONTPENSIER TREVOUX</td>
<td style="text-align: left;">010000065</td>
<td style="text-align: left;">CH DE TREVOUX - MONTPENSIER</td>
<td style="text-align: left;">Auvergne-Rhône-Alpes</td>
<td style="text-align: left;">2- Facultatif</td>
<td style="text-align: left;">1- Oui</td>
<td style="text-align: left;">NA</td>
<td style="text-align: right;">NA</td>
<td style="text-align: left;">DI</td>
<td style="text-align: left;">NA</td>
<td style="text-align: right;">NA</td>
<td style="text-align: left;">NA</td>
<td style="text-align: right;">NA</td>
<td style="text-align: left;">NA</td>
<td style="text-align: right;">NA</td>
<td style="text-align: left;">NA</td>
<td style="text-align: right;">NA</td>
<td style="text-align: left;">NA</td>
<td style="text-align: right;">NA</td>
<td style="text-align: left;">NA</td>
<td style="text-align: right;">NA</td>
<td style="text-align: left;">NA</td>
<td style="text-align: right;">NA</td>
<td style="text-align: right;">NA</td>
</tr>
<tr class="even">
<td style="text-align: left;">010008407</td>
<td style="text-align: left;">CH DU HAUT BUGEY</td>
<td style="text-align: left;">010005239</td>
<td style="text-align: left;">CH DU HAUT BUGEY - GEOVREISSET</td>
<td style="text-align: left;">Auvergne-Rhône-Alpes</td>
<td style="text-align: left;">1- Obligatoire</td>
<td style="text-align: left;">1- Oui</td>
<td style="text-align: left;">36</td>
<td style="text-align: right;">75.05</td>
<td style="text-align: left;">B</td>
<td style="text-align: left;">NA</td>
<td style="text-align: right;">36</td>
<td style="text-align: left;">77.82</td>
<td style="text-align: right;">36</td>
<td style="text-align: left;">82.23</td>
<td style="text-align: right;">36</td>
<td style="text-align: left;">79.68</td>
<td style="text-align: right;">36</td>
<td style="text-align: left;">73.98</td>
<td style="text-align: right;">36</td>
<td style="text-align: left;">62.89</td>
<td style="text-align: right;">36</td>
<td style="text-align: left;">64.36</td>
<td style="text-align: right;">41.7</td>
<td style="text-align: right;">36</td>
</tr>
<tr class="odd">
<td style="text-align: left;">010780195</td>
<td style="text-align: left;">CLINIQUE DOCTEUR CONVERT</td>
<td style="text-align: left;">010780195</td>
<td style="text-align: left;">CLINIQUE CONVERT</td>
<td style="text-align: left;">Auvergne-Rhône-Alpes</td>
<td style="text-align: left;">1- Obligatoire</td>
<td style="text-align: left;">1- Oui</td>
<td style="text-align: left;">114</td>
<td style="text-align: right;">73.16</td>
<td style="text-align: left;">C</td>
<td style="text-align: left;">NA</td>
<td style="text-align: right;">114</td>
<td style="text-align: left;">72.19</td>
<td style="text-align: right;">114</td>
<td style="text-align: left;">82.37</td>
<td style="text-align: right;">114</td>
<td style="text-align: left;">80.45</td>
<td style="text-align: right;">114</td>
<td style="text-align: left;">73.36</td>
<td style="text-align: right;">114</td>
<td style="text-align: left;">53.83</td>
<td style="text-align: right;">114</td>
<td style="text-align: left;">62.8</td>
<td style="text-align: right;">54.0</td>
<td style="text-align: right;">113</td>
</tr>
<tr class="even">
<td style="text-align: left;">010780203</td>
<td style="text-align: left;">HÔPITAL PRIVE D–AMBÉRIEU</td>
<td style="text-align: left;">010780203</td>
<td style="text-align: left;">HOPITAL PRIVE D’AMBERIEU</td>
<td style="text-align: left;">Auvergne-Rhône-Alpes</td>
<td style="text-align: left;">1- Obligatoire</td>
<td style="text-align: left;">1- Oui</td>
<td style="text-align: left;">278</td>
<td style="text-align: right;">71.54</td>
<td style="text-align: left;">C</td>
<td style="text-align: left;">NA</td>
<td style="text-align: right;">278</td>
<td style="text-align: left;">72.92</td>
<td style="text-align: right;">278</td>
<td style="text-align: left;">80.37</td>
<td style="text-align: right;">278</td>
<td style="text-align: left;">79.78</td>
<td style="text-align: right;">278</td>
<td style="text-align: left;">68.94</td>
<td style="text-align: right;">278</td>
<td style="text-align: left;">65.46</td>
<td style="text-align: right;">278</td>
<td style="text-align: left;">54.51</td>
<td style="text-align: right;">43.3</td>
<td style="text-align: right;">277</td>
</tr>
</tbody>
</table>

# Analyses descriptives

**Nombre d’hopitaux participants:** 1150

**Variables catégorielles**

![](readme_files/figure-markdown_strict/unnamed-chunk-6-1.png)

**Scores et taux de recommandation**

![](readme_files/figure-markdown_strict/unnamed-chunk-7-1.png)

# Datamanagement

    iqss$region <- as.factor(iqss$region)
    iqss$participation <- as.factor(iqss$participation)
    iqss$classement <- as.factor(iqss$classement)
    iqss$nb_rep_score_all_rea_ajust <- as.numeric(iqss$nb_rep_score_all_rea_ajust)
    iqss$score_accueil_rea_ajust <- as.numeric(iqss$score_accueil_rea_ajust)
    iqss$score_PECinf_rea_ajust <- as.numeric(iqss$score_PECinf_rea_ajust)
    iqss$score_PECmed_rea_ajust <- as.numeric(iqss$score_PECmed_rea_ajust)
    iqss$score_chambre_rea_ajust <- as.numeric(iqss$score_chambre_rea_ajust)
    iqss$score_repas_rea_ajust <- as.numeric(iqss$score_repas_rea_ajust)
    iqss$score_sortie_rea_ajust <- as.numeric(iqss$score_sortie_rea_ajust)

# Licence du jeu de données brut

Licence Etalab 2.0