# Contexte

Le réseau public d’eau potable dessert aujourd’hui la quasi-totalité de la population française (urbaine ou rurale). L’eau du robinet provient des nappes souterraines, des ressources superficielles d’eau douce (fleuves, rivières, canaux, lacs, barrages) ou d’eau de mer. L'eau prélevée est traitée pour devenir potable et maintenir sa qualité dans les installations de stockage et les réseaux de distribution.

En France, l'eau du robinet fait l’objet d’un suivi sanitaire permanent pour assurer la sécurité sanitaire des consommateurs.

Le suivi sanitaire de l’eau comprend à la fois :
- la surveillance exercée par la personne responsable de la production et distribution de l’eau (PRPDE),
- le contrôle sanitaire, mis en œuvre par les Agences Régionales de Santé (ARS) en application des dispositions de la Directive européenne 98/83/CE relative à la qualité des eaux destinées à la consommation humaine et du Code de la santé publique.


# Présentation du jeu de données

Les données correspondent aux prélévements "complets" (validés par l'ARS) de l'ensemble des contrôles sanitaires réglementaire (ARS, contrôles complémentaires, recontrôles) de l'eau distribuée commune par commune depuis le **01/01/2016**. Elles correspondent ainsi aux résultats des analyses réalisées sur les unités de distribution ou les installations directement en amont, ainsi que sur les liens entre communes et unités de distribution.

Les résultats d’analyses sont considérés comme représentatifs de la qualité de l’eau distribuée aux consommateurs sur chaque réseau de distribution.

Plus particulièrement, les éléments de ce jeu de données correspondent à une compilation des bulletins d’analyses diffusés en ligne, commune par commune, sur le site internet du Ministère en charge de la santé : http://eaupotable.sante.gouv.fr/.

Les données proviennent de la **base nationale SISE-Eaux d’alimentation**. Elles ont été récupérées sur le site : data.gouv *(https://www.data.gouv.fr/fr/datasets/resultats-du-controle-sanitaire-de-leau-distribuee-commune-par-commune/)*

  
***Fréquence de mise à jour*** : mensuelle à m+1, par remplacement du fichier annuel en cours (NB: les données des départements d'outre-mer et leur historique seront progressivement ajoutées au jeu de données).

**Licence**: Licence Ouverte / Open Licence 

# Présentation des fichiers
## Présentation générale

Le jeu de données est composé de trois fichiers au format *".txt"* dont les variables sont séparées par des virgules.

1. **DIS_PLV_2021** : fichier des prélèvements (282735 observations x 18 variables )

2. **DIS_RESULT_2021** : fichier des résultats d'analyse (8156634 observations x 16 variables)

3. **DIS_COM_UDI_2021** : fichier détaillant le lien entre UDI et communes (49695 observations x 6 variabes)

--> Le lien entre les fichiers PLV et RESULT se fait via le code du prélèvement ('referenceprel')

--> Le lien entre les fichiers PLV et UDI se fait via les coddeds SISE-Eaux pour les unités de distribution ('cdreseau')

/!\ Les prélèvements peuvent avoir été effectivement réalisés en amont du réseau de distribution, sur une installation de traitement, de production et de transport de l’eau (TTP), voire sur une installation de ressource en eau ou captage (CAP). Cependant, ***les résultats d’analyses sont rapportés à l’unité de distribution concernée***.

Dans le cas où le prélèvement est réalisé sur une autre installation, certains champs supplémentaires du fichier PLV fournissent des informations sur cette installation :


| Champs supplémentaires | Code associé |
|:-------------------:|:------------:|
|Installation en amont | cdreseauamont|
|Nom de l'installation amont | nomreseauamont |
|% débit installation amont : indique si installation amont alimente exclusivement ou non cette UDI | pourcentdebit|

## Aperçu des trois fichiers

***DIS_PLV_2021***

| cddept | cdreseau | inseecommuneprinc | nomcommuneprinc | cdreseauamont | nomreseauamont | pourcentdebit | referenceprel | dateprel | heureprel | conclusionprel | ugelib | distrlib | moalib | plvconformitebacterio | plvconformitechimique | plvconformitereferencebact | plvconformitereferencechim | 
|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
| 1 | 1000003 | 1007 | AMBRONAY | NA | NA | NA | 100124281 | 12/01/2021 | 12h16 | Eau d'alimentation conforme aux exigences de qualitÃ© en vigueur pour l'ensemble des paramÃ¨tres mesurÃ©s. | SI REGION D'AMBERIEU-EN-BUGEY | SIE REGION D'AMBERIEU-EN-BUGEY | SIE REGION D'AMBERIEU-EN-BUGEY | 1 | 1 | 1 | 1 |
| 1 | 1000003 | 1007 | AMBRONAY | 1001304 | TTP (CLG) AMBRONAY | 100 | 100124675 | 09/02/2021 | 10h17 | Eau d'alimentation conforme aux exigences de qualitÃ© en vigueur pour l'ensemble des paramÃ¨tres mesurÃ©s. | SI REGION D'AMBERIEU-EN-BUGEY | SIE REGION D'AMBERIEU-EN-BUGEY	SIE REGION D'AMBERIEU-EN-BUGEY | 1 | 1 | 1 | 1 |
|1 | 1000003 | 1007 | AMBRONAY | NA | NA | NA | 100124958 | 09/03/2021 | 10h52 | Eau d'alimentation conforme aux exigences de qualitÃ© en vigueur pour l'ensemble des paramÃ¨tres mesurÃ©s. | SI REGION D'AMBERIEU-EN-BUGEY | SIE REGION D'AMBERIEU-EN-BUGEY | SIE REGION D'AMBERIEU-EN-BUGEY | 1 | 1 | 1 | 1 |
| 1 | 1000003 | 1007 | AMBRONAY | NA | NA | NA | 100125301 | 16/04/2021 | 12h04 | Eau d'alimentation conforme aux exigences de qualitÃ© en vigueur pour l'ensemble des paramÃ¨tres mesurÃ©s. | SI REGION D'AMBERIEU-EN-BUGEY | SIE REGION D'AMBERIEU-EN-BUGEY | SIE REGION D'AMBERIEU-EN-BUGEY | 1 | 1 | 1 | 1 |

***DIS_RESUTS_2021***
| cddept | referenceprel | cdparametresiseeaux | cdparametre | libmajparametre | libminparametre | qualitparam | insituana | rqana | cdunitereferencesiseeaux | cdunitereference | limitequal | refqual | valtraduite | casparam | referenceanl |
|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|--|
| 1 | 100123497 | ACPT | 1453 | ACÃ‰NAPHTÃˆNE | AcÃ©naphtÃ¨ne | 1 | 1 | < 0001 | Âµg/L | 133 | NA | NA | 0 | 83-32-9 | 100130636 |
| 1 | 100123497 | ACRYL | 1457 | ACRYLAMIDE | Acrylamide | 1 | 1 | < 0,10 | Âµg/L | 133 | <=0.1 Âµg/L | NA | 0 | 79-06-1 | 100130636 |
| 1 | 100123497 | ANTHRA | 1458 | ANTHRACÃˆNE | AnthracÃ¨ne | 1 | 1 | < 0.001 | Âµg/L | 133 | NA | NA | 0 | 120-12-7 | 100130636 |
| 1 | 100123497 | ASP | 6489 | ASPECT (QUALITATIF) | Aspect (qualitatif) | 0 | 1 | Aspect normal | SANS OBJET | X | NA | NA | 0 | NA | 100130636 |


***DIS_COM_UDI_2021***

| Inseecommune | nomcommune | quartier | cdreseau | nomreseau | debutalim |
|---|---|---|---|---|---|
| 01001 | ABERGEMENT-CLEMENCIAT (L')  | NA | 001000556 | BDS ST DIDIER/CHALARONNE | 2010-09-07|
| 01002 | ABERGEMENT-DE-VAREY (L') | NA | 001000369 | L'ABERGEMENT-DE-VAREY | 2010-09-07|
| 01004 | AMBERIEU-EN-BUGEY | Vareilles | 001000248 | AMBERIEU VAREILLES | 2010-09-07|
| 01004 | AMBERIEU-EN-BUGEY | St Germain_Brédevent | 001000249 | AMBERIEU SAINT GERMAIN DOUVRES | 2010-09-07|

# Présentation des variables

## Fichier des prélèvements

| Nom Colomne | Description | Type |
|:------------:|:----------:|:--:|
|cddept |Département gestionnaire (DD-ARS)| character |
|cdreseau |**Code SISE-Eaux de l'installation (UDI)**| character |
|inseecommuneprinc|Code Insee de la commune du point de surveillance où le prélèvement est réalisé|character|
|nomcommuneprinc|Nom de la commune du point de surveillance où le prélèvement est réalisé|character|
|cdreseauamont|Code SISE-Eaux de l'installation en amont| character |
|nomreseauamont|Nom de l'installation en amont|character |
|pourcentdebit|Pourcentage de débit de l'installation en amont| numeric |
|referenceprel|**Code SISE-Eaux du prélèvement**| character |
|dataprel|Date du prélèvement|date|
|heureprel|Heure du prélèvement| character |
|conclusionprel|Conclusion sanitaire du prélèvement| character |
|ugelib|Libellé de l'unité de gestion (service public de distribution)| character |
|distrlib|Nom de l'organisme exploitant (en charge de la distribution)| character |
|moalib|Nom du maître d'ouvrage| character |
|plvconformitebacterio|Conformité bactériologique du prélèvement aux limites de qualité (1 - Conforme ; 0 - Non-Conforme ; 2 - Sans objet) | character |
|plvconformitechimique|Conformité physico-chimique du prélèvement aux limites de qualité (1 - Conforme ; 0 - Non-Conforme ; 2 - Sans objet ; 3 - Conforme suite à dérogation)| character |
|plvconformitereferencebact|Conformité du prélèvement aux références de qualité (1 - Conforme ; 0 - Non-Conforme ; 2 - Sans objet)| character |
|plvconformitereferencechim|Conformité chimique du prélèvement aux références de qualité (1 - Conforme ; 0 - Non-Conforme ; 2 - Sans objet ; 3 - Conforme suite à dérogation)| character |


## Fichier des analyses
| Nom Colomne | Description | Type |
|:------------:|:----------:|:--:|
|cddept |Département gestionnaire (DD-ARS)| character |
|referenceprel|**Code SISE-Eaux du prélèvement**| character |
|rdparametresiseeaux|Code SISE-Eaux du paramètre| character |
|rdparametre|Code SANDRE du paramètre| character |
|libMAJparametre|Nom du paramètre en majuscule| character |
|libMINparametre|Nom du paramètre en minuscule| character |
|qualitparam|Paramètre de qualité (0 - qualtitatif ; 1 - quantitatif) | character |
|insituana|Type de résultat (Terrain, Laboratoire)|character|
|rqana|Résultat de l'analyse, si paramètre qualitatif présence libellé correspondant|character|
|cdunitereferencesiseeaux|Unité|character|
|cdunitereference|Code SANDRE Unité|character|
|limitequal|Limite(s) qualité|character|
|refqual|Référence(s) qualité|character|
|valTraduite|Valeur traduite du résultat Rqana au format numérique| numeric |
|casparam|Code CAS du paramètre|character|
|referenceAnl|Code de l'analyse faite en laboratoire|character|

## Fichier UDI

| Nom Colomne | Description | Type |
|:------------:|:----------:|:--:|
|inseecommune| Code Insee de la commune du point de surveillance | character |
|nomcommune|Nom de la commune alimentée par le réseau | character |
|nuartier|Quartier alimenté par le réseau| character |
|cdreseau|Code de l'installation **(UDI)**|character |
|nomreaseau| Nom de l'installation| character |
|debutalim| Date de début d'alimentation du quartier |character |

# Statistiques descriptives
## Variables quantitatives
![result_quant_pc_microbio](./statistiques_descriptives/result_quant_pc_microbio.png)

## Variables qualitatives

![quant](./statistiques_descriptives/quant.PNG)

# Modifications apportées
- Remplacement des cases vides par NA
- Elimination  des colonnes vides (libwebparametre)
- Réencodage de variables (plvconformitebacterio, plvconformitereferencebact ,plvconformitechimique, plvconformitereferencechim, qualitparam,insituana)
- Elimination des symboles "%" dans la variable pourcentdebit
- Réattribution de bon type à certaines variables

***Edition de trois fichiers .csv : DIS_PLV_cleaned.csv - DIS_RESULTS_cleaned.csv - DIS_UDI_cleaned.csv***
