# Introduction

Ce dataframe contient toutes les communes de France métropolitaine et
les communes qui leurs sont adjacentes. Les communes sont identifiées
par leur label et code INSEE (unique pour chaque commune).

# Intérêt

Cette matrice d’adjacence entre les communes sous forme de dataframe
peut être utile dans les études écologiques, notamment pour imputer les
données manquantes.

Aucun package n’est disponible sur R pour créer efficacement une matrice
d’adjacence sous forme de dataframe.

# Données brutes

-   Cartes des communes de France au format (.shp)
-   Carte des arrondissements au format (.shp)

(source: <https://geoservices.ign.fr/adminexpress#telechargement>)

# Variables

-   LIB\_COM: Nom de la commune
-   INSEE\_COM: Code INSEE de la commune
-   LIB\_NB: Nom de la commune adjacente
-   INSEE\_NB: Code INSEE de la commune adjacente

# Extrait pour la commune de Loos

<table>
<thead>
<tr class="header">
<th style="text-align: left;">LIB_COM</th>
<th style="text-align: left;">INSEE_COM</th>
<th style="text-align: left;">LIB_NB</th>
<th style="text-align: left;">INSEE_NB</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">Loos</td>
<td style="text-align: left;">59360</td>
<td style="text-align: left;">Wattignies</td>
<td style="text-align: left;">59648</td>
</tr>
<tr class="even">
<td style="text-align: left;">Loos</td>
<td style="text-align: left;">59360</td>
<td style="text-align: left;">Sequedin</td>
<td style="text-align: left;">59566</td>
</tr>
<tr class="odd">
<td style="text-align: left;">Loos</td>
<td style="text-align: left;">59360</td>
<td style="text-align: left;">Haubourdin</td>
<td style="text-align: left;">59286</td>
</tr>
<tr class="even">
<td style="text-align: left;">Loos</td>
<td style="text-align: left;">59360</td>
<td style="text-align: left;">Emmerin</td>
<td style="text-align: left;">59193</td>
</tr>
<tr class="odd">
<td style="text-align: left;">Loos</td>
<td style="text-align: left;">59360</td>
<td style="text-align: left;">Lille</td>
<td style="text-align: left;">59350</td>
</tr>
</tbody>
</table>

# Analyses descriptives

**Nombre de communes**

Le dataframe contient 34881 communes.

**Analyse du nombre de communes adjacentes à chaque commune**

<table>
<thead>
<tr class="header">
<th style="text-align: left;">INSEE_COM</th>
<th style="text-align: right;">nb_com_adj</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">01001</td>
<td style="text-align: right;">6</td>
</tr>
<tr class="even">
<td style="text-align: left;">01002</td>
<td style="text-align: right;">6</td>
</tr>
<tr class="odd">
<td style="text-align: left;">01004</td>
<td style="text-align: right;">7</td>
</tr>
<tr class="even">
<td style="text-align: left;">01005</td>
<td style="text-align: right;">7</td>
</tr>
<tr class="odd">
<td style="text-align: left;">01006</td>
<td style="text-align: right;">6</td>
</tr>
<tr class="even">
<td style="text-align: left;">01007</td>
<td style="text-align: right;">9</td>
</tr>
</tbody>
</table>


    ##    Min. 1st Qu.  Median    Mean 3rd Qu.    Max. 
    ##   1.000   5.000   6.000   5.959   7.000  26.000

# Exemple d’application: Revenu médian par commune en 2017

Ce jeu de donnée est soumis issu de l’INSEE est soumis au secret
statistique.Les données ne sont donc pas présentes en cas de faible
nombre d’habitants dans la commune. La colonne Q217 correspond au revenu
médian par unité de consommation. Le code géographique est unique pour
chaque commune. La éographie en vigueur est celle de 2018. (source:
<https://www.insee.fr/fr/statistiques/4291712>).

Les données manquantes sont ci-dessous imputées par la moyenne pondérée
par la population des communes adjacentes. Dans cet exemple, les données
de la population sont issues de l’INSEE.

**a. Import des librairies et des données**

    library(dplyr)
    library(readxl)

    path <- "C:/Users/mbail/Desktop/cours_M2_dss/Sante/donnees_sante/Data/matrice_adjacence_commune" # à modifier

    revenu_median17_geo18 <- read_excel(file.path(path,"FILO2017_DEC_COM_raw.xlsx"), sheet = 2, skip = 5)
    commune_adj <- read.csv2(file.path(path,"df_commune_adj_cleaned.csv"))
    pop_2017 <- read.csv2(file.path(path,"base-cc-evol-struct-pop-2017_raw.csv"))
    table_passage <- read_excel(file.path(path,"table_passage_annuelle_2021_raw.xlsx"), sheet = 1, skip = 5)

    pop_2017$P17_POP <- as.numeric(pop_2017$P17_POP)

**b. La matrice d’adjacence des communes ayant une géographie en vigueur
datant de 2020, la géographie des revenus médians est converti en celle
de 2020 en utilisant une table de passage.**

    revenu_median17_geo20 <-  revenu_median17_geo18%>%
      dplyr::left_join(., table_passage, by = c("CODGEO" = "CODGEO_2018"), keep=TRUE)%>%
      dplyr::filter(substr(CODGEO_2020, 1, 2) != "97") %>%
      dplyr::select("CODGEO_2020", "CODGEO", "Q217") %>%
      unique(.) %>%
      dplyr::left_join(., pop_2017, by = c("CODGEO_2020" = "CODGEO")) %>%
      dplyr::group_by(CODGEO_2020) %>%
      dplyr::summarise(revenu_median = weighted.mean(Q217, P17_POP))

**c. Identification du nombre de communes possédant une valeur
manquante**

    revenu_median17_geo20_cg_complet <-  revenu_median17_geo20%>%
      dplyr::full_join(., df_commune_adj, by = c("CODGEO_2020" = "INSEE_COM"), keep = TRUE) %>%
      dplyr::select(INSEE_COM, "revenu_median") %>%
      dplyr::filter(INSEE_COM != is.na(INSEE_COM)) %>%
      unique(.)

    nrow <- nrow(revenu_median17_geo20_cg_complet)
    na <- sapply(revenu_median17_geo20_cg_complet, function(y) sum(is.na(y)))

Le dataframe comprend 3542 données manquantes (10.2%).

**d. Imputation des revenus médians par la moyenne pondérée par la
population des communes adjacentes.**

    revenu_median17_geo20_imputation <- df_commune_adj %>%
      dplyr::full_join(., revenu_median17_geo20_cg_complet, 
                       by = "INSEE_COM") %>%
      dplyr::full_join(., revenu_median17_geo20_cg_complet, 
                       by = c("INSEE_NB" = "INSEE_COM")) %>%
      dplyr::filter(is.na(revenu_median.x))%>%
      dplyr::rename(revenu_median_com = revenu_median.x, 
                    revenu_median_nb = revenu_median.y, 
                    INSEE_NB = INSEE_NB) %>%
      dplyr::left_join(., pop_2017, by = c("INSEE_NB" = "CODGEO")) %>%
      dplyr::select(INSEE_COM,revenu_median_com, INSEE_NB,  revenu_median_nb, P17_POP) %>%
      dplyr::group_by(INSEE_COM) %>%
      dplyr::summarise(revenu_median = weighted.mean(revenu_median_nb, P17_POP, na.rm = TRUE))

**e. Jointure entre les dataframes contenant les revenus médians connus
(en vigueur de la géographie 2020) et les revenus médians imputés.**

    revenu_median17_geo20_complet <- revenu_median17_geo20_cg_complet %>%
      dplyr::filter(revenu_median != is.na(revenu_median)) %>%
      dplyr::bind_rows(., revenu_median17_geo20_imputation) %>%
      dplyr::filter(INSEE_COM != is.na(INSEE_COM))

    na <- sapply(revenu_median17_geo20_complet, function(y) sum(is.na(y)))

Il n’y a plus que 69 données manquantes (0.2%) correspondant aux
communes dont aucune commune adjacente ne comprenait de valeur de revenu
médian.

# Licence des données brutes

Licence Etalab 2.0

# Datamanagement

**Etape 1:** Superposition des cartes des communes et des
arrondissements à l’aide de QGIS (logiciel gratuit) en suivant les
étapes ci-dessous:

-   Ouvrir les 2 cartes
-   Cliquer sur vecteur,outils de gestion de données, fusionner les 2
    couches de vecteurs
-   Choisir les 2 couches puis cliquer sur Executer
-   Exporter la carte créée au format geojson par exemple

**Etape 2:** Utilisation du script disponible sur git pour extraire les
communes adjacentes à chaque commune
