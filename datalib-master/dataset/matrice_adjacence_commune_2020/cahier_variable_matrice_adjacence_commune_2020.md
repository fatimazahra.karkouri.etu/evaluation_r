<table>
<thead>
<tr class="header">
<th style="text-align: left;">nom_long</th>
<th style="text-align: left;">nom_court</th>
<th style="text-align: left;">type</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">Libellé de la commune</td>
<td style="text-align: left;">LIB_COM</td>
<td style="text-align: left;">chaîne de caractères</td>
</tr>
<tr class="even">
<td style="text-align: left;">Code INSEE de la commune</td>
<td style="text-align: left;">INSEE_COM</td>
<td style="text-align: left;">chaîne de caractères</td>
</tr>
<tr class="odd">
<td style="text-align: left;">Libellé de la commune voisine</td>
<td style="text-align: left;">LIB_NB</td>
<td style="text-align: left;">chaîne de caractères</td>
</tr>
<tr class="even">
<td style="text-align: left;">Code INSEE de la commune voisine</td>
<td style="text-align: left;">INSEE_NB</td>
<td style="text-align: left;">chaîne de caractères</td>
</tr>
</tbody>
</table>
