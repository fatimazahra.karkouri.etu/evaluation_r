# Contexte

Les infections nosocomiales sont des infections contractées au cours d'un séjour dans un établissement de santé (hôpital, clinique...). Afin d'avoir un aperçu de la lutte des établissements de santé contre ce types d'infections, six indicateurs ont été mis en place pour visualiser l'organisation, les moyens et actions mises en œuvre ainsi que la surveillance et  la maitrise d'infections graves ou évitables dans ces établissements. Des classes de A à E ont été définies pour chaque indicateur à partir d'analyses statistiques. Dans une optique de visualisation de l'évolution de ces classes au cours des années, les bornes inférieures et supérieures ne changent pas au cours des années. 
 
## Description des classes : 
- **Classes A et B** : établissements les plus en avance dans le domaine
- **Classe C** : classe intermédiaire
- **Classe D** : établissements en retard dans le domaine 
- **Classe E** : établissements en retard dans le domaine et n'envoyant pas le bilan standarisé
- **Classe W** : établisements dont la classe n'a pas été déterminée pour raisons inconnues
 
## Informations relatives aux indicateurs :

- **ICALIN** : Indice Composite des Activités de Lutte contre les Infections Nosocomiales 
*L’ICALIN objective l’organisation de la lutte contre les IN dans l’établissement, les moyens qu’il a mobilisés et les actions qu’il a mises en oeuvre.
Ce score sur 100 points reflète le niveau d’engagement de l’établissement de santé et de ses personnels.*

- **ICSHA** : Indicateur de consommation es solutions hydro-alcooliques 
*L’indicateur ICSHA est un marqueur indirect de la mise en oeuvre effective de l’hygiène des mains.Il permet d’apprécier la mise en oeuvre par les professionnels soignants des recommandations de pratiques de prévention.
L’ICSHA, exprimé en pourcentage, est le rapport entre le volume de produits hydro-alcooliques consommé réellement par l’établissement et son objectif personnalisé de consommation vers lequel les établissements doivent tendre. Celui-ci est déterminé à partir d’un référentiel national prenant en compte les types d’activités*

- **SURVISO** : Surveillance des infections du site opérateur 
*L’indicateur SURVISO rend visible l’engagement de l’établissement dans une démarche d’évaluation et d’amélioration des pratiques et de maîtrise du risque infectieux en chirurgie.
Il s’intéresse à la mise en place par l’établissement d’une surveillance épidémiologique des patients après leur opération chirurgicale. Il ne permet pas de mesurer la fréquence des infections du site opératoire.
SURVISO indique la proportion des services de chirurgie de l’établissement qui se sont impliqués au cours de l’année dans une enquête épidémiologique. Cet indicateur ne concerne que les structures faisant de la chirurgie.*

- **ICATB**: Indicateur du bon usage des antibiotiques
*L’ICATB objective l’organisation mise en place dans l’établissement pour promouvoir le bon usage des antibiotiques, les moyens qu’il a mobilisés et les actions qu’il a mises en œuvre. Ce bon usage associe des objectifs de bénéfice individuel pour le patient (meilleur traitement possible) et de bénéfice collectif (limitation de l’émergence de bactéries résistantes).
Ce score sur 20 points reflète le niveau d’engagement de l’établissement de santé, dans une stratégie d'optimisation de l'efficacité des traitements antibiotiques.*

- **SARM** : Indice de taux de Staphylococcus aureus résistant à la méticilline
*Cet indice dépend d’une part, du nombre de patients venant d’un autre hôpital (SARM dits importés) et d’autre part de la prévention de la diffusion des SARM d’un patient à l’autre (SARM dits acquis dans l’établissement) et de la politique de maîtrise de la prescription des antibiotiques.
Il permet de refléter l’écologie microbienne de l’établissement et sa capacité à la maitriser par des mesures de prévention de la transmission de patient à patient et par une politique de maîtrise des prescriptions d’antibiotiques.
Son calcul repose sur le nombre de patients hospitalisés chez lesquels au moins une souche de SARM a été isolée au sein d’un prélèvement à visée diagnostique (nombre de SARM déclarés), quelque soit le lieu d’acquisition (souches importées et acquises) rapportés à 1000 journées d’hospitalisation.*

# Présentation du jeu de données

Les données correspondent aux classes et différents indicateurs (ICALIN, ICSHA, SURVISO, ICATB, SARM) de la quasi-totalité des établissements de santé français (exhaustivité de l'enquête en 2009 = 99% ; 3 établissements sur 2780 n'ont pas répondus), de 2007 à 2009. 

Le jeu de données possède 22 variables x 8.225 observations. 

**Aperçu des données**

|annee|finess|departement|type_etablissement|score_agrege|classe_performance_globale|note_organisation_ICALIN|note_moyens_ICALIN|note_actions_ICALIN|note_totale_ICALIN|classe_perf_ICALIN|note_ICSHA|class_perf_ICSHA|organisation_ICATB|moyens_ICATB|actions_ICATB|note_total_ICATB|class_perf_ICATB|enquete_SURVISO|nb_service_chi_SURVISO|part_service_chi_SURVISO|indice_SARM|
|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|
2007|10000024|1|02-CH SUP 300|80.87|B|33|30|33.5|96.5|A|65.9|C|4|5|7.5|16.5|B|1|4|0.666666666666667|0.56|
2007|10000065|1|02-CH INF 300|62.41|C|26|31|30|87|B|36.4|C|4|1|5.5|10.5|C|2|NA|NA|0.26|
2007|10000081|1|04-HOPITAL LOCAL|53.02|C|29|24|22|75|A|58.4|C|0|0|0.25|0.25|D|2|NA|NA|NA|
2007|10000099|1|04-HOPITAL LOCAL|64.38|B|24.5|28|33.5|86|A|42.8|C|2|3|5.75|10.75|B|2|NA|NA|NA|


Les données proviennent du Ministère des Solidarités et de la Santé et ont été récupérées sur le site **data.gouv** : https://www.data.gouv.fr/fr/datasets/les-indicateurs-relatifs-aux-infections-nosocomiales-dans-les-etablissements-de-sante-30378376/

**License** : Licence Ouverte / Open Licence 

# Présentation des variables

| Nom de la variable | Description | Type |
|:------------------:|:-----------:|:----:|
| annee | Année | character |
| finess |numéro FINESS de l'établissement |character|
| departement | Département| character |
| type_etablissement |Type d'établissement (hôpital, clinique...) et nom|character |
| score_agrege |score de l'établissement permettant de définir sa classe : **0.4*score_ICALIN + 0.3*score_ICSHA + 0.2*score_ICATB + 0.1*SURVISO**| numeric |
|classe_performance_globale |Classe attribée à l'établissement|character|
|note_organisation_ICALIN|Note reliée à l'organisation : politique et implication, équipe opérationnelle hygiène, information des usagers, signalement **(sur 20 pts)**| numeric |
|note_moyens_ICALIN | Note reliée aux moyens : humains, matériels, formation **(sur 30 pts)**|numeric |
|note_actions_ICALIN| Note reliée aux actions : prévention et évaluation **(sur 50 pts)**|numeric |
|note_totale_ICALIN|Note finale (pondérée pour un totale de 100)|numeric|
|classe_perf_ICALIN|Classe de l'établissement pour critère ICALIN| character|
|note_ICSHA |volume de produits hydro-alcooliques consommé réellement par l’établissement / objectif personnalisé de consommation attendu (%) | numeric |
|class_perf_ICSHA|Classe de l'établissement pour critère ICSHA| character |
|organisation_ICATB|Note reliée à l'organisation : accès au conseil, collaboration, politique et prgrm d'action **(sur 16 pts)**|numeric |
|moyens_ICATB|Note reliée aux moyens : informatiques, humains, formations **(sur 46 pts)**| numeric |
|actions_ICATB|Note reliée aux actions : prévention, surveillance, évaluation **(sur 46 pts)**|numeric |
|note_total_ICATB|Note finale (pondérée pour un totale de 100)|numeric |
|class_perf_ICATB| Classe de l'établissement pour critère ICATB|character|
|enquete_SURVISO| Réalisation ou non d'une enquête SURVISO (0 - Non ; 1 - Oui ; 2 - Non Concerné)| character |
|nb_service_chi_SURVISO|Nombre de service de chirurgie évalués selon SURVISO| numeric |
|part_service_chi_SURVISO|Proportion des services de chirurgie participant aux enquêtes SURVISO (%)| numeric |
|indice_SARM|Indice de taux de Staphylococcus aureus résistant à la méticilline| numeric|

# Statistiques descriptives
![total.PNG](./statistique_descriptive/total.PNG)

# Modifications apportées 

- Fichier *.xls* à 3 feuilles dont 2 vides, réduit à 1 exporté en *.csv*
- Renommage des variables (élimination des redondances à la main, renommage via R)
- Réencodage des données relatives aux enquêtes d'incidents sur infections du site opératoire (SURVISO) : non - 0 ; oui - 1 ; non-concerné - 2
- Réencodage des données relatives aux indices SARM et adaptation du type. 

**Génération d'un fichier propre nommé *"data_maladies_nosocomiales_cleaned.csv"*.**
