# CORINE Land Cover

## Description du fichier

<table style="width:100%;">
<colgroup>
<col style="width: 7%" />
<col style="width: 75%" />
<col style="width: 16%" />
</colgroup>
<tbody>
<tr class="odd">
<td style="text-align: left;">Description</td>
<td style="text-align: center;">CORINE Land Cover (CLC) est un inventaire biophysique de l’occupation des sols et de son évolution selon une nomenclature en 44 postes. Le fichier présent dans ce gitlab comporte uniquement 5 postes mais les fichiers contenant les autres postes sont disponibles sur le lien ci-contre</td>
<td><a href="https://www.statistiques.developpement-durable.gouv.fr/corine-land-cover-0" class="uri">https://www.statistiques.developpement-durable.gouv.fr/corine-land-cover-0</a></td>
</tr>
<tr class="even">
<td style="text-align: left;">Source / Téléchargement</td>
<td style="text-align: center;">Disponible avec plus ou moins de postes (44 au maximum)</td>
<td><a href="https://www.statistiques.developpement-durable.gouv.fr/corine-land-cover-0" class="uri">https://www.statistiques.developpement-durable.gouv.fr/corine-land-cover-0</a></td>
</tr>
<tr class="odd">
<td style="text-align: left;">Producteur</td>
<td style="text-align: center;">Programme européen de surveillance des terres de Copernicus (Agence européenne pour l’environnement)</td>
<td><a href="https://www.copernicus.eu/fr" class="uri">https://www.copernicus.eu/fr</a></td>
</tr>
<tr class="even">
<td style="text-align: left;">Licence</td>
<td style="text-align: center;">Open data</td>
<td></td>
</tr>
<tr class="odd">
<td style="text-align: left;">Format</td>
<td style="text-align: center;">Fichier CSV (csv2)</td>
<td></td>
</tr>
<tr class="even">
<td style="text-align: left;">Encodage</td>
<td style="text-align: center;">UTF-8</td>
<td></td>
</tr>
<tr class="odd">
<td style="text-align: left;">Date de Création</td>
<td style="text-align: center;">Base de données a été initiée en 1985</td>
<td></td>
</tr>
<tr class="even">
<td style="text-align: left;">Date de téléchargement du fichier</td>
<td style="text-align: center;">07/10/2021</td>
<td></td>
</tr>
<tr class="odd">
<td style="text-align: left;">Fréquence de mise à jour</td>
<td style="text-align: center;">Selon millésimes (disponible pour les années suivantes : 1990, 2000, 2006, 2012 et 2018)</td>
<td></td>
</tr>
<tr class="even">
<td style="text-align: left;">Couverture territoriale</td>
<td style="text-align: center;">Selon fichier (base de données produite sur 39 États européens). Le fichier de ce gitlab concerne la France</td>
<td></td>
</tr>
<tr class="odd">
<td style="text-align: left;">Granularité</td>
<td style="text-align: center;">Commune</td>
<td></td>
</tr>
<tr class="even">
<td style="text-align: left;">Raison / Méthode de création</td>
<td style="text-align: center;">Cet inventaire est produit par interprétation visuelle d’images satellite. L’échelle de production est le 1/100 000. CLC permet de cartographier des unités homogènes d’occupation des sols d’une surface minimale de 25 ha. Projet dans le cadre du programme européen de surveillance des terres de Copernicus, piloté par l’Agence européenne pour l’environnement.</td>
<td></td>
</tr>
</tbody>
</table>

## Intérêt et possibilités

-   Cartographies et statistiques par communes / départements / régions
    et pour différents pays

-   Caractérisation des territoires : urbain, rural, forêt…

-   Calcul de densité de population par type de d’occupation des sols

-   Prévalence et localisation de certaines maladie en fonction des
    types d’occupation des sols

-   Très utile en épidémiologie environnementale et pour identifier des
    sources potentielles de pollution

-   …

## Améliorations

Bien que les données brutes ouvertes en csv2 soient très propres, le
fichier présente deux défauts mineurs :

-   Nom des colonnes difficilement lisible

-   3 Premières lignes inutiles

-   Mauvais typage des variables

Le script R suivant propose une amélioration du jeu de données initial :

    library(janitor) #permet d'améliorer les noms de colonnes
    library(dplyr)
    df <- read.csv2("clc_etat_com_n1.csv", encoding = "UTF-8")
    df <- df[-c(1, 2, 3),]
    df <- df %>%
      clean_names() 

    # Transformation du typage de la plage de variables suivantes en numeric
    df[,4:ncol(df)] <- sapply(df[,4:ncol(df)],as.numeric)

    # Transformation du typage des variables suivantes en facteur
    df$code_insee_de_la_commune <- as.factor(df$code_insee_de_la_commune)
    df$millesime <- as.factor(as.numeric(df$millesime))
    df$base_de_donnees_label <- as.factor(df$base_de_donnees_label)

## Description des données

Les parties suivantes concernent le jeu de données dont les défauts ont
été supprimés à l’aide de l’action script R disponible dans la partie
“Améliorations”.

### Variables

<table>
<colgroup>
<col style="width: 31%" />
<col style="width: 34%" />
<col style="width: 10%" />
<col style="width: 23%" />
</colgroup>
<thead>
<tr class="header">
<th>Champ</th>
<th>Libellé</th>
<th>Type</th>
<th>Exemple</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td>code_insee_de_la_commune</td>
<td>Code insee de la commune</td>
<td>Factor (character)</td>
<td>“01001” “01002” “01004” “01005” …</td>
</tr>
<tr class="even">
<td>millesime</td>
<td>Millésime concerné</td>
<td>Factor (numeric)</td>
<td>“1990” “1990” “1990” “1990” …</td>
</tr>
<tr class="odd">
<td>base_de_donnees_label</td>
<td>Label base de donnée</td>
<td>Factor (character)</td>
<td>“CLC 1990” “CLC 1990” “CLC 1990” “CLC 1990”</td>
</tr>
<tr class="even">
<td>superficie_du_poste_1_territoires_artificialises_en_ha</td>
<td>Superficie en ha du poste 1 : “Territoires artificialisés”)</td>
<td>numeric</td>
<td>62.3 0 700.5 112.5 33.1 …</td>
</tr>
<tr class="odd">
<td>superficie_du_poste_2_territoires_agricoles_en_ha</td>
<td>Superficie en ha du poste 2 : “Territoires agricoles”)</td>
<td>numeric</td>
<td>1128 322 542 1252 125 …</td>
</tr>
<tr class="even">
<td>superficie_du_poste_3_forets_et_milieux_semi_naturels_en_ha</td>
<td>Superficie en ha du poste 3 : “Forets et milieux semi naturels”)</td>
<td>numeric</td>
<td>375.6 591.2 1206.8 55.3 444.9 …</td>
</tr>
<tr class="odd">
<td>superficie_du_poste_4_zones_humides_en_ha</td>
<td>Superficie en ha du poste 4 : “Zones humides”)</td>
<td>numeric</td>
<td>0 0 0 0 0 …</td>
</tr>
<tr class="even">
<td>superficie_du_poste_5_surfaces_en_eau_en_ha</td>
<td>Superficie en ha du poste 5 : “Surfaces en eau”)</td>
<td>numeric</td>
<td>0 0 0 185 0 …</td>
</tr>
</tbody>
</table>

### Dimensions / analyses descriptives (au 07/10/2021)

    ##  code_insee_de_la_commune millesime         base_de_donnees_label
    ##  01001  :     8           1990:35756   CLC 1990        :35756    
    ##  01002  :     8           2000:71512   CLC 2000        :35756    
    ##  01004  :     8           2006:71512   CLC 2000 révisée:35756    
    ##  01005  :     8           2012:71512   CLC 2006        :35756    
    ##  01006  :     8           2018:35756   CLC 2006 révisée:35756    
    ##  01007  :     8                        CLC 2012        :35756    
    ##  (Other):286000                        (Other)         :71512    
    ##  superficie_du_poste_1_territoires_artificialises_en_ha
    ##  Min.   :    0.00                                      
    ##  1st Qu.:    0.00                                      
    ##  Median :   30.44                                      
    ##  Mean   :   81.31                                      
    ##  3rd Qu.:   75.14                                      
    ##  Max.   :13547.16                                      
    ##                                                        
    ##  superficie_du_poste_2_territoires_agricoles_en_ha
    ##  Min.   :    0.0                                  
    ##  1st Qu.:  361.6                                  
    ##  Median :  650.6                                  
    ##  Mean   :  914.7                                  
    ##  3rd Qu.: 1145.5                                  
    ##  Max.   :40256.4                                  
    ##                                                   
    ##  superficie_du_poste_3_forets_et_milieux_semi_naturels_en_ha
    ##  Min.   :    0.00                                           
    ##  1st Qu.:   46.93                                           
    ##  Median :  191.35                                           
    ##  Mean   :  522.25                                           
    ##  3rd Qu.:  552.27                                           
    ##  Max.   :24411.32                                           
    ##                                                             
    ##  superficie_du_poste_4_zones_humides_en_ha
    ##  Min.   :    0.000                        
    ##  1st Qu.:    0.000                        
    ##  Median :    0.000                        
    ##  Mean   :    4.901                        
    ##  3rd Qu.:    0.000                        
    ##  Max.   :17655.811                        
    ##                                           
    ##  superficie_du_poste_5_surfaces_en_eau_en_ha
    ##  Min.   :    0.00                           
    ##  1st Qu.:    0.00                           
    ##  Median :    0.00                           
    ##  Mean   :   11.88                           
    ##  3rd Qu.:    0.00                           
    ##  Max.   :18080.82                           
    ## 

-   Nombre de millésimes concernés : 5 (1990, 2000, 2006, 2012, 2018)

-   Nombre de postes du fichier : 5

-   Nombre de communes concernées : 35756

<!-- -->

    #code ayant permis de générer le graphique ci-dessous
    ggplot(df %>%
             select(-code_insee_de_la_commune, -base_de_donnees_label) %>%
             group_by(millesime) %>%
             summarise(across(everything(), list(mean))) %>%
             pivot_longer(names_to = "Poste", values_to = "Superficie_en_ha", 
                          superficie_du_poste_1_territoires_artificialises_en_ha_1:superficie_du_poste_5_surfaces_en_eau_en_ha_1) 
           , aes(x=millesime, y=Superficie_en_ha, fill=Poste)) + 
      geom_bar(stat="identity", width=0.7, position=position_dodge(width=0.8)) +
      scale_fill_manual(labels = c("Territoires Artificialises", "Territoires Agricoles", "Forets et milieux semi-naturels","Zones humides","Surfaces en eau"), values = c("blue","green","red","pink","lightblue")) +
      ggtitle("Superficie des différents millieux (en ha)", "Selon le millésime") +
      xlab("Superficie (en ha)") +
      ylab("Millésime")

![](CORINE/plot_CORINE.png)

Carte proposant l’ensemble des 44 postes (source :
<https://www.statistiques.developpement-durable.gouv.fr/sites/default/files/2018-12/carte-corine-land-cover-2012.pdf>)

![](CORINE/Carte_CORINE.png)

## Extrait de la base de données

<table>
<colgroup>
<col style="width: 0%" />
<col style="width: 8%" />
<col style="width: 3%" />
<col style="width: 7%" />
<col style="width: 17%" />
<col style="width: 16%" />
<col style="width: 19%" />
<col style="width: 13%" />
<col style="width: 14%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;"></th>
<th style="text-align: left;">code_insee_de_la_commune</th>
<th style="text-align: left;">millesime</th>
<th style="text-align: left;">base_de_donnees_label</th>
<th style="text-align: right;">superficie_du_poste_1_territoires_artificialises_en_ha</th>
<th style="text-align: right;">superficie_du_poste_2_territoires_agricoles_en_ha</th>
<th style="text-align: right;">superficie_du_poste_3_forets_et_milieux_semi_naturels_en_ha</th>
<th style="text-align: right;">superficie_du_poste_4_zones_humides_en_ha</th>
<th style="text-align: right;">superficie_du_poste_5_surfaces_en_eau_en_ha</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">4</td>
<td style="text-align: left;">01001</td>
<td style="text-align: left;">1990</td>
<td style="text-align: left;">CLC 1990</td>
<td style="text-align: right;">62.28998</td>
<td style="text-align: right;">1127.5723</td>
<td style="text-align: right;">375.61225</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0.00000</td>
</tr>
<tr class="even">
<td style="text-align: left;">5</td>
<td style="text-align: left;">01002</td>
<td style="text-align: left;">1990</td>
<td style="text-align: left;">CLC 1990</td>
<td style="text-align: right;">0.00000</td>
<td style="text-align: right;">321.8208</td>
<td style="text-align: right;">591.17233</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0.00000</td>
</tr>
<tr class="odd">
<td style="text-align: left;">6</td>
<td style="text-align: left;">01004</td>
<td style="text-align: left;">1990</td>
<td style="text-align: left;">CLC 1990</td>
<td style="text-align: right;">700.50729</td>
<td style="text-align: right;">541.5990</td>
<td style="text-align: right;">1206.75362</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0.00000</td>
</tr>
<tr class="even">
<td style="text-align: left;">7</td>
<td style="text-align: left;">01005</td>
<td style="text-align: left;">1990</td>
<td style="text-align: left;">CLC 1990</td>
<td style="text-align: right;">112.49591</td>
<td style="text-align: right;">1251.8495</td>
<td style="text-align: right;">55.27385</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">185.43720</td>
</tr>
<tr class="odd">
<td style="text-align: left;">8</td>
<td style="text-align: left;">01006</td>
<td style="text-align: left;">1990</td>
<td style="text-align: left;">CLC 1990</td>
<td style="text-align: right;">33.09930</td>
<td style="text-align: right;">124.6596</td>
<td style="text-align: right;">444.88877</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0.00000</td>
</tr>
<tr class="even">
<td style="text-align: left;">9</td>
<td style="text-align: left;">01007</td>
<td style="text-align: left;">1990</td>
<td style="text-align: left;">CLC 1990</td>
<td style="text-align: right;">357.49489</td>
<td style="text-align: right;">2073.2812</td>
<td style="text-align: right;">863.65569</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">64.77748</td>
</tr>
</tbody>
</table>
