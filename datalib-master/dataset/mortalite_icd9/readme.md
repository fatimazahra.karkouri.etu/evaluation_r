### Données de mortalité mondiale, sur la base de l'ICD-9 (1983-2013)

Description
-----------

La classification internationale des maladies, neuvième révision (ICD-9)
est la classification internationale des maladies de l’Organisation
mondiale de la santé. C’est le système officiel d’attribution de codes
aux diagnostics et aux procédures associés à l’utilisation des hôpitaux.
L’ICD-9 (CIM-9 en Français) a été utilisée pour coder et classer les
données de mortalité, de 1983 à 2013.

| Fichier | Description | 
| -------- | -------- | 
| Morticd9.csv | Jeu de données brut de l'OMS   | 
| code_cause.csv | Jeu de données associant les codes ICD-9 à leur description   | 
| country_codes.csv | Jeu de données associant les codes pays à leur dénomination   | 
| desc_who_icd9.odt | Documentation sur les variables     | 
| mortalite_icd9.Rmd |  Markdown pour la création d'une base de donnée agrégée + .md |
| dash-who-fr-12.png |  Capture : Dashboard WHO mortalité en France (2012) |

Preview
-------

<table style="width:100%;">
<colgroup>
<col style="width: 1%" />
<col style="width: 2%" />
<col style="width: 3%" />
<col style="width: 1%" />
<col style="width: 1%" />
<col style="width: 1%" />
<col style="width: 1%" />
<col style="width: 1%" />
<col style="width: 13%" />
<col style="width: 1%" />
<col style="width: 1%" />
<col style="width: 2%" />
<col style="width: 2%" />
<col style="width: 2%" />
<col style="width: 2%" />
<col style="width: 2%" />
<col style="width: 2%" />
<col style="width: 2%" />
<col style="width: 2%" />
<col style="width: 2%" />
<col style="width: 2%" />
<col style="width: 2%" />
<col style="width: 2%" />
<col style="width: 2%" />
<col style="width: 2%" />
<col style="width: 2%" />
<col style="width: 2%" />
<col style="width: 2%" />
<col style="width: 2%" />
<col style="width: 2%" />
<col style="width: 2%" />
<col style="width: 2%" />
<col style="width: 2%" />
<col style="width: 2%" />
<col style="width: 2%" />
<col style="width: 2%" />
<col style="width: 2%" />
<col style="width: 2%" />
<col style="width: 2%" />
<col style="width: 2%" />
<col style="width: 2%" />
<col style="width: 2%" />
</colgroup>
<thead>
<tr class="header">
<th style="text-align: left;"></th>
<th style="text-align: right;">country</th>
<th style="text-align: left;">name</th>
<th style="text-align: right;">admin1</th>
<th style="text-align: left;">subdiv</th>
<th style="text-align: right;">year</th>
<th style="text-align: left;">list</th>
<th style="text-align: left;">cause</th>
<th style="text-align: left;">description</th>
<th style="text-align: right;">sex</th>
<th style="text-align: right;">frmat</th>
<th style="text-align: right;">im_frmat</th>
<th style="text-align: right;">deaths1</th>
<th style="text-align: right;">deaths2</th>
<th style="text-align: right;">deaths3</th>
<th style="text-align: right;">deaths4</th>
<th style="text-align: right;">deaths5</th>
<th style="text-align: right;">deaths6</th>
<th style="text-align: right;">deaths7</th>
<th style="text-align: right;">deaths8</th>
<th style="text-align: right;">deaths9</th>
<th style="text-align: right;">deaths10</th>
<th style="text-align: right;">deaths11</th>
<th style="text-align: right;">deaths12</th>
<th style="text-align: right;">deaths13</th>
<th style="text-align: right;">deaths14</th>
<th style="text-align: right;">deaths15</th>
<th style="text-align: right;">deaths16</th>
<th style="text-align: right;">deaths17</th>
<th style="text-align: right;">deaths18</th>
<th style="text-align: right;">deaths19</th>
<th style="text-align: right;">deaths20</th>
<th style="text-align: right;">deaths21</th>
<th style="text-align: right;">deaths22</th>
<th style="text-align: right;">deaths23</th>
<th style="text-align: right;">deaths24</th>
<th style="text-align: right;">deaths25</th>
<th style="text-align: right;">deaths26</th>
<th style="text-align: right;">im_deaths1</th>
<th style="text-align: right;">im_deaths2</th>
<th style="text-align: right;">im_deaths3</th>
<th style="text-align: right;">im_deaths4</th>
</tr>
</thead>
<tbody>
<tr class="odd">
<td style="text-align: left;">2982</td>
<td style="text-align: right;">4005</td>
<td style="text-align: left;">Albania</td>
<td style="text-align: right;">NA</td>
<td style="text-align: left;"></td>
<td style="text-align: right;">1993</td>
<td style="text-align: left;">09B</td>
<td style="text-align: left;">B313</td>
<td style="text-align: left;">Deflected nasal septum and nasal polyps</td>
<td style="text-align: right;">2</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">10</td>
<td style="text-align: right;">2</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">2</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">2</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">NA</td>
<td style="text-align: right;">NA</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">2</td>
</tr>
<tr class="even">
<td style="text-align: left;">2983</td>
<td style="text-align: right;">4005</td>
<td style="text-align: left;">Albania</td>
<td style="text-align: right;">NA</td>
<td style="text-align: left;"></td>
<td style="text-align: right;">1993</td>
<td style="text-align: left;">09B</td>
<td style="text-align: left;">B314</td>
<td style="text-align: left;">Chronic pharyngitis, nasopharyngitis and sinusitis</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">4</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">NA</td>
<td style="text-align: right;">NA</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
</tr>
<tr class="odd">
<td style="text-align: left;">158245</td>
<td style="text-align: right;">2090</td>
<td style="text-align: left;">Canada</td>
<td style="text-align: right;">NA</td>
<td style="text-align: left;"></td>
<td style="text-align: right;">1995</td>
<td style="text-align: left;">09B</td>
<td style="text-align: left;">B060</td>
<td style="text-align: left;">Syphilis</td>
<td style="text-align: right;">2</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">2</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">NA</td>
<td style="text-align: right;">NA</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
</tr>
<tr class="even">
<td style="text-align: left;">158246</td>
<td style="text-align: right;">2090</td>
<td style="text-align: left;">Canada</td>
<td style="text-align: right;">NA</td>
<td style="text-align: left;"></td>
<td style="text-align: right;">1995</td>
<td style="text-align: left;">09B</td>
<td style="text-align: left;">B061</td>
<td style="text-align: left;">Gonococcal infections</td>
<td style="text-align: right;">2</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">NA</td>
<td style="text-align: right;">NA</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
</tr>
<tr class="odd">
<td style="text-align: left;">356055</td>
<td style="text-align: right;">3090</td>
<td style="text-align: left;">Hong Kong SAR</td>
<td style="text-align: right;">NA</td>
<td style="text-align: left;"></td>
<td style="text-align: right;">1989</td>
<td style="text-align: left;">09B</td>
<td style="text-align: left;">B141</td>
<td style="text-align: left;">Leukaemia</td>
<td style="text-align: right;">2</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">2</td>
<td style="text-align: right;">75</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">4</td>
<td style="text-align: right;">2</td>
<td style="text-align: right;">4</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">6</td>
<td style="text-align: right;">6</td>
<td style="text-align: right;">2</td>
<td style="text-align: right;">5</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">9</td>
<td style="text-align: right;">2</td>
<td style="text-align: right;">9</td>
<td style="text-align: right;">7</td>
<td style="text-align: right;">4</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">NA</td>
<td style="text-align: right;">NA</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">NA</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
</tr>
<tr class="even">
<td style="text-align: left;">356056</td>
<td style="text-align: right;">3090</td>
<td style="text-align: left;">Hong Kong SAR</td>
<td style="text-align: right;">NA</td>
<td style="text-align: left;"></td>
<td style="text-align: right;">1989</td>
<td style="text-align: left;">09B</td>
<td style="text-align: left;">B149</td>
<td style="text-align: left;">Remainder of B14</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">2</td>
<td style="text-align: right;">132</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">6</td>
<td style="text-align: right;">3</td>
<td style="text-align: right;">6</td>
<td style="text-align: right;">5</td>
<td style="text-align: right;">13</td>
<td style="text-align: right;">14</td>
<td style="text-align: right;">18</td>
<td style="text-align: right;">17</td>
<td style="text-align: right;">22</td>
<td style="text-align: right;">13</td>
<td style="text-align: right;">10</td>
<td style="text-align: right;">2</td>
<td style="text-align: right;">NA</td>
<td style="text-align: right;">NA</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">NA</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
</tr>
<tr class="odd">
<td style="text-align: left;">715548</td>
<td style="text-align: right;">3350</td>
<td style="text-align: left;">Singapore</td>
<td style="text-align: right;">NA</td>
<td style="text-align: left;"></td>
<td style="text-align: right;">2010</td>
<td style="text-align: left;">09B</td>
<td style="text-align: left;">B18</td>
<td style="text-align: left;">Endocrine and metabolic diseases, immunity disorders</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">156</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">2</td>
<td style="text-align: right;">5</td>
<td style="text-align: right;">7</td>
<td style="text-align: right;">14</td>
<td style="text-align: right;">23</td>
<td style="text-align: right;">14</td>
<td style="text-align: right;">21</td>
<td style="text-align: right;">9</td>
<td style="text-align: right;">16</td>
<td style="text-align: right;">18</td>
<td style="text-align: right;">10</td>
<td style="text-align: right;">14</td>
<td style="text-align: right;">NA</td>
<td style="text-align: right;">NA</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0</td>
</tr>
<tr class="even">
<td style="text-align: left;">715549</td>
<td style="text-align: right;">3350</td>
<td style="text-align: left;">Singapore</td>
<td style="text-align: right;">NA</td>
<td style="text-align: left;"></td>
<td style="text-align: right;">2010</td>
<td style="text-align: left;">09B</td>
<td style="text-align: left;">B180</td>
<td style="text-align: left;">Disorders of thyroid gland</td>
<td style="text-align: right;">2</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">1</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">NA</td>
<td style="text-align: right;">NA</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
<td style="text-align: right;">0</td>
</tr>
</tbody>
</table>

Dataset Characteristics
-----------------------

-   Rows : 900 558
-   Columns : 41
-   Size : 81mb

Variables
---------

| Variable | Description | 
| -------- | -------- | 
| country | WHO Country Code  | 
| name | Country name   | 
| year | Year  | 
| cause | Cause of death (ICD9 code)  | 
| description |  Cause of death (ICD9 description) |
| sex |  Sex |
| deaths1 |  Deaths at all ages |
| im_deaths1 |  Infant deaths at age 0 day |


Use examples
------------

![Mortalité ICD-9 Monde](dataset/mortalite_icd9/dash-who-fr-12.png)

Licence
-------

**Attribution-NonCommercial-ShareAlike 3.0 IGO (CC BY-NC-SA 3.0 IGO)**

Source
------

Source :
<a href="https://www.who.int/data/data-collection-tools/who-mortality-database" class="uri">https://www.who.int/data/data-collection-tools/who-mortality-database</a>

Data Management
---------------

    data = read.csv2(file.path(path, "Morticd9.csv"), sep = ",")
    country_codes = read.csv2(file.path(path, "country_codes.csv"), sep = ",")
    code_cause = read.csv2(file.path(path, "code_cause.csv"))


    colnames(data) = tolower(colnames(data))

    data <- merge(data, code_cause, by = "cause", all.x = T)
    data <- merge(data, country_codes, by = "country")

    data <- data %>% relocate(name, .after = country)
    data <- data %>% relocate(cause, .after = list)
    data <- data %>% relocate(description, .after = cause) %>% arrange(name, year, cause)


    #write.csv(data,file.path(path, "mortalite_icd9.csv"), row.names = FALSE)
