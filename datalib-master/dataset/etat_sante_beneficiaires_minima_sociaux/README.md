# TODO
Mettre à jour le readme pour être conforme au nouveau format Description/Preview/Dataset_Characteristics/Variable/Source/Datamanagement
Mettre à jour le fichier de description des variables pour être conforme au format nom_dans_df/nom_court/nom_long/type_variable/description
Mettre à jour le readme pour refléter les changements dans les noms de fichier

---

| Fichier | Description |
| ------- | ----------- |
| État de santé des bénéficiaires | Résultats d'une enquête sur les bénéficiaires de minima sociaux basée sur un questionnaire. Fichier xlsx |
| Description des variables | CSV décrivant les variables trouvables dans le xlsx |
| Data 3 | Un script R traitant un exemple de traitement de données et créant un graphique basique |

https://drees.solidarites-sante.gouv.fr/sources-outils-et-enquetes/01-enquete-sur-les-beneficiaires-de-minima-sociaux-bms

# Résumé
L’enquête sur les bénéficiaires de minima sociaux (BMS) réalisée par la DREES s’inscrit dans le cadre du dispositif d’observation statistique des situations des populations en difficulté. Elle a pour principal objectif de mieux connaitre les conditions de vie des bénéficiaires de minima sociaux. La dernière enquête a été menée fin 2018. Elle succède à trois enquêtes menées en 2012, 2006 et 2003.
Objectifs et thèmes

L’enquête de 2018 vise à prolonger celles menées par la DREES auprès des bénéficiaires de minima sociaux lors des trois précédentes éditions, afin d’actualiser les enseignements tirés. Elle met aussi l’accent sur la question des revenus, des dépenses et du « reste à vivre » des bénéficiaires de revenus minima garantis.

Alors que le débat public a surtout porté depuis la fin des années 1990 sur l’incitation au retour à l’emploi des bénéficiaires de minima sociaux d’âge actif, les questions sur le niveau de vie des bénéficiaires de revenus minima garantis reviennent sur le devant de la scène ces dernières années à la faveur notamment des débats sur l’évolution du pouvoir d’achat. Le poids grandissant des dépenses pré-engagées dans les revenus des plus modestes pose en effet la question de leur difficulté à « boucler leurs fins de mois ». Il s’agit donc de comprendre dans quelle mesure les bénéficiaires de revenus minima garantis ont des revenus leur permettant de vivre décemment.

Deux nouveautés majeures sont introduites pour cette édition de 2018 :

Les bénéficiaires de la prime d’activité, mise en place au 1er janvier 2016, sont interrogés ce qui permettra d’obtenir des informations inédites sur ces personnes.
Les départements d’Outre-mer (La Réunion et la Martinique) sont intégrés à l’enquête ce qui permettra d’obtenir des informations exploitables au niveau de la France, et non plus de la Métropole uniquement.

Enfin, la réédition de cette enquête auprès de bénéficiaires de minima sociaux est aussi l’occasion de compléter et d’approfondir les thèmes abordés dans les enquêtes précédentes sur leurs conditions de vie.

Les principaux thèmes abordés dans le questionnaire de 2018 sont les suivants :

- Situation familiale et professionnelle,
- Revenu et niveau de vie,
- Dépenses contraintes (logement, transport, énergie, téléphone…),
- Conditions de logement,
- Privations matérielles,
- Accès aux aides, relations avec les organismes, accompagnement action d’insertion (partie plus développée cette année),
- Situation financière,
- Santé, handicap,
- Opinion et perception des minima sociaux.

L’enquête BMS 2012 avait également permis d’actualiser les enseignements tirés lors des deux précédentes enquêtes en mettant l’accent, pour la première fois, sur la question du « reste-à-vivre » des bénéficiaires de minima sociaux. Elle visait à appréhender cette thématique tant dans ses aspects quantitatifs (mesure approchée d’un reste-à-vivre) que qualitatifs (conséquences en termes d’adaptation des modes de consommation, de privations, de recherche de ressources alternatives).

L’enquête BMS 2006 avait pour objectif premier de mesurer les conséquences de la nouvelle organisation institutionnelle du RMI (décentralisation de 2004) sur les parcours d’insertion sociale et professionnelle des allocataires. Elle insistait également davantage sur la situation des allocataires par rapport à l’emploi : calendrier d’emploi, recherche d’emploi, connaissance des dispositifs d’intéressement, afin de mesurer l’impact de la mise en place du plan de cohésion sociale de 2005.

La première enquête BMS 2003 avait pour objectif de décrire et de comparer la situation des allocataires de minima sociaux en apportant des informations aussi bien sur leurs conditions de vie (habitat et santé notamment) que sur leurs trajectoires professionnelles.
Champ de l’enquête

L’enquête BMS 2018 a été réalisée au quatrième trimestre 2018 en France métropolitaine, à La Réunion et en Martinique auprès d’environ 12 200 personnes qui bénéficiaient d’un minimum social ou de la prime d’activité au 31 décembre 2017. 3 700 personnes percevaient le revenu de solidarité active (RSA), 4 100 la prime d’activité (PA), 1 500 personnes bénéficiaient de l’allocation de solidarité spécifique (ASS), 2 000 de l’allocation aux adultes handicapés (AAH) et 1 800 du minimum vieillesse (la somme dépasse 12 200 car certaines personnes cumulent des prestations). 950 personnes ont été interrogées dans les DROM. Les bénéficiaires de minima sociaux d’âge actif (RSA, AAH, ASS) et de la prime d’activité ont été sélectionnés dans les bases de gestion administratives (CNAF, CCMSA et Pôle emploi) à partir de l’échantillon national interrégimes d’allocataires de compléments de revenus d’activité et de minima sociaux (ENIACRAMS). Pour le minimum vieillesse, l’échantillon a été tiré dans les fichiers des principaux organismes verseurs du minimum vieillesse : la Caisse nationale de l’assurance vieillesse (CNAV), le Service de l’allocation de solidarité aux personnes âgées de la Caisse des Dépôts (SASPA) et la Caisse centrale de la mutualité sociale agricole (CCMSA).

L’enquête BMS 2012 a été réalisée au quatrième trimestre 2012 en France métropolitaine auprès d’environ 8 450 personnes qui bénéficiaient d’un minimum social au 31 décembre 2011. 3 850 personnes percevaient le revenu de solidarité active (RSA) : 1 450 le RSA socle non majoré, 1 100 le RSA socle majoré et 1 300 le RSA activité seul. 1 800 personnes bénéficiaient de l’allocation de solidarité spécifique (ASS), 1 400 de l’allocation aux adultes handicapés (AAH) et 1 400 du minimum vieillesse. Dans le cas du RSA, le champ de l’enquête couvrait l’ensemble des bénéficiaires, c’est-à-dire les allocataires administratifs, mais aussi les éventuels conjoints. Les bénéficiaires de minima sociaux d’âge actif (RSA, AAH, ASS) ont été sélectionnés dans les bases de gestion administratives (CNAF, CCMSA et Pôle emploi) à partir de l’échantillon national interrégimes d’allocataires de minima sociaux (ENIAMS). Pour le minimum vieillesse, l’échantillon a été tiré dans les fichiers des principaux organismes verseurs du minimum vieillesse : la Caisse nationale de l’assurance vieillesse (CNAV), le Service de l’allocation de solidarité aux personnes âgées de la Caisse des Dépôts (SASPA) et la Caisse centrale de la mutualité sociale agricole (CCMSA).

L’enquête BMS 2006 a été réalisée au deuxième trimestre 2006 auprès d’environ 6 800 personnes bénéficiaires d’un minimum social au 31 décembre 2004, dont 3 600 bénéficiaires du RMI (allocataires+éventuels conjoints), 2 000 de l’API et 1 200 de l’ASS. L’enquête BMS 2003 a été réalisée au premier trimestre 2003 auprès d’environ 5 000 personnes bénéficiaires d’un minimum social au 31 décembre 2001, dont 2 000 bénéficiaires du RMI (allocataires+éventuels conjoints) et 1 000 bénéficiaires de chacune des trois prestations suivantes : API, ASS et AAH. Tout comme en 2012 pour les minima sociaux d’âge actif, les échantillons des enquêtes BMS 2003 et 2006 ont été sélectionnés à partir de l’ENIAMS sur le champ de la France métropolitaine. .

À noter que les allocataires de l’AAH ne font pas partie du champ de l’enquête BMS 2006 et que les allocataires du minimum vieillesse ne font partie que du champ de l’enquête BMS 2012.
Protocole de collecte

L’interrogation des personnes s’est faite en face-à-face par des enquêteurs et dure une heure en moyenne.

Les résultats de l’enquête 2018 seront appariés avec des sources administratives et d’autres fichiers existants :

pour les ressources fiscales : appariement avec le fichier « revenus fiscaux » de l’Insee ;
pour les prestations familiales, les aides au logement, les minima sociaux, la prime d’activité et les allocations chômage : appariement avec les fichiers de la CNAF, de la MSA et de Pôle emploi ;
pour les prestations vieillesse non imposables : appariement avec les fichiers de la CNAV, de la MSA et de la caisse des dépôts et consignation.
Le résultat de ces appariements sera disponible en 2021.

Les procédures d’imputations post appariements réalisées sur les données de l’enquête BMS 2012 sont décrites dans le document de travail suivant :

Céline ARNOLD, Nathalie MISSEGUE, 2017, « Appariement fiscal et social de l’enquête Bénéficiaires de minima sociaux (2012) - Imputations post appariement », Document de travail, Série sources et méthodes, n°64, Drees, septembre
Information juridique sur les appariements de données de revenus menés pour l’enquête BMS 2018

Comme mentionné plus haut, l’enquête BMS 2018 met l’accent sur la question des revenus, des dépenses et du « reste à vivre » des bénéficiaires de revenus minima garantis. Les montants de dépenses des ménages sont issus des réponses au questionnaire de l’enquête. Pour gagner du temps lors de la passation du questionnaire mais aussi pour assurer une meilleure comparabilité avec les résultats issus d’autres enquêtes du service statistique public (enquête ERFS, enquête SRCV, …), les revenus sont pour l’essentiel obtenus à partir d’appariements menés par l’Insee, la CNAF, la CCMSA, la CNAV, le SASPA et Pôle emploi.

Conformément au règlement général sur la protection des données (RGPD) et à la loi n° 78-17 du 6 janvier 1978 modifiée relative à l’informatique, aux fichiers et aux libertés (loi informatique et libertés), toute personne appartenant à un ménage ayant répondu au questionnaire de l’enquête BMS 2018 dispose d’un droit d’accès, de rectification de ses données à caractère personnel et de limitation du traitement la concernant.

Ces droits peuvent être exercés en écrivant à l’adresse drees-rgpd@sante.gouv.fr en indiquant le code BMS 2018 dans la demande.

Par ailleurs, toute personne appartenant à un ménage ayant répondu au questionnaire de l’enquête BMS 2018 dispose également d’un droit d’introduire une réclamation auprès de l’autorité de contrôle, la Commission nationale de l’informatique et des libertés (CNIL), si elle considère que le traitement de données à caractère personnel la concernant par le responsable de traitement constitue une violation du RGPD et de la loi informatique et libertés.

