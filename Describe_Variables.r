#install.packages('docstring')
library(docstring)
library(roxygen2)
library(purrr)
library(base)
library(kableExtra)

library(tinytex)
library(ggplot2)
library(knitr)

library(gridExtra)
library(cowplot)
library(stringr)
library(lubridate)

library(dplyr)
library(tinytex)
library(tidyr)
library(rmarkdown)
library(pander)


##########################################################################################
##########################################################################################
##########################################################################################
############################## 1st section : Descriptive Functions #######################
##########################################################################################
##########################################################################################
##########################################################################################
#desc_binaire(variable, lib_court, libelle_long)	Binary Variable Description
desc_binaire <- function(variable, lib_court, libelle_long)
{
  
  #' Describe a binary variable
  #' 
  #' Print the number of missing data , the frequency of each modality. And plots the bar plot.
  #' 
  #' @param variable a dataframe column containing 2 modalities.
  #' @param lib_court the plots title.
  #' @param libelle_long the paragraph title.
  #'  
  #'
  
  
  
  # #print the Paragraph's title
  cat("\n##",libelle_long,"\n")
  
  # #print the type of the variable
  cat("- Le type est : **Binaire**\n\n")
  modalities = unique(variable)
  cat(paste0('- Les deux modalités sont : **',modalities[[1]][1],'** et **',modalities[[1]][2],'**\n\n'))
  
  # #print the number of missing data
  #variable = variable%>%mutate_all(funs(str_replace_all(.,pattern=" ", repl=""))
  missing_data = sum(is.na(variable),variable=="") 
  cat(paste0('- Le nombre de données manquantes est : **', missing_data,' **. Le pourcentage de données manquantes est : **', round(missing_data/nrow(variable)*100,2),'% ** des données.\n\n'))
  # #Computes the frequency table and modalities
  frequency_table = variable%>%
    group_by_at(1)%>%
    summarise(Effectif = n()) %>%
    mutate(Pourcentage = round(Effectif / sum(Effectif) * 100, 3)) %>% 
    arrange(desc(Pourcentage))
  colnames(frequency_table)[1] = "Modalités"
  # #print the frequency of the n most frequent modalities
  cat('- L\'effectif de chaque modalité :\n\n')
  cat(pander(frequency_table , style = 'rmarkdown', caption = "Effectif de chaque modalité"))
  
  
  
  # #Creat the barplot
  frequency_table$Modalités <- as.factor(frequency_table$Modalités)
  ggplot(frequency_table, aes(x=Modalités ,
                              y=Pourcentage,
                              fill=Modalités )) + 
    geom_bar(width=0.4,stat="identity") +
    ggtitle(lib_court) +
    xlab("Modalités")+ylab("Pourcentage (%)")+
    theme_light()+
    theme(legend.position="none",
          plot.title = element_text(hjust = 0.5)
    ) 
  
  
  
}


##########################################################################################
##########################################################################################
#desc_quali(variable, lib_court, libelle_long) Categorical Variable Description
desc_quali <- function(variable, lib_court, libelle_long , n = 10)
{
  #' Describe a categorical variable
  #' 
  #' Print the number of missing data , the frequency of each modality sorted. And plots the bar plot.
  #' 
  #' @param variable a dataframe column containing categorical data.
  #' @param lib_court the plots title.
  #' @param libelle_long the paragraph title.
  #' @param n number of modalities to show.  
  #'
  
  # #print the Paragraph's title
  cat("\n##",libelle_long,"\n")
  
  # #print the type of the variable
  cat("- Le type est : **Qualitative**\n\n")
  modalities = unique(variable)
  nbr_modalities = nrow(modalities)
  
  cat(paste0('- Le nombre de modalité est : **',nbr_modalities,'**\n\n'))
  
  # #print the number of missing data
  #variable = variable%>%mutate_all(funs(str_replace_all(.,pattern=" ", repl=""))
  missing_data = sum(is.na(variable),variable=="") 
  cat(paste0('- Le nombre de données manquantes est : **', missing_data,' **. Le pourcentage de données manquantes est : **', round(missing_data/nrow(variable)*100,2),'% ** des données.\n\n'))
  
  # #Computes the frequency table and modalities
  frequency_table = variable%>%
    group_by_at(1)%>%
    summarise(Effectif = n()) %>%
    mutate(Pourcentage = round(Effectif / sum(Effectif) * 100, 3)) %>% 
    arrange(desc(Pourcentage))
  colnames(frequency_table)[1] = "Modalités"
  
  # #print the frequency of the n most frequent modalities
  n = min(n, nbr_modalities)
  caption = if(nbr_modalities > n) paste0("L\'effectif des ",n," plus fréquentes modalités") else "L'effectif par modalité"
  
  cat(paste0('\n\n- ', caption ,':\n\n'))
  cat(pander(frequency_table[1:n, ] , style = 'rmarkdown', caption = caption ))
  
  
  
  
  # #Creat the barplot
  frequency_table$Modalités <- as.factor(frequency_table$Modalités)
  ggplot(frequency_table[1:n,], aes(x=Modalités ,
                                    y=Pourcentage,
                                    fill=Modalités )) + 
    geom_bar(width=0.2,stat="identity") +
    ggtitle(lib_court) +
    xlab("Modalités")+ylab("Pourcentage (%)")+
    theme_light()+
    theme(legend.position="none",
          plot.title = element_text(hjust = 0.5),
          axis.text.x = element_text(angle = 90)
    ) 
  
  
}

##########################################################################################
##########################################################################################
#desc_quanti_disc(variable, lib_court, libelle_long) Discrete Variable Description
desc_quanti_disc <- function(variable, lib_court, libelle_long)
{
  
  #' Describe a discrete quantitative variable
  #' 
  #' Print the number of missing data , the mean , the median , the min and max . And plots the histogram.
  #' 
  #' @param variable a dataframe column containing discrete quantitative data.
  #' @param lib_court the plots title.
  #' @param libelle_long the paragraph title. 
  #'
  
  # #print the Paragraph's title
  cat("\n##",libelle_long,"\n")
  
  # #print the type of the variable
  cat("- Le type est : **Quantitative discrete**\n\n")
  
  
  # #print the number of missing data
  missing_data = sum(is.na(variable)) 
  cat(paste0('- Le nombre de données manquantes est : **', missing_data,'**. Le pourcentage de données manquantes est : **', round(missing_data/nrow(variable)*100,2),'%** des données.\n\n'))
  
  
  # #Computes the mean , the median , the min and max
  cat('- Les statistiques de la variable :\n\n')
  kbl = as.data.frame(sapply(variable, summary))
  colnames(kbl) <- c('Statistique')
  cat(pander(kbl, style = 'rmarkdown', caption = "Les statistiques de la variable"))
  
  
  # #Creat the barplot
  K = floor(1 + 3.322*log10(nrow(variable))) #bin sizes with Sturge’s Rule
  
  colnames(variable)[1] = "valeurs"
  ggplot(variable, aes(x=valeurs)) + 
    geom_histogram(bins = K , na.rm = TRUE , fill="#69b3a2", color="#e9ecef", alpha=0.8) +
    ggtitle(lib_court) +
    xlab("Valeurs")+ylab("Count")+
    theme_light()+
    theme(legend.position="none",
          plot.title = element_text(hjust = 0.5)
    ) 
  
}


##########################################################################################
##########################################################################################
#desc_quanti_cont(variable, lib_court, libelle_long) Continue Variable Description
desc_quanti_cont <- function(variable, lib_court, libelle_long)
{
  
  #' Describe a continuous quantitative variable
  #' 
  #' Print the number of missing data , the mean , the median , the min and max . And plots the histogram.
  #' 
  #' @param variable a dataframe column containing continuous quantitative data.
  #' @param lib_court the plots title.
  #' @param libelle_long the paragraph title. 
  #'
  
  ##Check if the variable contains any letters if so we will use the desc_quali instead 
  if(any(sapply (variable , function(x) str_detect(string = replace_na(x, ''), pattern = "[:alpha:]"))))
  {
    desc_quali(variable, lib_court, libelle_long , n = 10) 
  }else{
    variable = as.data.frame(sapply(variable,function(x) as.numeric(gsub(",",".",as.character(x)))))
    if(all(sapply(variable, is.numeric))){
      # #print the Paragraph's title
      cat("\n##",libelle_long,"\n")
      
      # #print the type of the variable
      cat("- Le type est : **Quantitative continue**\n\n")
      
      
      # #print the number of missing data
      missing_data = sum(is.na(variable)) 
      cat(paste0('- Le nombre de données manquantes est : **', missing_data,'**. Le pourcentage de données manquantes est : **', round(missing_data/nrow(variable)*100,2),'%** des données.\n\n'))
      
      
      # #Computes the mean , the median , the min and max
      cat('- Les statistiques de la variable :\n\n')
      kbl = as.data.frame(sapply(variable, summary))
      colnames(kbl) <- c('Statistique')
      cat(pander(kbl, style = 'rmarkdown', caption = "Les statistiques de la variable"))
      
      
      
      # #Creat the barplot
      K = floor(1 + 3.322*log10(nrow(variable))) #bin sizes with Sturge’s Rule
      
      colnames(variable)[1] = "valeurs"
      ggplot(variable, aes(x=valeurs)) + 
        geom_histogram(aes(y = ..density..),bins = K , na.rm = TRUE , fill="#69b3a2", color="#e9ecef", alpha=0.8) +
        geom_density(na.rm = TRUE)+
        ggtitle(lib_court) +
        xlab("Valeurs")+ylab("Densité")+
        theme_light()+
        theme(legend.position="none",
              plot.title = element_text(hjust = 0.5)
        ) 
      
    }}}



##########################################################################################
##########################################################################################
#desc_date(variable, lib_court, libelle_long) Temporal Variable Description
desc_date <- function(variable, lib_court, libelle_long , unit = "month")
{
  
  #' Describe a date variable
  #' 
  #' Print the number of missing data , the mean , the median , the min and max . And plots the histogram.
  #' 
  #' @param variable a dataframe column containing continuous quantitative data.
  #' @param lib_court the plots title.
  #' @param libelle_long the paragraph title. 
  #' @param unit unit of time. 
  #'
  
  # #print the Paragraph's title
  cat("\n##",libelle_long,"\n")
  
  # #print the type of the variable
  cat("- Le type est : **Date**\n\n")
  
  
  # #print the number of missing data
  missing_data = sum(is.na(variable) , variable=="") 
  cat(paste0('- Le nombre de données manquantes est : **', missing_data,'**. Le pourcentage de données manquantes est : **', round(missing_data/nrow(variable)*100,2),'%** des données.\n\n'))
  
  # # Convert the variable into a date format
  colnames(variable) = 'date'
  variable$date = as.Date(variable$date, tryFormats = c("%d/%m/%Y", "%d-%m-%Y", "%Y-%m-%d", "%Y/%m/%d"))
  
  
  ## Compute and Print the Most recent and oldest dates, interval , average interval between 2 dates
  cat(paste0("- Date Plus Récente : **",max(variable$date),"**\n\n"))
  cat(paste0("- Date Plus Ancienne : **",min(variable$date),"**\n\n"))
  cat(paste0("- Interval : **",as.duration(max(variable$date) - min(variable$date)),"**\n\n"))
  cat(paste0("- Interval moyen entre deux dates : **",as.duration(mean(diff(sort(variable$date)))),"**\n\n"))
  
  
  ## Compute the number of observations per unit of time 
  frequency_table = variable %>% group_by(unit = floor_date(date, unit)) %>% summarise(Effectif = n())
  
  colnames(frequency_table)[1] = "Dates"
  
  cat(pander(frequency_table, style = "rmarkdown", caption = paste0("Effectif par unité de temps ",unit)))
  
  
  
  
  # #Creat the line plot with the count of rows for each date
  
  ggplot(variable %>% count(date) , aes(x=date , n ,group = 1)) +
    geom_line() + 
    geom_point() +
    ggtitle('lib_court') +
    xlab("Date")+ylab("Count")+
    theme_light()+
    theme(legend.position="none",
          plot.title = element_text(hjust = 0.5),
          axis.text.x = element_text(angle = 90))
  
}

##########################################################################################
##########################################################################################
##########################################################################################
################################### 2nd section : Scan Folder ############################
##########################################################################################
##########################################################################################
##########################################################################################
#Parcours de dossiers
scan_folder = function (url, pattern, recursive, search_variables = FALSE)
{
  #' Scans a specified directory and lists its files.
  #' 
  #' @param url the path to the directory to be scanned
  #' @param pattern a regular expression pattern for the filenames
  #' @param recursive a boolean if TRUE the under-directories will also be scanned
  #' @param search_variables a boolean if TRUE will check if the directory containing the filename with a specifed pattern also contains a file with the name pattern 'variables_*’
  #' 
  #' @return If search_variables is TRUE, returns a data.frame with 2 columns:
  #' 
  #' data_file : The names of files with specified pattern
  #' 
  #' variable_file : The names of the files in the same directory as the data_file and with a name respecting the pattern '^variables_*'
  #' 
  #'  If search_variables is FALSE, A character vector of the names of the files respecting the specified criterion (i.e. Locatio and pattern).
  
  
  data = list.files(path = url, pattern = pattern, recursive = recursive,full.names = TRUE)
  if(search_variables)
  {
    data = data.frame(data_file = data)
    data$variable_file = map_chr(data$data_file , .f = function(x) 
    {var = list.files(path = dirname(x),pattern = '^variable*',full.names = TRUE)
    if(rlang::is_empty(var)) return("")
    else return(var)})
  }
  
  return(data)
  
}




##########################################################################################
##########################################################################################
##########################################################################################
################################## 3rd section : Build Report ############################
##########################################################################################
##########################################################################################
##########################################################################################
#Create file RMD
create_rmd = function(filename = "Variable_Description_Report.Rmd",
                      default_data = "./datalib-master/dataset/vaccination/data_donnees-de-vaccination-par-epci.csv",
                      default_variable = "./datalib-master/dataset/vaccination/variables_donnees-de-vaccination-par-epci.csv",
                      default_title = "Variables donnees de vaccination par epci")
{
  #' Create the RMD File in the working directory.
  #' 
  #' @param filename the RMD file name
  #' @param default_data the default data file to pass As parameter
  #' @param default_variable the default variable file to pass As parameter
  #' @param default_title the default title to pass As parameter
  #' 
  #' 
  
  
  
  ### Rmd Yaml Parameters
  chunks <- paste("---",
                  "title: \"`r params$doc_title`\"",
                  "date: \"`r format(Sys.time(),  '%d %B %Y')`\"",
                  "output: ",
                  "    html_document : ",
                  "        toc: true    ",             #Add the table of content
                  "        toc_float: true",
                  "    pdf_document  : ", 
                  "        toc: true    ",       
                  "    word_document : ",
                  "        toc: true    ",
                  "params:",
                  paste0("    data_file: \"",default_data,"\""), #Default data file
                  paste0("    variables_file: \"",default_variable,"\""),  #Default variable file
                  paste0("    doc_title: \"",default_title,"\""),        #Default title 
                  "---",
                  sep="\n")
  ##### Rmd Chunks
  ### setup chunk
  chunks <- paste(chunks,
                  "```{r setup, include=FALSE}",
                  "knitr::opts_chunk$set(echo = FALSE, error=FALSE, fig.align = \"center\")",
                  "```",    
                  "```{r,Libraries,include=FALSE}",
                  "library(purrr)",
                  "library(base)",
                  "library(kableExtra)",
                  "library(tinytex)",
                  "library(ggplot2)",
                  "library(stringr)",
                  "library(dplyr)",
                  "library(pander)",
                  "library(knitr)",
                  "library(lubridate)",
                  "library(tidyr)",
                  "```",
                  sep="\n")
  
  ### functions chunk
  chunks <- paste(chunks,
                  "```{r,functions,include=FALSE}",
                  "#Binary Variable Description",
                  "desc_binaire <- function(variable, lib_court, libelle_long) ",
                  paste(body(desc_binaire), collapse = '\n\n'),
                  "}\n",
                  "#Qualitative Variable Description",
                  "desc_quali <- function(variable, lib_court, libelle_long , n = 10) ",
                  paste(body(desc_quali), collapse = '\n\n'),
                  "}\n",
                  "#Discrete Variable Description",
                  "desc_quanti_disc <- function(variable, lib_court, libelle_long) ",
                  paste(body(desc_quanti_disc), collapse = '\n\n'),
                  "}\n",
                  "#Continuous Variable Description",
                  "desc_quanti_cont <- function(variable, lib_court, libelle_long) ",
                  paste(body(desc_quanti_cont), collapse = '\n\n'),
                  "}\n",
                  "#Date Variable Description",
                  "desc_date <- function(variable, lib_court, libelle_long,unit=\"month\") ",
                  paste(body(desc_date), collapse = '\n\n'),
                  "}\n",
                  "```",
                  sep="\n")
  
  ### Load Data chunk
  chunks <- paste(chunks,
                  "```{r DataLoad,include=FALSE}",
                  "data = read.csv(params$data_file,sep=';')",
                  "variables = read.csv(params$variables_file,sep = ';')",
                  "```",
                  sep="\n")
  
  
  ### Describe Variable Chunks
  
  chunks <- paste(chunks,
                  "\n```{r Describe Variables,include=FALSE}",
                  "template <-\"```{r {{variable}}, results='asis'}",
                  "if('{{type}}' %in% c('binaire')){",
                  "desc_binaire(variable = data['{{variable}}'], lib_court = '{{short_label}}', libelle_long = '{{long_label}}')",
                  "}else if('{{type}}' %in% c('quali')){",
                  "desc_quali(variable = data['{{variable}}'], lib_court = '{{short_label}}', libelle_long = '{{long_label}}')",
                  "}else if('{{type}}' %in% c('quanti')){",
                  "desc_quanti_disc(variable = data['{{variable}}'],lib_court = '{{short_label}}',libelle_long = '{{long_label}}')",
                  "}else if('{{type}}' %in% c('quanti_cont')){",
                  "desc_quanti_cont(variable = data['{{variable}}'], lib_court = '{{short_label}}',libelle_long = '{{long_label}}')",
                  "}else if('{{type}}' %in% c('date')){",
                  "desc_date(variable = data['{{variable}}'], lib_court = '{{short_label}}', libelle_long = '{{long_label}}')",
                  "}\"",
                  "variables = variables%>%filter(variable %in% colnames(data))", ##Filter the variables that dont exist in the dataframe
                  ##Add a R chunk for each variable
                  "src = apply(variables,1, function(x)",
                  "{
                      knitr::knit_expand(text = template,
                                         variable = x['variable'],
                                         type = x['type'],
                                         short_label= str_replace_all(x['short_label'],'[^[:alnum:]]', ' '),
                                         long_label = str_replace_all(x['long_label'],'[^[:alnum:]]', ' '))
                    })",
                  "```",
                  "`r knitr::knit(text = unlist(src))`",
                  sep="\n")
  
  
  
  ### Write the file if it exists it will override it
  
  fileConn<-file(filename)
  
  writeLines(chunks, fileConn)
  
  close(fileConn)
  
}



#Build Reports
build_report = function(data, variables, output, title , rmdFile = "Variable_Description_Report.Rmd")
{
  #' Build a report based on the RMD file passed as parameter, in the output format passed as parmeter
  #' 
  #' @param data the data file. 
  #' @param variables the variable file.
  #' @param output the report format.
  #' @param title the title of the report.
  #' @param rmdFile The Rmd File.
  #' 
  #' 
  ################################################
  #Prepare FileName
  ################################################
  path = dirname(data)
  date = format(Sys.Date() , "%y%m%d")
  filename = basename(data)
  filename = str_replace(str_replace(filename,pattern = "data",repl="stats_desc"),pattern = ".csv",repl=paste("_",date,sep=""))
  
  ################################################
  #Generate Rmd File if doesn't exist
  ################################################
  if(!file.exists(rmdFile)){
    create_rmd(filename = rmdFile)
  }
  
  ################################################
  #Rendre HTML/PDF/Word File
  ################################################
  render( input = rmdFile,
          output_format = output,
          output_file = filename,
          output_dir = path,
          params = list(
            data_file = data,
            variables_file = variables ,
            doc_title = str_to_title(title)
          ))
}




##########################################################################################
##########################################################################################
##########################################################################################
##################################### 4th section : Test #################################
##########################################################################################
##########################################################################################
##########################################################################################

main <- function(url = "." , output = "html_document")
{
  #' Construct a report for each repository under the url, that contains a data and variable files.
  #' 
  #' @param url the url to sacn for data and varaible files. 
  #' @param output the report type
  #' 
  #' 
  #' 
  ### Scan Folder
  
  data_files = scan_folder(url , pattern = "^data_*" , recursive = TRUE ,search_variables = TRUE)
  
  ### Filter out Data files that don't have a variable description 
  
  data_files = data_files%>%
    filter(variable_file != "")
  
  
  ### Filter out Data files that don't have four columns 
  data_files = cbind(data_files,
                     'Nbr_columns' = apply(data_files , 1, 
                                           function(x) length(names(read.csv(x['variable_file'],
                                                                             sep = ";")))))
  data_files = data_files%>% filter(Nbr_columns == 4)
  
  
  for (i in seq(nrow(data_files)))
  {
    ### Read data file
    data = data_files[i,"data_file"]
    cat("Data :", data , '\n')
    ### Read variable file
    variables = data_files[i,"variable_file"]
    variables_check_file = read.csv(variables, sep = ';')
    colnames(variables_check_file)
    ### File Title
    
    title = str_replace_all(str_replace_all(basename(data_files[i,"data_file"]),"[_-]"," "),
                            ".csv","")
    
    ### Build the report
    build_report(data = data, 
                 variables = variables, 
                 output = output, 
                 title = title)
    
  }
  
}

#main()
#main(output = "pdf_document")
#main(output = "word_document")


